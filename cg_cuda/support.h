#ifndef SUPPORT_H_
#define SUPPORT_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>


////////////////////////////////////////////////////////////////////////////////

//#define TRACE_ON
#ifdef TRACE_ON
#define TRACE(x) do { printf x; /*fflush(stdout);*/ } while(0);
#define CUTRACE(x) do { cuPrintf x; } while(0);
#else
#define TRACE(x) ;
#define CUTRACE(x) ;
#endif // TRACE_ON

////////////////////////////////////////////////////////////////////////////////


// float format (printf)
#define FFMT "%1.6e"

// warp size is 32 on all devices/compute capabs.
#define WARP_SIZE 32
#define BLOCK_SIZE 64
#define GRID_SIZE 16


// memory location
enum LOC_TYPE {
	LOC_NONE = 0,
	LOC_HOST,
	LOC_DEVICE,
	LOC_MAPPED_HOST,
	LOC_MAPPED_DEV
};


typedef struct {
	size_t loc; // location
	size_t size;
	float *items;
} VECTOR, *PVECTOR;


typedef struct {
	size_t loc; // location
	size_t nrow;
	size_t ncol;
	size_t stride; // for submatrix representation
	float *items;
} MATRIX, *PMATRIX;


// create host or device vector
VECTOR CreateVector(size_t size, size_t loc);
void ZeroVector(VECTOR v);
void CreateMappedVector(size_t size, VECTOR &mh_v, VECTOR &md_v);
void DestroyVector(PVECTOR v);
MATRIX CreateMatrix(size_t nrow, size_t ncol, size_t loc);
void DestroyMatrix(PMATRIX m);
void PrintFloat(float f);
void PrintMatrix(const PMATRIX m);
void TraceMatrix(const PMATRIX m);
void PrintVector(const PVECTOR v);
void PrintDevVector(VECTOR d_v);
void TraceDevVector(VECTOR d_v);
void TraceVector(const PVECTOR v);
void RandomVector(VECTOR v);
void RandomMatrix(MATRIX m);
float DotProduct(VECTOR v1, VECTOR v2);
float Norm(VECTOR v);
void TransposeMatrix(MATRIX m1, MATRIX m2);
void Saxpy(VECTOR x, float a, VECTOR y);
void MVMult(MATRIX A, VECTOR x, VECTOR y);
void MMMult(MATRIX A, MATRIX B, MATRIX C);
void CopyVector(VECTOR dst, VECTOR src);


#endif // SUPPORT_H_
