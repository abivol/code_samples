
/* File:    linalg.cu
 *
 * Purpose: linear algebra routines with CUDA
 *
 * Input:
 * Output:  CG output
 * Algorithm: linear algebra algorithms using CUDA routines
 *
 * Note:
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "linalg.h"
#include "timer.h"

// bleargh
#include <cuPrintf.cu>


/*
 * Function:    KERN_Saxpy
 * Purpose:     CUDA kernel for Saxpy
 * Input:		vectors, float
 * Output:		vector
 * Note:
 */
__global__ void KERN_Saxpy(VECTOR x, float a, VECTOR y)
{
	// each thread gets one vector element

	// assumes 1D grid & 1D block
    int pos = blockDim.x * blockIdx.x + threadIdx.x; // vector position

    while(pos < x.size) {
    	y.items[pos] += a * x.items[pos];
    	// skip stride (thread count), cyclic
    	pos += blockDim.x * gridDim.x;
    }
}


// per-block fast shared memory (as float*)
extern __shared__ float s_shm_float[];


/*
 * Function:    DEV_ReduceSharedMem
 * Purpose:     CUDA device function for reduction
 * Input:		block size
 * Output:
 * Note:
 */
__device__ void DEV_ReduceSharedMem(int block_sz)
{
	// sum reduction of floats in shared memory (per block)
	// assumes: block_sz <= 512 (max block size)
	// assumes: block_sz == 2^k (power of 2)
	// TODO: make block_sz constant !!!
	//	const int block_sz = 512;

	unsigned int tid = threadIdx.x;
	if (block_sz >= 512) {
		if (tid < 256) {
			s_shm_float[tid] += s_shm_float[tid + 256];
		}
		__syncthreads();
	}
	if (block_sz >= 256) {
		if (tid < 128) {
			s_shm_float[tid] += s_shm_float[tid + 128];
		}
		__syncthreads();
	}
	if (block_sz >= 128) {
		if (tid < 64) {
			s_shm_float[tid] += s_shm_float[tid + 64];
		} __syncthreads();
	}
	if (tid < 32) {
		if (block_sz >= 64) s_shm_float[tid] += s_shm_float[tid + 32];
		if (block_sz >= 32) s_shm_float[tid] += s_shm_float[tid + 16];
		if (block_sz >= 16) s_shm_float[tid] += s_shm_float[tid + 8];
		if (block_sz >= 8) s_shm_float[tid] += s_shm_float[tid + 4];
		if (block_sz >= 4) s_shm_float[tid] += s_shm_float[tid + 2];
		if (block_sz >= 2) s_shm_float[tid] += s_shm_float[tid + 1];
	}

	// result is in s_shm_float[0]
}


/*
 * Function:    KERN_DotProductShared
 * Purpose:     CUDA kernel for dot product
 * Input:		vectors
 * Output:		vector
 * Note:
 */
__global__ void KERN_DotProductShared(VECTOR v1, VECTOR v2, VECTOR y)
{
	// assumes 1D grid & 1D block
	// assumes shared memory size is blockDim.x floats
	int index = threadIdx.x; // s_shm_float position
    int pos = blockDim.x * blockIdx.x + threadIdx.x; // vector position

    float dp = 0;
    while(pos < v1.size) {
    	dp += v1.items[pos] * v2.items[pos];
    	// skip stride (thread count), cyclic
    	pos += blockDim.x * gridDim.x;
    }
	// update
    s_shm_float[index] = dp;

	// sync before reduction, wait for partial sums to complete
	__syncthreads();

	// reduction
/*
	int stride = blockDim.x / 2;
	while (stride > 0) {
		if (index < stride) {
			s_shm_float[index] += s_shm_float[index + stride];
		}
		__syncthreads();
		stride /= 2;
	}
*/
	DEV_ReduceSharedMem(blockDim.x);

	// save partial sum computed
	if(index == 0) {
		y.items[blockIdx.x] = s_shm_float[0];
	}
}


/*
 * Function:    DEV_SubVector
 * Purpose:     extract the bpos subvector with size bsz
 * Input:		vectors
 * Output:		vector
 * Note:
 */
__device__ VECTOR DEV_SubVector(VECTOR v, int bpos, int bsz)
{
	// extract the bpos subvector with size bsz

	VECTOR s = {0};
	if((bpos + 1) * bsz > v.size) { // overflows?
		s.size = v.size % bsz;
	} else {
		s.size = bsz;
	}
	s.items = &v.items[bpos * bsz];
//	CUTRACE(("sub-vector: bpos: %d, size: %d\n", bpos, s.size));
	return s;
}


/*
 * Function:    DEV_SubMatrix
 * Purpose:     extract the (brow, bcol) submatrix with size bsz x bsz
 * Input:		vectors
 * Output:		vector
 * Note:
 */
__device__ MATRIX DEV_SubMatrix(MATRIX m, int brow, int bcol, int bsz)
{
	// extract the (brow, bcol) submatrix with size bsz x bsz

	// location is undefined / invalid - cannot deallocate a submatrix
	MATRIX s = {0};
	// size
	if((brow + 1) * bsz > m.nrow) { // overflows?
		s.nrow = m.nrow % bsz;
	} else {
		s.nrow = bsz;
	}
	if((bcol + 1) * bsz > m.ncol) { // overflows?
		s.ncol = m.ncol % bsz;
	} else {
		s.ncol = bsz;
	}
	// keep stride the same as parent matrix
	s.stride = m.stride;
	// start items
	s.items = &m.items[m.stride * bsz * brow + bsz * bcol];
//	CUTRACE(("sub-matrix: brow: %d, bcol: %d, nrow: %d, ncol: %d\n",
//		brow, bcol, s.nrow, s.ncol));
	return s;
}


// ceiling of x / y
__device__ int DEV_Ceil(int x, int y)
{
	return (x + y - 1) / y;
}


/*
 * Function:    KERN_MVMultShared
 * Purpose:     kernel for M-V multiplication using submatrix distribution
 * Input:		matrix, vector
 * Output:		vector
 * Note:		adapted from CUDA C dev. guide
 */
__global__ void KERN_MVMultShared(MATRIX m, VECTOR x, VECTOR y)
{
	// assumes 1D grid & 2D block

	// locate submatrix and subvector in shared memory
	const size_t block_dim = blockDim.x;
	float *sh_v = s_shm_float; // subvector start

	// block row and column
	int block_row = blockIdx.x,
		block_col = 0;

	const int nrb = DEV_Ceil(m.nrow, block_dim),
		ncb = DEV_Ceil(m.ncol, block_dim);
//	CUTRACE(("nrb: %d, ncb: %d\n", nrb, ncb));
	int	local_row = 0,
		local_col = 0;
	float val = 0;

	while(block_row < nrb) { // cycle row

		// get target subvector
		VECTOR sub_y = DEV_SubVector(y, block_row, block_dim);

		// sub-matrix (block) coordinates
		local_row = threadIdx.y;
		local_col = threadIdx.x;

		// final vector value
		val = 0;

	//	CUTRACE(("ncb: %d\n", ncb);)
		// loop through column blocks
		for(block_col = 0; block_col < ncb; block_col++) {
//	    	CUTRACE(("block_row: %d, block_col: %d\n", block_row, block_col));
			// get sub-matrix and sub-vector
			MATRIX sub_m = DEV_SubMatrix(m, block_row, block_col, block_dim);
			VECTOR sub_x = DEV_SubVector(x, block_col, block_dim);

			// read sub blocks into shared memory
			sh_v[local_row] = sub_x.items[local_row];
//			CUTRACE(("sh_v[%d]: %g\n", local_row, sh_v[local_row]));

			//
			__syncthreads();

			// serial dot product: sh_m row * sh_v
			for(int i = 0; i < sub_m.ncol; i++) {
				val += sub_m.items[local_row * sub_m.stride + i] * sh_v[i];
			}

			//
			__syncthreads();

	//		CUTRACE(("val: %g\n", val);)
		} // ~ for(block_col = 0; block_col < ncb; block_col++)

		// save result
		sub_y.items[local_row] = val;

		// skip stride (cyclic)
		block_row += gridDim.x; // next row block
    } // ~ while(block_row < nrb)
}


////////////////////////////////////////////////////////////////////////////////


/*
 * Function:    CudaSaxpy
 * Purpose:     Saxpy host routine
 * Input:		vectors
 * Output:		vector
 * Note:
 */
void CudaSaxpy(VECTOR d_x, float a, VECTOR d_y,
	int block_sz, int grid_sz)
{
	cudaError_t err = cudaErrorApiFailureBase;
	// config
	assert(block_sz > 0);
	assert(grid_sz > 0);
	TRACE(("%s(), block size: %d, grid size: %d\n",
		__FUNCTION__, block_sz, grid_sz));

//	printf("vector y:\n");
//	PrintVector(&h_v);

	// launch kernel
	dim3 dimBlock(block_sz);
	dim3 dimGrid(grid_sz);
	KERN_Saxpy<<<dimGrid, dimBlock>>>(d_x, a, d_y);
	err = cudaThreadSynchronize ();
	assert(err == cudaSuccess);

//	printf("vector y:\n");
//	PrintVector(&h_v);
}


// round to upper power of 2
int RoundPow2(int b)
{
	int p = ceil(log2((double)b));
	return (int) pow(2, p);
}


/*
 * Function:    CudaDotProduct
 * Purpose:     DotProduct host routine
 * Input:		vectors
 * Output:		float
 * Note:
 */
float CudaDotProduct(VECTOR d_v1, VECTOR d_v2, VECTOR md_ps, VECTOR mh_ps,
	int block_sz, int grid_sz)
{
	// optimal
/*
	const size_t BLOCK_SIZE = 64,
			GRID_SIZE = 16;
*/

	cudaError_t err = cudaErrorApiFailureBase;
	// config
	assert(block_sz > 0);
	assert(grid_sz > 0);
	// round to upper power of 2
	block_sz = RoundPow2(block_sz);
	TRACE(("%s(), block size: %d, grid size: %d\n",
		__FUNCTION__, block_sz, grid_sz));

	// copy
//	printf("vector v1:\n");
//	PrintVector(&h_v1);
//	printf("vector v2:\n");
//	PrintVector(&h_v2);

	// zero out partial sums
	ZeroVector(md_ps);

	// launch kernel
	dim3 dimBlock(block_sz);
	dim3 dimGrid(grid_sz);
	KERN_DotProductShared<<<dimGrid, dimBlock, sizeof(float) * block_sz>>>(d_v1, d_v2, md_ps);
	err = cudaThreadSynchronize();
	assert(err == cudaSuccess);

//	printf("vector p:\n");
//	PrintVector(&h_p);

	// sum up partial sums
	float *p,
		dp = 0;
	for(p = mh_ps.items; p < mh_ps.items + mh_ps.size; p++) {
		dp += *p;
	}
	TRACE(("dot product: "FFMT"\n", dp));

	return dp;
}


/*
 * Function:    CudaMVMult1
 * Purpose:     M-V Multiplication method 1 host routine
 * Input:		vectors
 * Output:		float
 * Note:		uses dot product
 */
void CudaMVMult1(MATRIX d_m, VECTOR d_x, VECTOR md_y, VECTOR mh_y,
	VECTOR md_ps, VECTOR mh_ps,
	const cudaDeviceProp &prop,
	int block_sz, int grid_sz)
{
	// optimal
/*
	const size_t BLOCK_SIZE = 64,
			GRID_SIZE = 16;
*/
	cudaError_t err = cudaErrorApiFailureBase;
	// config
	assert(block_sz > 0);
	assert(grid_sz > 0);
	// round to upper power of 2
	block_sz = RoundPow2(block_sz);
	// check available shared mem
	assert(block_sz * sizeof(float) <= prop.sharedMemPerBlock); // partial sums
	// check n
	int n = d_x.size;
	if(grid_sz * block_sz > n) { // chop grid off
		grid_sz = ceil((float)n / (float)block_sz);
	}
	TRACE(("%s(), block size: %d, grid size: %d\n",
		__FUNCTION__, block_sz, grid_sz));

//	printf("matrix m:\n");
//	PrintMatrix(&h_m);
//	printf("vector x:\n");
//	PrintVector(&h_v);

	dim3 dimBlock(block_sz);
	dim3 dimGrid(grid_sz);
	VECTOR d_row = {0};
	float *p,
		dp = 0;
	for(size_t r = 0; r < d_m.nrow; r++) { // matrix rows
		d_row.items = d_m.items + d_m.stride * r;
		d_row.size = d_m.ncol;
		// zero out partial sums
		ZeroVector(md_ps);
		// launch kernel
		KERN_DotProductShared<<<dimGrid, dimBlock, sizeof(float) * block_sz>>>(d_row, d_x, md_ps);
		err = cudaThreadSynchronize();
		assert(err == cudaSuccess);
		// sum up partial sums
		dp = 0;
		for(p = mh_ps.items; p < mh_ps.items + mh_ps.size; p++) { // mapped
			dp += *p;
		}
		// save result
		 mh_y.items[r] = dp; // mapped
//		TRACE(("dp: %g\n", dp));
	}

	TRACE(("vector y: "FFMT"\n", Norm(mh_y)));
}


/*
 * Function:    CudaMVMult2
 * Purpose:     M-V Multiplication method 2 host routine
 * Input:		vectors
 * Output:		float
 * Note:		uses submatrix
 */
void CudaMVMult2(MATRIX d_m, VECTOR d_x, VECTOR d_y,
	const cudaDeviceProp &prop,
	int block_sz, int grid_sz)
{
	cudaError_t err = cudaErrorApiFailureBase;

	// config
	assert(block_sz > 0);
	assert(grid_sz > 0);

	// compute sizes
	int block_dim = ceil(sqrt(block_sz)); // in threads
	// check available shared mem
	if(block_dim * (block_dim + 1) * sizeof(float) > prop.sharedMemPerBlock) {
		block_dim = sqrt(prop.sharedMemPerBlock / sizeof(float)) - 1;
	}
	int grid_dim = prop.maxGridSize[0];
	// check submatrix size
	int n = d_x.size;
	if(block_dim * block_dim * grid_dim > n) { // chop grid
		grid_dim = ceil((float) n / (float)(block_dim * block_dim));
	}
	if(block_dim * block_dim > n) { // chop block
		block_dim = ceil(sqrt(n));
		grid_dim = 1;
	}
	TRACE(("block_dim: %d, grid_dim: %d\n", block_dim, grid_dim));

//	printf("matrix m:\n");
//	PrintMatrix(&h_m);
//	printf("vector x:\n");
//	PrintVector(&h_v);

	// launch kernel
	dim3 dimBlock(block_dim, block_dim);
	dim3 dimGrid(grid_dim);
	KERN_MVMultShared<<<dimGrid, dimBlock, sizeof(float) * block_dim>>>(d_m, d_x, d_y);
	err = cudaThreadSynchronize();
	assert(err == cudaSuccess);

	TRACE(("vector y: \n"));
//	TraceDevVector(d_y);
}


////////////////////////////////////////////////////////////////////////////////

// Unit Tests


void UT_Saxpy(const cudaDeviceProp *prop, int n = 0)
{
	cudaError_t err = cudaErrorApiFailureBase;
	float *p = NULL,
			q = 0;

	// compute sizes
	bool test = (n == 0); // test or benchmark?
	if(test) {
		n = 7;
	}
	int block_sz = prop->maxThreadsPerBlock, // in threads (per block)
		grid_sz = prop->maxGridSize[0]; // in blocks (per grid)
	// check available shared mem
	if(grid_sz * sizeof(float) > prop->sharedMemPerBlock) {
		grid_sz = ceil((float)prop->sharedMemPerBlock / sizeof(float));
	}
	printf("block_sz: %d, grid_sz: %d\n", block_sz, grid_sz);

	VECTOR h_x = CreateVector(n, LOC_HOST),
		h_y = CreateVector(n, LOC_HOST);;
	VECTOR d_x = CreateVector(n, LOC_DEVICE),
		d_y = CreateVector(n, LOC_DEVICE);

	// fill
	if(test) {
		q = 3.3;
		for(p = h_x.items; p < h_x.items + h_x.size; p++) {
			*p = q;
			q += 1;
		}

		q = 2.2;
		for(p = h_y.items; p < h_y.items + h_y.size; p++) {
			*p = q;
			q += 2;
		}
	} else {
		// random
		RandomVector(h_x);
		RandomVector(h_y);
	}

//	TRACE(("vector x:\n"));
//	TraceVector(&h_x);
//	TRACE(("vector y:\n"));
//	TraceVector(&h_y);

	err = cudaMemcpy(d_x.items, h_x.items, sizeof(float) * h_x.size, cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);
	err = cudaMemcpy(d_y.items, h_y.items, sizeof(float) * h_y.size, cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);

	float a = 1.3;
	TRACE(("float a: "FFMT"\n", a));

	// call cuda
	double start, finish, elapsed;
	GET_TIME(start);
	CudaSaxpy(d_x, a, d_y, block_sz, grid_sz);
	GET_TIME(finish);
	elapsed = finish - start;
	printf("elapsed: "FFMT"\n", elapsed);

	// copy result
	err = cudaMemcpy(h_y.items, d_y.items, sizeof(float) * h_y.size, cudaMemcpyDeviceToHost);
	assert(err == cudaSuccess);

	TRACE(("vector y: "FFMT"\n", Norm(h_y)));
	if(n < 100) {
		TraceVector(&h_y);
	}

	// expected:
	// 6.4900    9.7900   13.0900   16.3900   19.6900   22.9900   26.2900

	// free
	DestroyVector(&h_x);
	DestroyVector(&h_y);
	DestroyVector(&d_x);
	DestroyVector(&d_y);
}


void UT_DotProductShared(const cudaDeviceProp *prop, int n = 0)
{
	// optimal
/*
	const size_t BLOCK_SIZE = 64,
			GRID_SIZE = 16;
*/

	cudaError_t err = cudaErrorApiFailureBase;
	float *p = NULL,
		q = 0;

	// compute sizes
	bool test = (n == 0); // test or benchmark?
	if(test) {
		n = 7;
	}
	int block_sz = prop->maxThreadsPerBlock, // in threads (per block)
		grid_sz = prop->maxGridSize[0]; // in blocks (per grid)
	// check available shared mem
	if(grid_sz * sizeof(float) > prop->sharedMemPerBlock) {
		grid_sz = ceil((float)prop->sharedMemPerBlock / sizeof(float));
	}
	printf("block_sz: %d, grid_sz: %d\n", block_sz, grid_sz);

   // allocate partial sum vector in mapped mem
	VECTOR mh_ps = {0},
		md_ps = {0};
	CreateMappedVector(grid_sz, mh_ps, md_ps);

	VECTOR h_v1 = CreateVector(n, LOC_HOST),
		h_v2 = CreateVector(n, LOC_HOST),
		d_v1 = CreateVector(n, LOC_DEVICE),
		d_v2 = CreateVector(n, LOC_DEVICE);

	// fill
	if(test) {
		q = 3.3;
		for(p = h_v1.items; p < h_v1.items + h_v1.size; p++) {
			*p = q;
			q += 2;
		}

		q = 2.2;
		for(p = h_v2.items; p < h_v2.items + h_v2.size; p++) {
			*p = q;
			q += 2;
		}
	} else {
		// random
		RandomVector(h_v1);
		RandomVector(h_v2);
	}

	// copy
//	printf("vector v1:\n");
//	PrintVector(&h_v1);
	err = cudaMemcpy(d_v1.items, h_v1.items, sizeof(float) * h_v1.size, cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);
//	printf("vector v2:\n");
//	PrintVector(&h_v2);
	err = cudaMemcpy(d_v2.items, h_v2.items, sizeof(float) * h_v2.size, cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);

	// call cuda
	double start, finish, elapsed;
	GET_TIME(start);
	float dp = CudaDotProduct(d_v1, d_v2, md_ps, mh_ps, block_sz, grid_sz);
	GET_TIME(finish);
	elapsed = finish - start;
	printf("elapsed: "FFMT"\n", elapsed);

	printf("dot product: "FFMT"\n", dp);

	// expected: 645.82

	// free
	DestroyVector(&h_v1);
	DestroyVector(&h_v2);
	DestroyVector(&d_v1);
	DestroyVector(&d_v2);
	DestroyVector(&mh_ps);
//	DestroyVector(&md_ps);  // not allocated
}


void UT_MVMult2(const cudaDeviceProp *prop, int n = 0)
{
	// optimal
//	const size_t BLOCK_SIZE = 90; // so that block_dim == 8

	cudaError_t err = cudaErrorApiFailureBase;
	float *p = NULL,
			q = 0;

	// compute sizes
	bool test = (n == 0); // test or benchmark?
	if(test) {
		n = 7;
	}

	// allocate
	MATRIX h_m = CreateMatrix(n, n, LOC_HOST);
	VECTOR h_x = CreateVector(n, LOC_HOST),
		h_y = CreateVector(n, LOC_HOST);
	MATRIX d_m = CreateMatrix(n, n, LOC_DEVICE);
	VECTOR d_x = CreateVector(n, LOC_DEVICE),
		d_y = CreateVector(n, LOC_DEVICE);

	// fill
	if(test) {
		// input matrix
		q = 3.3;
		for(p = h_m.items; p < h_m.items + h_m.nrow * h_m.stride; p++) {
			*p = q;
			q += 3;
		}

		// input vector
		q = 2.2;
		for(p = h_x.items; p < h_x.items + h_x.size; p++) {
			*p = q;
			q += 1;
		}
	} else {
		// random
		RandomMatrix(h_m);
		RandomVector(h_x);
	}

//	printf("matrix m:\n");
//	PrintMatrix(&h_m);
	err = cudaMemcpy(d_m.items, h_m.items, sizeof(float) * h_m.nrow * h_m.stride, cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);

//	printf("vector x:\n");
//	PrintVector(&h_v);
	err = cudaMemcpy(d_x.items, h_x.items, sizeof(float) * d_x.size, cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);

	double start, finish, elapsed;
	GET_TIME(start);
	// call cuda
	CudaMVMult2(d_m, d_x, d_y, *prop,
		prop->maxThreadsPerBlock, prop->maxGridSize[0]);
	GET_TIME(finish);
	elapsed = finish - start;
	printf("elapsed: "FFMT"\n", elapsed);

	// copy result
	err = cudaMemcpy(h_y.items, d_y.items, sizeof(float) * h_y.size, cudaMemcpyDeviceToHost);
	assert(err == cudaSuccess);

	printf("result vector y: "FFMT"\n", Norm(h_y));
	TraceVector(&h_y);

	// check
	ZeroVector(h_y);
	MVMult(h_m, h_x, h_y);
	printf("expected vector y: "FFMT"\n", Norm(h_y));
	TraceVector(&h_y);

	// expected:
	// 531.72   1296.12   2060.52   2824.92   3589.32   4353.72   5118.12

	// free
	DestroyMatrix(&h_m);
	DestroyVector(&h_x);
	DestroyVector(&h_y);
	DestroyMatrix(&d_m);
	DestroyVector(&d_x);
	DestroyVector(&d_y);
}


// MVMult via DotProductShared
void UT_MVMult1(const cudaDeviceProp *prop, int n = 0)
{
	// optimal
/*
	const size_t BLOCK_SIZE = 64,
			GRID_SIZE = 16;
*/

	cudaError_t err = cudaErrorApiFailureBase;
	float *p = NULL,
			q = 0;

	// compute sizes
	bool test = (n == 0); // test or benchmark?
	if(test) {
		n = 7;
	}

	// allocate
	MATRIX h_m = CreateMatrix(n, n, LOC_HOST),
			d_m = CreateMatrix(n, n, LOC_DEVICE);
	VECTOR h_x = CreateVector(n, LOC_HOST),
		d_x = CreateVector(n, LOC_DEVICE);

	// allocate partial sum vector in mapped mem
	int block_sz = prop->maxThreadsPerBlock;
	int grid_sz = prop->maxGridSize[0];
	if(grid_sz * block_sz > n) { // chop grid off
		grid_sz = ceil((float)n / (float)block_sz);
	}
	VECTOR mh_ps = {0},
		md_ps = {0};
	CreateMappedVector(grid_sz, mh_ps, md_ps);

   // allocate result vector in mapped mem
	VECTOR mh_y = {0},
		md_y = {0};
	CreateMappedVector(n, mh_y, md_y);

	if(test) {
		// input matrix
		q = 3.3;
		for(p = h_m.items; p < h_m.items + h_m.nrow * h_m.stride; p++) {
			*p = q;
			q += 3;
		}

		// input vector
		q = 2.2;
		for(p = h_x.items; p < h_x.items + h_x.size; p++) {
			*p = q;
			q += 1;
		}
	} else {
		// random
		RandomMatrix(h_m);
		RandomVector(h_x);
	}

//	printf("matrix m:\n");
//	PrintMatrix(&h_m);
	err = cudaMemcpy(d_m.items, h_m.items, sizeof(float) * h_m.nrow * h_m.stride, cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);

//	printf("vector x:\n");
//	PrintVector(&h_x);
	err = cudaMemcpy(d_x.items, h_x.items, sizeof(float) * h_x.size, cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);

	double start, finish, elapsed;
	GET_TIME(start);
	// call cuda
	CudaMVMult1(d_m, d_x, md_y, mh_y, md_ps, mh_ps, *prop,
		block_sz, grid_sz);
	GET_TIME(finish);
	elapsed = finish - start;
	printf("elapsed: "FFMT"\n", elapsed);

	printf("result vector y: "FFMT"\n", Norm(mh_y));
	TraceVector(&mh_y);

	// check
	ZeroVector(mh_y);
	MVMult(h_m, h_x, mh_y);
	printf("expected vector y: "FFMT"\n", Norm(mh_y));
	TraceVector(&mh_y);

	// expected:
	// 531.72   1296.12   2060.52   2824.92   3589.32   4353.72   5118.12

	// free
	DestroyMatrix(&h_m);
	DestroyMatrix(&d_m);
	DestroyVector(&h_x);
	DestroyVector(&d_x);
	DestroyVector(&mh_ps);
	DestroyVector(&mh_y);
}


////////////////////////////////////////////////////////////////////////////////


int main_linalg(int argc, char **argv)
{
	const int SR = 1;
	cudaError_t err = cudaErrorApiFailureBase;

	// init
	cudaSetDevice(0);
	err = cudaSetDeviceFlags(cudaDeviceMapHost);
	assert(err == cudaSuccess);
	cudaPrintfInit();

	// device detection
	cudaDeviceProp prop = {0};
	err = cudaGetDeviceProperties(&prop, 0);
	assert(err == cudaSuccess);

	int n = 0;
	// matrix order
	if(argc > 1) {
		n = atoi(argv[1]);
	}

	// block size
	if(argc > 2) {
		int q = atoi(argv[2]);
		prop.maxThreadsDim[0] = prop.maxThreadsDim[1] = sqrt(q);
		prop.maxThreadsDim[2] = 1;
		prop.maxThreadsPerBlock = q;
	}
	// grid size
	if(argc > 3) {
		int q = atoi(argv[3]);
		prop.maxGridSize[0] = q;
		prop.maxGridSize[1] = prop.maxGridSize[2] = 1;
	}

	printf("cudaGetDeviceProperties: compute: %d.%d, maxGridSize: (%d, %d, %d), "
			"maxThreadsDim: (%d, %d, %d), maxThreadsPerBlock: %d, "
			"sharedMemPerBlock: %lu, multiProcessorCount: %d, "
			"name: \"%s\"\n",
			prop.major, prop.minor,
			prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2],
			prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2],
			prop.maxThreadsPerBlock,
			prop.sharedMemPerBlock,	prop.multiProcessorCount,
			prop.name
			);

//	printf("UT_Saxpy\n");
//	srandom(SR);
//	UT_Saxpy(&prop, n);
//
//	//	UT_DotProductAtomic(&prop);
//
//	printf("UT_DotProductShared\n");
//	srandom(SR);
//	UT_DotProductShared(&prop, n);

//	UT_MVMultAtomic(&prop);

//	printf("UT_MVMultShared\n");
//	srandom(SR);
//	UT_MVMultShared(&prop, n);
//
	printf("UT_MVMult1\n");
	srandom(SR);
	UT_MVMult1(&prop, n);

	printf("UT_MVMult2\n");
	srandom(SR);
	UT_MVMult2(&prop, n);

	cudaPrintfDisplay(stdout, true);
	cudaPrintfEnd();

	return 0;
}
