#ifndef LINALG_H_
#define LINALG_H_


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "support.h"

void CudaSaxpy(VECTOR d_x, float a, VECTOR d_y,
	int block_sz = 0, int grid_sz = 0);

float CudaDotProduct(VECTOR d_v1, VECTOR d_v2, VECTOR md_ps, VECTOR mh_ps,
	int block_sz = 0, int grid_sz = 0);

void CudaMVMult1(MATRIX d_m, VECTOR d_x, VECTOR md_y, VECTOR mh_y,
	VECTOR md_ps, VECTOR mh_ps,
	const cudaDeviceProp &prop,
	int block_sz = 0, int grid_sz = 0);

void CudaMVMult2(MATRIX d_m, VECTOR d_x, VECTOR d_y,
	const cudaDeviceProp &prop,
	int block_sz = 0, int grid_sz = 0);


#endif // LINALG_H_
