
# benchmark command; forces 2000 iterations @ n = 128

# method 1 (shared memory - dot product)
./prog4 128 1e-38 2000 y y 0 0 1

# method 2 (shared memory - M-V multiplication)
./prog4 128 1e-38 2000 y y 0 0 2
