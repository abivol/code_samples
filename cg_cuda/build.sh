
declare CUDA_ROOT=/usr/local/cuda
declare LD_LIBRARY_PATH=$CUDA_ROOT/lib64:$CUDA_ROOT/lib:$LD_LIBRARY_PATH

declare SDK_ROOT=/usr/local/NVIDIA_GPU_Computing_SDK
nvcc -o prog4 -arch sm_11 -g -O3 --use_fast_math -I $SDK_ROOT/shared/inc -I $SDK_ROOT/C/common/inc -I $SDK_ROOT/C/src/simplePrintf support.cu linalg.cu cg.cu

