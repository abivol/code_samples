declare CUDA_ROOT=~/cuda

declare PATH=$PATH:$CUDA_ROOT/bin
echo PATH: $PATH
declare LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CUDA_ROOT/lib64:$CUDA_ROOT/lib
echo LD_LIBRARY_PATH: $LD_LIBRARY_PATH

nvcc -o prog4 -arch sm_11 -g -G -O3 --use_fast_math --ptxas-options=-v -I $CUDA_ROOT/NVIDIA_GPU_Computing_SDK/shared/inc -I $CUDA_ROOT/NVIDIA_GPU_Computing_SDK/C/common/inc -I $CUDA_ROOT/NVIDIA_GPU_Computing_SDK/C/src/simplePrintf support.cu linalg.cu cg.cu

