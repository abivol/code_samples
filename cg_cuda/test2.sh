
# Usage: prog4 o t i [s] [r] [bs] [gs] [m] [...]

# method 1 (shared memory - dot product)
./prog4 13 1e-18 50 y y 0 0 1

# method 2 (shared memory - M-V multiplication)
./prog4 13 1e-18 50 y y 0 0 2
