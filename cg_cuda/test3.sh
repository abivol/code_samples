
# Usage: prog4 o t i [s] [r] [bs] [gs] [m] [...]

# method 1 (shared memory - dot product)
./prog4 11 1e-18 50 y n 0 0 1 <input.txt

# method 2 (shared memory - M-V multiplication)
./prog4 11 1e-18 50 y n 0 0 2 <input.txt

