
/* File:    support.c
 *
 * Purpose:
 * Matrix and vector primitives
 *
 * Input:   []
 * Output:  []
 *
 * Algorithm:
 *
 * Note:
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "support.h"


// create host or device vector
VECTOR CreateVector(size_t size, size_t loc)
{
	VECTOR v = {0};
	v.loc = loc;
	v.size = size;
	size_t sz = size * sizeof(float);
	cudaError_t err = cudaErrorApiFailureBase;
	switch(loc) {
		case LOC_HOST:
			v.items = (float*) malloc(sz);
			break;
		case LOC_DEVICE:
			err = cudaMalloc(&v.items, sz);
			assert(err == cudaSuccess);
			break;
		case LOC_MAPPED_HOST:
			err = cudaHostAlloc(&v.items, sz, cudaHostAllocMapped);
			assert(err == cudaSuccess);
			break;
		case LOC_MAPPED_DEV:
			// no alloc
			break;
		default:
			assert(0 && "Invalid location type!");
			break;
	}
	// zero out mem
	ZeroVector(v);

	return v;
}


// create mapped vectors
void CreateMappedVector(size_t size,
	VECTOR &mh_v /* host */,
	VECTOR &md_v  /* device */)
{
	assert(mh_v.size == 0 && md_v.size == 0);
	assert(mh_v.loc == LOC_NONE && md_v.loc == LOC_NONE);
	cudaError_t err = cudaErrorApiFailureBase;
   // allocate vector in mapped mem
	mh_v = CreateVector(size, LOC_MAPPED_HOST);
	md_v.size = mh_v.size;
	md_v.loc = LOC_MAPPED_DEV;
	err = cudaHostGetDevicePointer(&md_v.items, mh_v.items, 0); // get device pointer
	assert(err == cudaSuccess);
}


void ZeroVector(VECTOR v)
{
	cudaError_t err = cudaErrorApiFailureBase;
	size_t sz = v.size * sizeof(float);
	switch(v.loc) {
		case LOC_HOST:
			// fallthrough
		case LOC_MAPPED_HOST:
			memset(v.items, 0, sz);
			break;
		case LOC_DEVICE:
			// fallthrough
		case LOC_MAPPED_DEV:
			err = cudaMemset(v.items, 0, sz);
			assert(err == cudaSuccess);
			break;
		default:
			assert(0 && "Invalid location type!");
			break;
	}
}


// destroy host or device vector
void DestroyVector(PVECTOR v)
{
	assert(v->loc != LOC_NONE);
	assert(v->size > 0);
	assert(v->items != NULL);
	switch(v->loc) {
		case LOC_HOST:
			free(v->items);
			break;
		case LOC_DEVICE:
			cudaFree(v->items);
			break;
		case LOC_MAPPED_HOST:
			cudaFreeHost(v->items);
			break;
		case LOC_MAPPED_DEV:
			break;
		default:
			assert(0 && "Invalid location type!");
			break;
	}
	v->loc = LOC_NONE;
	v->size = 0;
	v->items = NULL;
}


// create host or device matrix
MATRIX CreateMatrix(size_t nrow, size_t ncol, size_t loc)
{
	MATRIX m = {0};
	size_t sz = nrow * ncol;
	m.loc = loc;
	m.ncol = m.stride = ncol;
	m.nrow = nrow;
	cudaError_t err = cudaErrorApiFailureBase;
	switch(loc) {
		case LOC_HOST:
			m.items = (float*) calloc(sz, sizeof(float));
			break;
		case LOC_DEVICE:
			sz *= sizeof(float);
			err = cudaMalloc(&m.items, sz);
			assert(err == cudaSuccess);
			err = cudaMemset(m.items, 0, sz);
			assert(err == cudaSuccess);
			break;
		default:
			assert(0 && "Invalid location type!");
			break;
	}

	return m;
}


// destroy host or device matrix
void DestroyMatrix(PMATRIX m)
{
	assert(m->loc != LOC_NONE);
	assert(m->nrow > 0 && m->ncol > 0);
	assert(m->items != NULL);
	switch(m->loc) {
		case LOC_HOST:
			free(m->items);
			break;
		case LOC_DEVICE:
			cudaFree(m->items);
			break;
		default:
			assert(0 && "Invalid location type!");
			break;
	}
	m->loc = 0;
	m->ncol = 0;
	m->nrow = 0;
	m->items = NULL;
}


/*
static inline int RandomInt(int min, int max)
{
	assert(min <= max);
	// y = a * x + b
	float a = (float)(max - min) / (float)(RAND_MAX - 0),
			b = min;
	int x = random(),
			y = round(a * x + b);
	assert(y >= min && y <= max);
	return y;
}
*/


static inline float RandomFloat()
{
/*
	int sign = (random() > RAND_MAX / 2) ? 1 : -1; // 50% positive
    float x = sign * 10 * (float)random() / (float)RAND_MAX;
    int p = RandomInt(-4, 4);
//    TRACE(("%s() x: %g, p: %d\n",__FUNCTION__, x, p));
    return x * pow(10.0, p);
*/

	return random() / ((double) RAND_MAX);
}


// generate random vector into v
void RandomVector(VECTOR v)
{
	// compute
	for(float *p = v.items; p < v.items + v.size; p++) {
		*p = RandomFloat();
	}
}


// generate random matrix into m
void RandomMatrix(MATRIX m)
{
	for(float *p = m.items; p < m.items + m.nrow * m.ncol; p++) {
		*p = RandomFloat();
	}
}


float DotProduct(VECTOR v1, VECTOR v2)
{
	assert(v1.size == v2.size);
	float dp = 0.0;
	for(size_t i = 0; i < v1.size; i++) {
		dp += *(v1.items + i) * *(v2.items + i);
	}

	return dp;
}


float Norm(VECTOR v)
{
	return sqrt(DotProduct(v, v));
}


// transpose matrix m1 into m2
void TransposeMatrix(MATRIX m1, MATRIX m2)
{
	assert(m1.nrow == m2.ncol && m1.ncol == m2.nrow);
	size_t i = 0,
		j = 0;
	// compute
	for(i = 0; i < m1.nrow; i++) { // rows
		for(j = 0; j < m1.ncol; j++) { // cols
			m2.items[m2.stride * j + i] = m1.items[m1.stride * i + j];
		}
	}
}


// y = y + x * alpha
void Saxpy(VECTOR x, float a, VECTOR y)
{
	assert(x.size == y.size);
	size_t i = 0,
		n = x.size;
	for(i = 0; i < n; i++) {
		y.items[i] += x.items[i] * a;
	}
}


void MVMult(MATRIX A, VECTOR x, VECTOR y)
{
	size_t i = 0,
		j = 0,
		m = A.nrow,
		n = A.ncol;
	// init destination
	for(i = 0; i < m; i++) { // rows
		y.items[i] = 0;
	}
	// compute
	for(i = 0; i < m; i++) { // rows
		for(j = 0; j < n; j++) { // cols
			y.items[i] += A.items[A.stride * i + j] * x.items[j];
//			TRACE(("i: %d, j: %d, fa: %f, fx: %f, fy: %f\n", i, j, *fa, *fx, *fy));
		}
	}
}


// serial matrix multiplication: A * B = C
void MMMult(MATRIX A, MATRIX B, MATRIX C)
{
	assert(A.ncol == B.nrow);
	assert(A.nrow == C.nrow);
	assert(B.ncol == C.ncol);
	size_t r, c, i,
		m = A.nrow,
		n = A.ncol,
		p = B.ncol;
	// compute dot products
	for(r = 0; r < m; r++) { // C rows
		for(c = 0; c < p; c++) { // C cols
			// dot product A row x B col
			C.items[C.stride * r + c] = 0;
			for(i = 0; i < n; i++) { //
				C.items[C.stride * r + c] +=
					A.items[A.stride * r + i] * B.items[B.stride * i + c];
			}
		}
	}
}


void CopyVector(VECTOR dst, VECTOR src)
{
	// only device to device!
	assert(dst.loc == LOC_DEVICE);
	assert(src.loc == LOC_DEVICE);
	assert(dst.size = src.size);
	cudaError_t err = cudaErrorApiFailureBase;
	err = cudaMemcpy(dst.items, src.items, sizeof(float) * src.size, cudaMemcpyDeviceToDevice);
	assert(err == cudaSuccess);
}

////////////////////////////////////////////////////////////////////////////////


void PrintFloat(float f)
{
	printf(FFMT" ", f);
}

void PrintMatrix(const PMATRIX m)
{
	size_t i = 0,
		j = 0;
	float f = 0;
	for(i = 0; i < m->nrow; i++) {
		for(j = 0; j < m->ncol; j++) {
			f = m->items[i * m->ncol + j];
			PrintFloat(f);
		}
		printf("\n");
	}
}


void TraceMatrix(const PMATRIX m)
{
#ifdef TRACE_ON
	PrintMatrix(m);
#endif // TRACE_ON
}


void PrintVector(const PVECTOR v)
{
	size_t i = 0;
	float f = 0;
	for(i = 0; i < v->size; i++) {
		f = v->items[i];
		PrintFloat(f);
	}
	printf("\n");
}


void TraceVector(const PVECTOR v)
{
#ifdef TRACE_ON
	PrintVector(v);
#endif // TRACE_ON
}


void PrintDevVector(VECTOR d_v)
{
	cudaError_t err = cudaErrorApiFailureBase;
	VECTOR h_v = CreateVector(d_v.size, LOC_HOST);
	err = cudaMemcpy(h_v.items, d_v.items, sizeof(float) * h_v.size, cudaMemcpyDeviceToHost);
	assert(err == cudaSuccess);
	PrintVector(&h_v);
	DestroyVector(&h_v);
}


void TraceDevVector(VECTOR d_v)
{
#ifdef TRACE_ON
	PrintDevVector(d_v);
#endif // TRACE_ON
}

////////////////////////////////////////////////////////////////////////////////


// tests
int main_support()
{
	return 0;

}
