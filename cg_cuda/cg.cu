
/* File:    cg.cu
 *
 * Purpose: main CG procedure and associated routines
 *
 * Input:   CG input
 * 		command line options, in order:
 * 		- The order of the system,
 * 		- The tolerance,
 * 		- The maximum number of iterations, and
 * 		- An optional fourth argument that will suppress output of the solution vector
 * 		The input to stdin will consist of, in order:
 * 		- The matrix A, and
 * 		- The right-hand side b
 *
 * Output:  CG output
 * 		output is printed to stdout:
 * 		- The number of iterations,
 * 		- The time used by the solver (not including I/O),
 * 		- The solution to the linear system,
 * 		- The norm of the residual calculated by the conjugate gradient method, and
 * 		- The norm of the residual calculated directly from the definition of residual.
 *
 * Algorithm: CG algorithm using CUDA routines
 *
 * Note:
 * see README for details
 * build: use build.sh (Linux)
 *
 */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include<memory.h>
#include <math.h>

#include <cuPrintf.cuh>

#include "support.h"
#include "linalg.h"
#include "timer.h"


// CG parameters
typedef struct {
	int order;
	float tolerance;
	int iterations;
	bool suppress;
	int block_sz;
	int grid_sz;
	int mvm_method; // MV mult. method
	double elapsed;
	cudaDeviceProp dev_prop;
	MATRIX h_A;
	VECTOR h_b;
	VECTOR h_x;
	MATRIX d_A;
	VECTOR d_b;
	VECTOR d_x;
} CG_PARAMS, *PCG_PARAMS;


static CG_PARAMS g_cgp = {
	8, // matrix order
	1.0e-06, // tolerance
	1.0e04, // iterations
	false, // suppress
	BLOCK_SIZE, //
	GRID_SIZE, //
	2, // mvm_method
	0, // elapsed
	// other structures..
};


/*
 * Function:    CreateData
 * Purpose:     allocate CG host and device structures
 * Input:
 * Output:
 * Note:
 */
void CreateData(size_t n)
{
	// create host structures
	g_cgp.h_A = CreateMatrix(n, n, LOC_HOST);
	g_cgp.h_b = CreateVector(n, LOC_HOST);
	g_cgp.h_x = CreateVector(n, LOC_HOST);

	// create device structures
	g_cgp.d_A = CreateMatrix(n, n, LOC_DEVICE);
	g_cgp.d_b = CreateVector(n, LOC_DEVICE);
	g_cgp.d_x = CreateVector(n, LOC_DEVICE);
}


/*
 * Function:    CopyData
 * Purpose:     copy CG host data to device
 * Input:
 * Output:
 * Note:
 */
void CopyData()
{
	// copy to device
	cudaError_t err = cudaErrorApiFailureBase;
	err = cudaMemcpy(g_cgp.d_A.items, g_cgp.h_A.items,
		sizeof(float) * g_cgp.h_A.nrow * g_cgp.h_A.stride,
		cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);
	err = cudaMemcpy(g_cgp.d_b.items, g_cgp.h_b.items,
		sizeof(float) * g_cgp.h_b.size,
		cudaMemcpyHostToDevice);
	assert(err == cudaSuccess);
}


/**
 * Function:    InputData
 * Purpose:     read input
 * Input:
 * Output:
 */
void InputData()
{
	int n = g_cgp.order,
		i = 0,
		j = 0;

	// create CG data
	CreateData(n);

	// input data
	for(i = 0; i < n; i++) {
		for(j = 0; j < n; j++) {
			(void) scanf("%e", &g_cgp.h_A.items[i * g_cgp.h_A.stride + j]);
		}
	}
	TRACE(("A:\n"));
	TraceMatrix(&g_cgp.h_A);
	for(j = 0; j < n; j++) {
		(void) scanf("%e", &g_cgp.h_b.items[j]);
	}
	TRACE(("b:\n"));
	TraceVector(&g_cgp.h_b);

	// copy to device
	CopyData();
}


/**
 * Function:    OutputData
 * Purpose:     write output
 * Input:
 * Output:
 */
void OutputData()
{
	printf("number of iterations: %d\n", g_cgp.iterations);
	printf("time used by the solver: "FFMT"\n", g_cgp.elapsed);
	if(! g_cgp.suppress) {
		printf("solution to the linear system:\n");
		PrintVector(&g_cgp.h_x);
	}
	printf("The square of the norm of the residual calculated by the conjugate gradient method: "
		FFMT"\n", g_cgp.tolerance);
	// compute norm of residual r = b - Ax
	VECTOR h_r = CreateVector(g_cgp.h_x.size, LOC_HOST);
	MVMult(g_cgp.h_A, g_cgp.h_x, h_r); // r = Ax
	Saxpy(h_r, -1, g_cgp.h_b); // b -= Ax
	float norm = Norm(g_cgp.h_b);
	DestroyVector(&h_r);
	printf("The square of the norm of the residual calculated from the definition: "FFMT"\n", norm * norm);
}


/**
 * Function:    DestroyData
 * Purpose:     free allocated structures
 * Input:
 * Output:
 */
void DestroyData()
{
	// host
	DestroyMatrix(&g_cgp.h_A);
	DestroyVector(&g_cgp.h_b);
	DestroyVector(&g_cgp.h_x);
	// device
	DestroyMatrix(&g_cgp.d_A);
	DestroyVector(&g_cgp.d_b);
	DestroyVector(&g_cgp.d_x);
	memset(&g_cgp, 0, sizeof(CG_PARAMS));
}


/*
 * Function:    GenerateData
 * Purpose:     generate random CG data
 * Input:
 * Output:
 * Note:
 */
void GenerateData()
{
	int n = g_cgp.order;

	// create local structures
	MATRIX h_m1 = CreateMatrix(n, n, LOC_HOST),
		h_m2 = CreateMatrix(n, n, LOC_HOST);

	// create CG data
	CreateData(n);

	// generate random data

	srandom(1);

	RandomMatrix(h_m1);
	TransposeMatrix(h_m1, h_m2);
//	TRACE(("m1:\n"));
//	TraceMatrix(&h_m1);
//	TRACE(("m2:\n"));
//	TraceMatrix(&h_m2);

	MMMult(h_m1, h_m2, g_cgp.h_A);
//	TRACE(("A:\n"));
//	TraceMatrix(&g_cgp.h_A);
	printf("A:\n");
	PrintMatrix(&g_cgp.h_A);

	RandomVector(g_cgp.h_x);
//	TRACE(("x:\n"));
//	TraceVector(&g_cgp.h_x);
	printf("x:\n");
	PrintVector(&g_cgp.h_x);

	MVMult(g_cgp.h_A, g_cgp.h_x, g_cgp.h_b);
//	TRACE(("b:\n"));
//	TraceVector(&g_cgp.h_b);
	printf("b:\n");
	PrintVector(&g_cgp.h_b);

	// copy to device
	CopyData();

	// free locals
	DestroyMatrix(&h_m1);
	DestroyMatrix(&h_m2);

	// fill parameters
//	g_cgp.tolerance = 0.03;
//	g_cgp.iterations = 100000;
}


/**
 * Function:    ConjugateGradient
 * Purpose:     implements CG method
 * Input:
 * Output:
 */
bool ConjugateGradient()
{
	// params
	MATRIX d_A = g_cgp.d_A;
	VECTOR d_b = g_cgp.d_b;
	VECTOR d_x = g_cgp.d_x;
	const float tolerance = g_cgp.tolerance;
	const int max_iter = g_cgp.iterations;
	TRACE(("tolerance: "FFMT", max_iter: %d\n", tolerance, max_iter));
	printf("block size: %d, grid size: %d\n", g_cgp.block_sz, g_cgp.grid_sz);

	// locals
	float norm_rsq = 0,
		norm_r1sq = 0,
		beta = 0,
		alpha = 0;
	// init
	int k = 0,
		n = d_b.size;

    // create vectors on device
     VECTOR d_r = CreateVector(n, LOC_DEVICE),
             d_r1 = CreateVector(n, LOC_DEVICE),
             d_p = CreateVector(n, LOC_DEVICE),
             d_t = CreateVector(n, LOC_DEVICE);

   // allocate partial sum vector in mapped mem
	VECTOR mh_ps = {0},
		md_ps = {0};
	md_ps.size = mh_ps.size;
	CreateMappedVector(g_cgp.grid_sz, mh_ps, md_ps);

	// allocate s vector in mapped mem
	VECTOR mh_s = {0},
		md_s = {0};
	CreateMappedVector(n, mh_s, md_s);

	// r = b
	CopyVector(d_r, d_b);

	// CG loop
	do {
		// next step - update
		norm_r1sq = norm_rsq;
		norm_rsq = CudaDotProduct(d_r, d_r, md_ps, mh_ps,
			g_cgp.block_sz, g_cgp.grid_sz);
		TRACE(("k: %d, norm_rsq: "FFMT"\n", k, norm_rsq));
		CopyVector(d_r1, d_r); // r1 = r

		k++;
		if(k == 1) {
			CopyVector(d_p, d_r); // p = r
		} else {
			beta = norm_rsq / norm_r1sq; // beta = (r * r) / (r1 * r1)
			TRACE(("beta: "FFMT"\n", beta));
			// 2 saxpy:
			// p = r + beta * p;
			ZeroVector(d_t); // t = 0
			CudaSaxpy(d_p, beta, d_t, // t = beta * p
				g_cgp.block_sz, g_cgp.grid_sz);
			CudaSaxpy(d_r, 1, d_t, // t += 1 * r
				g_cgp.block_sz, g_cgp.grid_sz);
			CopyVector(d_p, d_t); // p = t
		}
		TRACE(("vector p: \n"));
		TraceDevVector(d_p);
		switch(g_cgp.mvm_method) {
			case 1:
				CudaMVMult1(d_A, d_p, md_s, mh_s, md_ps, mh_ps, // s = A * p
					g_cgp.dev_prop,
					g_cgp.block_sz, g_cgp.grid_sz);
				break;
			case 2: // fastest!
				CudaMVMult2(d_A, d_p, md_s, // s = A * p
					g_cgp.dev_prop,
					g_cgp.block_sz, g_cgp.grid_sz);
				break;
			default:
				assert(0 && "Invalid mvm_method!");
				break;
		}
		TRACE(("vector s (host): "FFMT"\n", Norm(mh_s)));
		TraceVector(&mh_s);
		TRACE(("vector s (dev): \n"));
		TraceDevVector(md_s);
		alpha = norm_rsq / CudaDotProduct(d_p, md_s, md_ps, mh_ps, // alpha = (r * r) / (p * s)
				g_cgp.block_sz, g_cgp.grid_sz);
		TRACE(("alpha: "FFMT"\n", alpha));
		CudaSaxpy(d_p, alpha, d_x, // x = x + alpha * p
			g_cgp.block_sz, g_cgp.grid_sz);
		TRACE(("vector x: \n"));
		TraceDevVector(d_x);
		CudaSaxpy(md_s, (-1) * alpha, d_r, // r = r - alpha * s
			g_cgp.block_sz, g_cgp.grid_sz);
		TRACE(("vector r: \n"));
		TraceDevVector(d_r);

		// step done
//		TRACE(("x:\n"));
//		TraceVector(d_x);
		TRACE(("\n"));
	} while(norm_rsq > tolerance && k < max_iter);

	// output
	g_cgp.iterations = k;
	g_cgp.tolerance = norm_rsq;

	// destroy vectors on device
	DestroyVector(&d_r);
	DestroyVector(&d_r1);
	DestroyVector(&d_p);
	DestroyVector(&d_t);

   // destroy vectors in mapped mem
	DestroyVector(&mh_ps);
	DestroyVector(&mh_s);

	return k < max_iter;
}



/**
 * Function:    main
 * Purpose:     entry point
 * Input:       CG parameters
 * Output:      CG output
 */
int main(int argc, char** argv)
{
	cudaError_t err = cudaErrorApiFailureBase;
	bool generate = false;
    double start = 0.0,
		finish = 0.0;

	// process command line args
	if(argc < 4) {
		printf("Usage: prog4 o t i [s] [r] [bs] [gs] [m] [...]\n");
		printf("o\tmatrix order (numeric)\n");
		printf("t\ttolerance (numeric)\n");
		printf("i\tmax iterations (numeric)\n");
		printf("s\tprint solution [y/n]\n");
		printf("r\tgenerate random data [y/n]\n");
		printf("bs\tblock size (numeric)\n");
		printf("gs\tgrid size (numeric)\n");
		printf("m\tMV multiplication method [1..2]\n");
		printf("...\tdata input\n");
		return -1;
	}
	// order of the system
	sscanf(argv[1], "%d", &g_cgp.order);
	// tolerance
	sscanf(argv[2], "%e", &g_cgp.tolerance);
	// maximum number of iterations
	sscanf(argv[3], "%d", &g_cgp.iterations);
	// suppress output of solution?
	if(argc >= 5) {
		g_cgp.suppress = (argv[4][0] == 'n');
	}
	// generate random data?
	if(argc >= 6) {
		generate = (argv[5][0] == 'y');
	}
	// block size
	if(argc >= 7) {
		sscanf(argv[6], "%d", &g_cgp.block_sz);
		// fix
		if(g_cgp.block_sz <= 0) {
			g_cgp.block_sz = BLOCK_SIZE;
		}
	}
	// grid size
	if(argc >= 8) {
		sscanf(argv[7], "%d", &g_cgp.grid_sz);
		// fix
		if(g_cgp.grid_sz <= 0) {
			g_cgp.grid_sz = GRID_SIZE;
		}
	}
	// mvm_method
	if(argc >= 9) {
		sscanf(argv[8], "%d", &g_cgp.mvm_method);
	}

	// cuda init
	cudaSetDevice(0);
	err = cudaSetDeviceFlags(cudaDeviceMapHost);
	assert(err == cudaSuccess);
	cudaPrintfInit();

	// device detection
	err = cudaGetDeviceProperties(&g_cgp.dev_prop, 0);
	assert(err == cudaSuccess);

	if(generate) {
		GenerateData(); // TEST only!
	} else {
		InputData();
	}

	// run CG procedure
	GET_TIME(start);
	ConjugateGradient();
	GET_TIME(finish);

	//copy result
	assert(g_cgp.h_x.size == g_cgp.d_x.size);
	err = cudaMemcpy(g_cgp.h_x.items, g_cgp.d_x.items,
		sizeof(float) * g_cgp.h_x.size, cudaMemcpyDeviceToHost);
	assert(err == cudaSuccess);

    // output
	g_cgp.elapsed = finish - start;
	OutputData();

	DestroyData();

	// cuda end
	cudaPrintfDisplay(stdout, true);
	cudaPrintfEnd();

    return 0;
}
