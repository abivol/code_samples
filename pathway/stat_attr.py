#!/usr/bin/python

import sys, os, csv


def stat_attr(fn_in, cutoff=0.1):
    i = 1
    with open(fn_in, "r") as f_in:
        for ln in f_in:
            ln = ln.strip()
            if len(ln) <= 0:
                continue
            toks = ln.split("\t")
            if len(toks) < 3:
                continue
            pathway = toks[0]
            pval = float(toks[1])
            if pval > cutoff:
                continue
            fn_out = "pathway_{0:03d}.tab".format(i)
            with open(fn_out, "w") as f_out:
                print >> f_out, "{0}\t{1}".format("id", pathway)
                for t in toks[2].split(","):
                    print >> f_out, "{0}\t{1}".format(t, 1)
            i += 1


def main(args):
    stat_attr(args[1])


if __name__ == "__main__":
    main(sys.argv)
