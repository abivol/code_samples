#!/usr/bin/python

import sys, os, csv


def pathway_sif(fn_in):
    with open(fn_in, "r") as f_in:
        for ln in f_in:
            ln = ln.strip()
            if len(ln) <= 0:
                continue
            toks = ln.split("\t")
            if len(toks) < 3:
                continue
            assert len(toks) <= 3
            print "{0}\t{2}\t{1}".format(toks[0], toks[1], toks[2])



def main(args):
    pathway_sif(args[1])


if __name__ == "__main__":
    main(sys.argv)
