#!/usr/bin/python

import os
from fisher import pvalue

refNet = "reference/pid_clean_pathway.sif"
refSet = "reference/clean_pid_3_pathway.gmt"

# PARADIGM entity suffixes
# https://dna.five3genomics.com/paradigm/help
# These are for the "cleaned" pathway version (Five3),
# which only accepts a subset of ASCII chars.
# Unfortunately, some type suffixes are confounded w/
# the actual name suffix, for instace "_protein".
bioTypeLst = ["complex", "family", "abstract"]
suffLst = ["_{0}".format(s) for s in bioTypeLst]


#
# remove biological entity type suffix from name
# so that it matches the entries in the pathway .gmt file (without suffix)
#
# Examples:
# FOXA2-3_family  -->  FOXA2-3
#
def removeTypeSuffix(s):
    for suff in suffLst:
        r = s.rfind(suff)
        if r > 0 and r == len(s) - len(suff):
            return s.rstrip(suff)
    #
    return s


def getMembers(netFile):
    members = set()
    f = open(netFile, "r")
    for line in f:
        if line.isspace():
            continue
        pline = line.rstrip().split("\t")
        members.add(removeTypeSuffix(pline[0]))
        members.add(removeTypeSuffix(pline[2]))
    f.close()
    return(members)

refMembers = getMembers(refNet)

setMap = {}
f = open(refSet, "r")
for line in f:
    if line.isspace():
        continue
    pline = line.rstrip().split("\t")
    name = pline.pop(0)
    setMap[name] = set(pline)
f.close()

for file in os.listdir("."):
    if file.endswith(".sif"):
        thisMembers = getMembers(file)
        notMembers = refMembers - thisMembers
        
        scoreMap = {}
        memberMap = {}
        for name in setMap:
            a = len(thisMembers & setMap[name])
            b = len(thisMembers - setMap[name])
            c = len(notMembers & setMap[name])
            d = len(notMembers - setMap[name])
            pval = pvalue(a, b, c, d)
            scoreMap[name] = pval.right_tail
            memberMap[name] = list(thisMembers & setMap[name])
        names = scoreMap.keys()
        names.sort(lambda x, y: cmp(scoreMap[x], scoreMap[y]))
        f = open("%s.stat" % (file.rstrip(".sif")), "w")
        for name in names:
            f.write("%s\t%s\t%s\n" % (name, scoreMap[name]*len(scoreMap.keys()), ",".join(memberMap[name])))
        f.close()

