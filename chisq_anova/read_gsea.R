

#
# Read filtered GSEA files (name, NES) and join into matrix
#
read.gsea = function(path="pw2") {
    v.files = list.files(path)
    v.feat = NULL # feature vector
    mx.gsea = NULL
    i = 0
    for(fn in v.files) {
        i = i + 1
        print(sprintf("%d: %s", i, fn))
        fp = sprintf("%s/%s", path, fn)
        df.sample = read.delim(fp, header=F, as.is=T, row.names=1, check.names=F)
        v.rows = sort(rownames(df.sample))
        v.sample = df.sample[v.rows, ]
        # View(v.sample)
        if(is.null(v.feat)) {
            v.feat = v.rows
            mx.gsea = matrix(0, nrow=length(v.feat), ncol=length(v.files))
        } else {
            stopifnot(all.equal(v.feat, v.rows))
        }
        mx.gsea[, i] = v.sample
    }
    #
    rownames(mx.gsea) = v.feat
    colnames(mx.gsea) = substr(v.files, 1, 12) # TCGA patient/sample id
    #
    return(mx.gsea)
}

