
#
# !!! IMPORTANT !!!
#
# Query files must be split manually, such that the multiple queries for the same pathway
# do not straddle multiple files, otherwise there will be numbering conflicts!
#
# Manual split: 400 / 500 / 670
#

# split -C 191k -d --additional-suffix=.query go-queries.tab query-
# split -l 524 -d --additional-suffix=.query go-queries.tab query-

for i in {0..2}
do
   echo ${i}
   in=query-0${i}.query
   echo ${in}
   out=out_${i}.log
   echo ${out}
   #
   nohup python hw1.py ${in} &> ${out} &
done
