#!/usr/bin/python

## Usage:
## ./gene-search.py 1 [query-file] [pathways-file] [cluster-file]
## Example:
## ./gene-search.py 1 go-queries.tab go-pathways.tab ppi-clusters.tab
##


import sys, os, time, scipy.stats


def read_clusters(fn):
    expers = dict()
    clusters = dict()
    with open(fn, "r") as f:
        for ln in f:
            ln = ln.strip()
            tok_l = ln.split("\t")
            # header
            h = tok_l[0]
            # gene set
            gs = set(tok_l[1:])
            # parse head
            tok_h = h.split("@")
            e = tok_h[1]
            if e not in expers:
                exper = dict()
                expers[e] = exper
            else:
                exper = expers[e]
            #
            exper[h] = gs
            clusters[h] = gs
            #
            # print "exper: {0}, cluster: {h}, set: {2}".format(e, m, " ".join(gs))
    #
    return (expers, clusters)


def read_pathways(fn):
    # pathways = dict of gene sets
    pathways = dict()
    with open(fn, "r") as f:
        for ln in f:
            ln = ln.strip()
            tok_l = ln.split("\t")
            # header
            h = tok_l[0]
            # set
            gs = set(tok_l[1:])
            #
            pathways[h] = gs
            #
            # print "pathway: {0}, set: {1}".format(h, " ".join(gs))
    #
    return pathways


def read_queries(fn):
    # queries = dict of queries
    qids = set()
    queries = dict()
    with open(fn, "r") as f:
        i = 1
        for ln in f:
            ln = ln.strip()
            tok_l = ln.split("\t")
            # header
            h = tok_l[0]
            # set
            gs = set(tok_l[1:])
            #
            if h in qids:
                i = i + 1
            else:
                i = 1
                qids.add(h)
            #
            qid = "{0}@{1}".format(h, i)
            queries[qid] = gs
            #
            # print "query: {0}, set: {1}".format(qid, " ".join(gs))
    #
    return queries


def get_genome(clusters):
    genome = set()
    for c in clusters:
        genome = genome.union(clusters[c])
    #
    return genome


def gene_in_cluster(genome, clusters):
    map_gc = dict()
    for g in genome:
        gc = set()
        map_gc[g] = gc
        for c in clusters:
            if(g in clusters[c]):
                gc.add(c)
    #
    return map_gc


def query_in_cluster(clusters, query):
    qc_intersection = dict()
    qc_union = dict()
    for k in clusters:
        c = clusters[k]
        x = len(c.intersection(query))
        if x > 0:
            qc_intersection[k] = x
            qc_union[k] = len(c.union(query))
    #
    return (qc_intersection, qc_union)


#
# gene = gene
# query = query
# clusters = dict() of clusters
# expers = dict() of expers
# cs = cluster selection (for gene, query)
# qc_intersection = dict() of set sizes, for overlap between query and clusters
# qc_union = dict() of set sizes, for union between query and (overlapping) clusters
#
def score_gene(gene, expers, cs, qc_intersection, qc_union):
    cs1 = cs
    assert len(cs1) > 0
    # gene score
    S = 0
    # number of expers
    ne = 0
    #
    for e in expers:
        # select clusters in experiment
        cs2 = cs1.intersection(expers[e].keys())
        if len(cs2) <= 0:
            continue
        #
        ne = ne + 1
        W = sum(float(qc_intersection[i])/float(qc_union[i]) for i in cs2)
        S = S + W / len(cs2)
        # print exper score
        # print "e: {0}, s: {1}".format(e, W / len(cs2))
    #
    return (ne, S)


#
# gS = dict() of gene scores
# kq = query name (key)
# R = gene diff set (P - Q)
#
def print_ranks(gS, kq, R):
    lk = gS.keys()
    ls = gS.values()
    # ranks
    vr = scipy.stats.rankdata(ls)
    # max rank
    mr = max(vr)
    # revert rankings (low to high)
    vr = [mr - vr[i] + 1 for i in range(len(vr))]
    # select ranks in R
    rr = list()
    for i in range(len(lk)):
        g = lk[i]
        if g in R:
            rr.append(vr[i])
    # append genes missing from genome (gS)
    for g in R.difference(set(lk)):
        rr.append(mr + 1)
    #
    rr.sort()
    #
    print "{0}\t{1}\t{2}".format(kq.replace(" ", "_"),
        max(rr),
        "\t".join(str(x) for x in rr))


def get_sets(pathways, queries, kq):
    Q = queries[kq]
    # print "Q: '{0}', len: {1}, set: '{2}'".format(kq, len(Q), " ".join(Q))
    # extract the pathway
    np = kq.split("@")[0]
    P = pathways[np]
    # print "P: '{0}', len: {1}, set: '{2}'".format(np, len(P), " ".join(P))
    R = P.difference(Q)
    # print "R: '{0}', len: {1}, set: '{2}'".format(np, len(R), " ".join(R))
    return (P, Q, R)


def predict_single(fn_queries, fn_pathways, fn_clusters, output=False):
    #
    (dE, dC) = read_clusters(fn_clusters)
    # print "len(dE): {0}, len(dC): {1}".format(len(dE), len(dC))
    #
    dP = read_pathways(fn_pathways)
    # print "len(dP): {0}".format(len(dP))
    #
    dQ = read_queries(fn_queries)
    # print "len(dQ): {0}".format(len(dQ))
    #
    G = get_genome(dC)
    # print "len(G): {0}".format(len(G))
    map_gc = gene_in_cluster(G, dC)
    # print "len(map_gc): {0}".format(len(map_gc))
    #
    # return dict of (exper count and score) per gene, for each query
    r_nexper = dict()
    r_gscore = dict()
    #

    start = time.time()
    for kq in dQ:
        #
        (P, Q, R) = get_sets(dP, dQ, kq)
        #
        (qc_intersection, qc_union) = query_in_cluster(dC, Q)
        # print "len(qc_intersection): {0}".format(len(qc_intersection))
        # gene nexper
        gNE = dict()
        # gene scores
        gS = dict()
        #
        for g in G.difference(Q):
            # gene clusters
            gc = map_gc[g]
            # cluster selection
            cs = gc.intersection(qc_intersection.keys())
            if len(cs) <= 0:
                (gNE[g], gS[g]) = (0, 0)
                continue
            #
            (gNE[g], gS[g]) = score_gene(g, dE, cs, qc_intersection, qc_union)
            # print "GENE: {0}, score: {1}".format(g, gS[g])
        #
        if output:
            print_ranks(gS, kq, R)
        else:
            # save results
            r_nexper[kq] = gNE
            r_gscore[kq] = gS
    #
    end = time.time()
    # print "time: {0}".format(end - start)
    return (r_nexper, r_gscore)


#
# l_fn_clusters = list of dataset (clusters) files
#
def predict_multi(fn_queries, fn_pathways, l_fn_clusters):
    #
    # compute individual gene scores (gS) in each dataset, for each query,
    # also record the number of experiments (gNE) in each dataset
    #
    # the multi-dataset score[g,Q] will be = sum [over datasets] of (gS / gNE)
    #
    l_dset = list()
    G = set()
    for fn_c in l_fn_clusters:
        l_dset.append(predict_single(fn_queries, fn_pathways, fn_c))
        # compute joint Genome (inefficient - read clusters again)
        (dE, dC) = read_clusters(fn_c)
        # print "len(dE): {0}, len(dC): {1}".format(len(dE), len(dC))
        G = G.union(get_genome(dC))
    #
    # print "len(G): {0}".format(len(G))

    #
    # TODO: queries and pathways are read in multiple times (inefficient)
    #
    dQ = read_queries(fn_queries)
    # print "len(dQ): {0}".format(len(dQ))
    #
    dP = read_pathways(fn_pathways)
    # print "len(dP): {0}".format(len(dP))
    #
    # compute multi-dataset gene scores for each query
    for kq in dQ:
        #
        (P, Q, R) = get_sets(dP, dQ, kq)
        # multi gene score
        gM = dict()
        #
        for g in G.difference(Q):
            gm = 0
            for (dNE, dS) in l_dset:
                gNE = dNE[kq]
                gS = dS[kq]
                if g in gS and gNE[g] > 0:
                    gm = gm + gS[g]/gNE[g]
            #
            gM[g] = gm
            # print "GENE: {0}, score: {1}".format(g, gM[g])
        #
        print_ranks(gM, kq, R)
    #


def main(args):
    if args[1] == "1":
        predict_single(args[2], args[3], args[4], output=True)
    elif args[1] == "2":
        predict_multi(args[2],  args[3], args[4:])
    #


if __name__ == "__main__":
    main(sys.argv)
