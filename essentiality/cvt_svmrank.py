#!/usr/bin/python

import sys, string, os, scipy.stats, pickle


def pickle_dump(obj, fn):
    with open(fn, "wb") as f_dump:
        pickle.dump(obj, f_dump, pickle.HIGHEST_PROTOCOL)


def pickle_load(fn):
    with open(fn, 'rb') as f_load:
        obj = pickle.load(f_load)
    return(obj)


#
# read gene assignment to output clusters
#
def read_clust_out(fn_clust):
    d_clust = {}
    with open(fn_clust, "r") as f_clust:
        for l in f_clust:
            l = l.strip()
            tok = l.split()
            d_clust[ tok[0] ] = int(tok[1])
    return(d_clust)


#
# read gene assignment to feature clusters/groups
#
def read_clust_feat(fn_clust, skip=0):
    d_clust = {}
    with open(fn_clust, "r") as f_clust:
        for l in f_clust:
            l = l.strip()
            tok = l.split()
            tok = tok[skip:]
            g = frozenset(tok)
            for t in tok:
                # pick max group containing a gene
                if (t not in d_clust) or (len(g) > len(d_clust[t])):
                    d_clust[t] = g
    return(d_clust)


#
# read the feature vectors for each cell line
# feature vectors must be rows in this file!
#
# cell lines must be in the exact same order in ranking and feature files!
#
def read_features(fn_feat):
    d_feat = {}
    with open(fn_feat, "r") as f_feat:
        # read header
        l = f_feat.next()
        l = l.strip()
        tok = l.split()
        l_names = tok[1:]
        # read features
        i = 0
        for l in f_feat:
            l = l.strip()
            tok = l.split()
            d_feat[i] = tok[1:]
            i = i + 1
    #
    return((l_names, d_feat))


#
# convert ranking and features (matrices) to rank_svm format,
# for input to RankLib
#
# clust_out = clustering of output space (essentiality)
# clust_feat = clustering of feature space (expression)
#
def cvt_ranksvm(clust_out, feat_names, d_feat, fn_rank,
    feat_clust=None, f_min=10, f_max=1000):
    # qid name map
    d_qid = {}
    # file handles for each cluster/model
    d_fh = {}
    with open(fn_rank, "r") as f_rank:
        # skip header
        f_rank.next()
        # gene index
        i = 0
        for l in f_rank:
            l = l.strip()
            #
            tok = l.split()
            #
            gene = tok[0]
            # rank the cell lines and create a qid w/ features per rank
            # cell lines must be in the exact same order in ranking and feature files!
            v_rdata = map(float, tok[1:])
            v_rank = scipy.stats.rankdata(v_rdata)
            #
            clust = clust_out[gene]
            fn = "clust_{0:05d}.txt".format(clust)
            # sanitize file name
            # fn = re.sub("[^0-9a-zA-Z]+", "_", fn)
            f_clust = None
            if clust in d_fh:
                f_clust = d_fh[clust]
            else:
                f_clust = open(fn, "w")
                d_fh[clust] = f_clust
            #
            qid = i
            d_qid[qid] = gene
            feat_sel = None
            if feat_clust is not None and gene in feat_clust:
                feat_sel = set(feat_clust[gene]).intersection(feat_names)
                if len(feat_sel) < f_min:
                    feat_sel = None
            #
            # !!! TODO: subsample or resample the input space (documents) and/or output space (ordered pairs),
            # to make up more training data
            #
            # write out the features for each rank
            for j in range(0, len(v_rank)):
                # pick features for this cell line (j)
                r = int(v_rank[j])
                feat_lst = d_feat[j]
                # select features for this gene
                if feat_sel is not None:
                    ix = [feat_names.index(x) for x in feat_sel]
                    l_feat = [feat_lst[x] for x in ix]
                else:
                    l_feat = feat_lst
                #
                z = zip(range(0, len(l_feat)), l_feat)
                str_feat = " ".join("{0}:{1}".format(p[0], p[1]) for p in z)
                out = "{0:d} qid:{1} {2}".format(r, qid, str_feat)
                f_clust.write("{0}\n".format(out))
            # next
            i = i + 1
        # close all files
        for f in d_fh.values():
            f.close()
    #
    return(d_qid)


#
# convert RankLib scores to .gct format
#
def score_gct(d_clust, d_qid, fn_score):
    # invert cluster map
    d_gcl = {}
    for g in d_clust:
        c = d_clust[g]
        if c in d_gcl:
            s = d_gcl[c]
            s.add(g)
        else:
            s = set([g])
            d_gcl[c] = s
    # read in all per cluster rankings from score file
    d_score = {}
    with open(fn_score, "r") as f_score:
        old_gene = None
        for l in f_score:
            l = l.strip()
            #
            tok = l.split()
            qid = int(tok[0])
            gene = d_qid[qid]
            r = tok[2]
            #
            if gene != old_gene:
                d_score[gene] = []
            #
            d_score[gene].append(float(r))
            # 
            old_gene = gene
        #
        # find and write all rankings for genes in this cluster
        d_cdone = set()
        for gene in d_score:
            # find its cluster
            c = d_clust[gene]
            if c in d_cdone:
                continue
            d_cdone.add(c)
            # get all genes in cluster 
            gs = d_gcl[c]
            for g in gs:
                # write out predicted ranking
                print "{0}\t{1}\t{2}".format(g, g, "\t".join("{0:0.5f}".format(s) for s in d_score[gene]))
    #


#
#=======================================================================
# 
#=======================================================================
#


#
# convert training data
#
def cvt_train_1(args):
    d_clust = read_clust_out("clust_k_100.tab")
    fn_clust = "clust.bin"
    pickle_dump(d_clust, fn_clust)
    #
    d_feat = read_features("x_expr.tab")
    #
    d_qid = cvt_ranksvm(d_clust, d_feat, "x_esse.tab")
    fn_qid = "qid.bin"
    pickle_dump(d_qid, fn_qid)


#
# convert testing data
#
def cvt_test_1(args):
    # load
    fn_qid = "qid.bin"
    d_qid = pickle_load(fn_qid)
    #
    fn_clust = "clust.bin"
    d_clust = pickle_load(fn_clust)
    #
    score_gct(d_clust, d_qid, args[1])


#
#=======================================================================
# 
#=======================================================================
#


#
# convert training data
#
def cvt_train_2(args):
    clust_out = read_clust_out("ess_clust.tab")
    fn_clust_out = "clust.bin"
    pickle_dump(clust_out, fn_clust_out)
    
    feat_clust = read_clust_feat("c2.cp.v4.0.symbols.tab")
    pickle_dump(feat_clust, "feat_clust.bin")
    #
    (l_names, d_feat) = read_features("x_expr.tab")
    # pickle_dump(l_names, "feat_names.bin")
    # pickle_dump(d_feat, "feat_dict.bin")
    #
    d_qid = cvt_ranksvm(clust_out, l_names, d_feat, "x_esse.tab", feat_clust)
    fn_qid = "qid.bin"
    pickle_dump(d_qid, fn_qid)


#
# convert testing data
#
def cvt_test_2(args):
    # load
    fn_qid = "qid.bin"
    d_qid = pickle_load(fn_qid)
    #
    fn_clust = "clust.bin"
    d_clust = pickle_load(fn_clust)
    #
    score_gct(d_clust, d_qid, args[1])


#
#
#
if __name__ == '__main__':
    cvt_test_2(sys.argv)
