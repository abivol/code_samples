#!/usr/bin/python

import sys, string, os


#
# convert matrix to (ABC) format
#
def cvt_matrix(fn_in, fn_out, cutoff=5e-8):
    with open(fn_in, "r") as f_in:
        with open(fn_out, "w") as f_out:
            # parse nodes
            l_node = []
            l = f_in.next()
            l = l.strip()
            l_node = l.split()[1:]
            print(len(l_node))
            # parse edges
            i = 0
            for l in f_in:
                l = l.strip()
                tok = l.split()
                #
                n_i = l_node[i]
                for j in range(i+2, len(tok)):
                    e = float(tok[j])
                    if e > cutoff:
                        n_j = l_node[j-1]
                        # print(i,j)
                        f_out.write("{0}\t{1}\t{2:0.10f}\n".format(n_i, n_j, e))
                # next
                i = i + 1
    # done


#
#
#
def main(args):
    cvt_matrix(args[1], args[2])

#
#
#
if __name__ == '__main__':
    main(sys.argv)
