
import sys, random, math, os, re, copy
from util import *


def Intersect(set_list):
    assert len(set_list) > 0
    result = reduce(lambda s1, s2: s1.intersection(s2), set_list)
    return result


def Union(set_list):
    assert len(set_list) > 0
    result = reduce(lambda s1, s2: s1.union(s2), set_list)
    return result


class Item:
    def __init__(self, dir = None, name = None, tfidf = None, classif = None):
        self.Dir = dir
        self.Name = name
        self.TFIDF = tfidf
        self.Classif = classif

    def Norm(self):
        t = sum(self.TFIDF[w]**2 for w in self.TFIDF)
        return math.sqrt(t)
    
    def GetScore(self, w):
        if w in self.TFIDF:
            return self.TFIDF[w]
        else:
            return 0
    
    def __repr__(self):
        return str((self.Name, self.Classif, len(self.TFIDF)))


# cosine similarity
def CosineSim(item1, item2) :
    t1 = sum(item1.TFIDF[w] * item2.TFIDF[w] for w in item1.TFIDF if w in item2.TFIDF)
#    print 't1: ', t1
    t2 = item1.Norm() * item2.Norm()
    if t2 == 0:
        return 0
    return t1 / t2


class KMeans:
    def __init__(self, K = 20):
        self.K = K
        self.Centroids = []
        self.REDelimiters = re.compile('@|\.|\'|\"|,|;|=|\+|\(|\)|<|>|\[|\]|\{|}|\*|/|&|\\\\|%|#|\^|!|_|:|\?|\$|\|')
        self.DF = {}
        self.CorpusSize = 0
        self.Documents = []
        self.TFIDF_Min = math.pow(10, -6)
        self.Centroid_Min = 0.99
        pass


    # return cleaned, 'bag of words' document
    def Read(self, fn):
        f = open(fn, 'r')
        lines = f.readlines()
        f.close()
        # string
        doc = ''.join(lines)
        # cleaning
        doc = re.sub(self.REDelimiters, ' ', doc)
        doc = doc.lower()
        words = doc.split()
        words = [w for w in words if w not in stopwords and w.isalpha()]
        return words # not a set!

    
    # update DF
    def UpdateDF(self, fn):
        words = self.Read(fn)
        # update DF
        for w in set(words):
            if w in self.DF:
                self.DF[w] += 1
            else:
                self.DF[w] = 1
        # update CS
        self.CorpusSize += 1
        

    # update TF-IDF
    def ComputeTFIDF(self, fn):
        TFIDF = {}
        words = self.Read(fn)
        # update TF-IDF
        for w in set(words):
            TF = words.count(w)
            if w in self.DF:
                DF = self.DF[w]
            else:
                DF = 1.0 / float(self.CorpusSize)
            tfidf = TF * math.log(float(self.CorpusSize) / DF)
            # sparse - don't store zeros
            if tfidf < self.TFIDF_Min:
                continue
            # store
            TFIDF[w] = tfidf
        return TFIDF

    
    # compute TfIdf scores for data items
    def TfIdf(self, data):
        # data comes in pairs of (path, class)
        # DF
        self.DF = {}
        self.CorpusSize = 0
        for d in data:
            self.UpdateDF(d[0])
        # TF-IDF
        self.Documents = []
        for d in data:
            p, c = d
            TFIDF = self.ComputeTFIDF(p)
            head, tail = os.path.split(p)
            self.Documents.append(Item(head, tail, TFIDF, c))
        pass


    # return index of closest centroid/cluster
    def Best(self, item):
        # compute similarity w/ each centroid
        cosim = []
        for i in range(self.K):
            centroid = self.Centroids[i]
            cosim.append(CosineSim(centroid, item))
        # assign best/closest centroid
        best = cosim.index(max(c for c in cosim))
        return best


    def ComputeCentroid(self, k):
        # first get dimensions
        tfidf = {}
        set_list = [set(x.TFIDF.keys()) for x in self.Clusters[k]]
        dims = Union(set_list)
        for w in dims:
            tfidf[w] = sum(x.GetScore(w) for x in self.Clusters[k]) / len(self.Clusters[k])
        centroid = Item(name = 'Centroid ' + str(k), tfidf = tfidf)
#        print 'Centroid: ', centroid
        return centroid


    def PrintClusters(self):
        for i in range(self.K):
            print 'Cluster: %d, centroid: %s, items: %d' % (i, self.Centroids[i], len(self.Clusters[i]))
        pass

  
    # k-means
    def Train(self, data):
        # process and extract data
        self.TfIdf(data)
        # select initial centroids
        self.Centroids = []
        self.Clusters = []
        initial = range(len(self.Documents))
        random.shuffle(initial)
        initial = initial[:self.K]
        for i in initial:
            # centroids are not data
            centroid = copy.deepcopy(self.Documents[i])
            self.Centroids.append(centroid)
            self.Clusters.append([])
        done = False
        while not done:
            done = True
            # clear clusters
            for i in range(self.K):
                self.Clusters[i] = []
            # assign documents to centroids (cluster)
            for d in self.Documents:
                # assign best/closest centroid
                best = self.Best(d)
                self.Clusters[best].append(d)
#                print 'doc: %s, cluster: %d' % (d.Name, best)
            # update centroids
            for i in range(self.K):
                centroid = self.ComputeCentroid(i)
                # did it move?
                cosim = CosineSim(centroid, self.Centroids[i])
#                print 'cosim: ', cosim
                if cosim < self.Centroid_Min:
                    done = False
                # update
                self.Centroids[i] = centroid
            # verifications
#            set_list = [set(self.Clusters[i]) for i in range(self.K)]
#            assert Union(set_list) == set(self.Documents)
        # label the centroids using majority
        for i in range(self.K):
            labels = [x.Classif for x in self.Clusters[i]]
            if len(labels) <= 0:
                continue
            classes = list(set(labels))
            counts = [labels.count(c) for c in classes]
            major = classes[counts.index(max(c for c in counts))]
            self.Centroids[i].Classif = major
        self.PrintClusters()
        pass


    # predict class
    def Predict(self, fn):
        # find closest cluster
        head, tail = os.path.split(fn)
        item = Item(head, tail)
        item.TFIDF = self.ComputeTFIDF(fn)
        best = self.Best(item)
        label = self.Centroids[best].Classif
        return label


def UT_TFIDF():
    d = 'mini/misc.forsale'
    files = [os.path.join(d, f) for f in os.listdir(d)]
    km = KMeans()
    km.TfIdf(files)
#    tfidf = km.ComputeTFIDF(p)
    print CosineSim(km.Documents[10], km.Documents[-5])


def UT_Train():
    d = 'mini/misc.forsale'
    data = [(os.path.join(d, f), 'forsale') for f in os.listdir(d)]
    km = KMeans()
    km.Train(data)
    c = km.Predict('mini/misc.forsale/76120')
    print c


def UT():
#    UT_TFIDF()
    UT_Train()
    pass


def main():
    args = sys.argv[1:]
    if len(args) <= 0:
        print 'Usage: mdp <file>'
        sys.exit(-1)
    f = args[0]


if __name__ == '__main__' :
    UT()
#    main()
