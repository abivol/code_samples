
import logging

def start(fn="tsp.log", level=logging.DEBUG):
    logging.basicConfig(filename = fn,
        filemode='a', level=level,
        format='%(asctime)s\t%(module)s/%(funcName)s/%(lineno)d\t%(levelname)s\t%(message)s')
    return logging


noLog = logging.getLogger("nul")
noLog.setLevel(logging.ERROR)
