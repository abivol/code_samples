'''

@author: adrian
'''

import os, shutil
import logging as log
from TreeHash import TreeHash


#
# sync tree A into tree B
# updates tree B to match tree A
# sync may be additive (no deletes), or not
#
class Mirror(object):

    def __init__(self, treeA, treeB, simulate=True):
        assert isinstance(treeA, TreeHash)
        self.treeA = treeA
        assert isinstance(treeB, TreeHash)
        self.treeB = treeB
        self.simulate = simulate


    def copyFile(self, sourcePath, targetPath):
        log.info("COPY\t{0}\t{1}".format(sourcePath, targetPath))
        # make new targetDir
        targetDir = os.path.dirname(targetPath)
        if not self.simulate:
            if not os.path.isdir(targetDir):
                os.makedirs(targetDir)
            # copy
            shutil.copy(sourcePath, targetPath)
        pass


    #
    # TODO:
    # update target tree (treeB)!!!
    # treeB should be updated during sync
    #
    def moveFiles(self, key):
        # find files and paths with this key
        setA = self.treeA.findFiles(key)
        setfnA = set(n.getPath() for n in setA)
        setB = self.treeB.findFiles(key)
        setfnB = set(n.getPath() for n in setB)
        # exclude files with conserved paths (unchanged)
        commonFn = setfnA.intersection(setfnB)
        divA = [n for n in setA if n.getPath() not in commonFn]
        divB = [n for n in setB if n.getPath() not in commonFn]
        # now these sets have diverged from treeB to treeA
        if len(divB) > len(divA):
            # too many in treeB
            delta = len(divB) - len(divA)
            # do not remove anything in this step! - see delete()
#            for i in range(0, delta):
#                log.debug("remove: {0}".format(divB[i]))
#                targetPath = divB[i].getPath(relative=False)
#                log.info("remove: {0}".format(targetPath))
#                if not self.simulate:
#                    os.remove(targetPath)
#                pass
            # update
            divB = divB[delta:]
        elif len(divA) > len(divB):
            # too many in treeA!
            delta = len(divA) - len(divB)
            # divB may be empty => use setB for copy
            b = setB.pop()
            for i in range(0, delta):
                a = divA[i]
                log.debug("local copy (dup): {0} => {1}".format(b, a))
                sourcePath = b.getPath(relative=False)
                # splice treeA path onto treeB tree (root)
                targetPath = self.treeB.splicePath(a.getPath())
                self.copyFile(sourcePath, targetPath)
            # update
            divA = divA[delta:]
        assert len(divB) == len(divA)
        # final moves
        for i in range(len(divA)):
            #log.debug("local move: {0} => {1}".format(divB[i], divA[i]))
            sourcePath = divB[i].getPath(relative=False)
            # splice treeA path onto treeB tree (root)
            targetPath = self.treeB.splicePath(divA[i].getPath())
            log.info("MOVE\t{0}\t{1}".format(sourcePath, targetPath))
            if not self.simulate:
                os.renames(sourcePath, targetPath)
        #
        pass


    # copy treeA -> treeB
    def copy(self):
        log.debug("start mirror copy")
        # find files that were moved (in treeA, vs treeB)
        setA = frozenset(self.treeA.hashTable.keys())
        shared = setA.intersection(
            frozenset(self.treeB.hashTable.keys()))
        # find the new paths in treeA for matching items from treeB
        for k in shared:
            self.moveFiles(k)
        # now copy all new (non-matching) files from A to B
        diffA = setA.difference(shared)
        for k in diffA:
            for a in self.treeA.hashTable[k]:
                sourcePath = a.getPath(relative=False)
                # splice treeA path onto treeB tree (root)
                targetPath = self.treeB.splicePath(a.getPath())
                self.copyFile(sourcePath, targetPath)
        pass


    # delete all in treeB, not in treeA  (set diff.: treeB - treeA)
    # TODO: update treeB !!!
    def delete(self):
        log.debug("start mirror delete")
        # remove children first! (bottom-up)
        for b in self.treeB.listNodes(preorder=False):
            if self.treeA.findNode(b.getPath()) is None:
                targetPath = b.getPath(relative=False)                
                log.info("REMOVE\t{0}".format(targetPath))
                if not self.simulate and os.path.exists(targetPath):
                    if b.isDir():
                        #shutil.rmtree(targetPath)
                        os.rmdir(targetPath)
                    else:
                        os.remove(targetPath)
                    pass
        pass
