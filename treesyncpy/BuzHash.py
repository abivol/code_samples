'''

@author: adrian
'''

import array, random


def bitLen(val):
    length = 0
    while (val):
        val >>= 1
        length += 1
    return(length)


class BitRotate(object):
    '''
    classdocs
    '''

    def __init__(self, width=16):
        '''
        Constructor
        '''
        self.WIDTH = width
        self.maskWord = int(2**self.WIDTH) - 1


    def bit_rotate_left(self, val, shift=1):
        shift %= self.WIDTH
        val = int(val & self.maskWord) # truncate
        return int( (val << shift) & self.maskWord) | (val >> (self.WIDTH - shift) )


class BuzHasher():

    def __init__(self, seed=None):
        self.BUF_LEN = 2**20
        self.WIDTH = 16
        self.seed = seed
        if self.seed is None:
            self.seed = 0xBAADF00C
        self.bitRotator = BitRotate(self.WIDTH)
        self.init()
        pass
    
    
    def init(self):
        #self.hashVal = range(0, 2**self.WIDTH)
        random.seed(self.seed)
        self.hashVal = random.sample(range(2**10, 2**22, 17), 2**self.WIDTH)
        self.hashValRotated = [self.bitRotator.bit_rotate_left(self.hashVal[i])
            for i in range(len(self.hashVal))]
        self.lastchar = 0
        self.digest = 0xD00D


    def update(self, char):
        self.digest = (self.bitRotator.bit_rotate_left(self.digest) ^
            self.hashValRotated[self.lastchar] ^
            self.hashVal[char])
        self.lastchar = char
        pass
    
    
    def __hashFile(self, fn):
        self.init()
        with open(fn, 'rb') as f:
            eof = False
            while not eof:
                try:
                    buf = array.array('H')
                    buf.fromfile(f, self.BUF_LEN)
                except EOFError:
                    eof = True
                for v in buf:
                    self.update(v)
        #
        return self.digest


    def hashArray(self, a):
        assert isinstance(a, array.array)
        self.init()
        for v in a:
            self.update(v)
        #
        return self.digest



#
#
#
if __name__ == '__main__':
    bh = BuzHasher()
    a = array.array('H')
    a.fromstring("bcde")
    print bh.hashArray(a)
    pass
