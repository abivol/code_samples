
# Python version <= 2.4 !

import ConfigParser

class conf_c:
    # class vars
    instance = None
    section = None
    
    @staticmethod
    def get(option, section=None):
        if section is not None:
            conf_c.section = section
        else:
            section = conf_c.section
        #
        if conf_c.instance is None:
            conf_c.instance = conf_c()
        return conf_c.instance.read(option, section)
        pass


    def __init__(self, savePath = "gm.conf"):
        self.parser = ConfigParser.SafeConfigParser()
        f_in = open(savePath)
        self.parser.readfp(f_in)
        f_in.close()

    def read(self, option, section):
        return self.parser.get(section, option)


    pass


#
