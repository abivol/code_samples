
import pickle, bz2


def str2bool(s):
    return s.lower() in ("yes", "true", "t", "1")



def pickleLoad(fn):
    with bz2.BZ2File(fn, "r", 2**20, 9) as f_in:
        obj = pickle.load(f_in)
    return obj


def pickleSave(obj, fn):
    with bz2.BZ2File(fn, "w", 2**20, 9) as f_out:
        pickle.dump(obj, f_out, pickle.HIGHEST_PROTOCOL)


def callersName():
    import sys
    return sys._getframe(2).f_code.co_name
