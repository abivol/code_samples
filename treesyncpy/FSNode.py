'''

@author: adrian
'''

import os, hashlib
import logging as log
from Tree import Node, Tree
import Util


class FSNodeKind:
    FILE = "F"
    DIRECTORY = "D"


class FSHash(Node):

    def __init__(self, offset, size, digest):
        self.offset = offset
        self.size = size
        self.digest = digest

    def __repr__(self):
        return "{0}:{1}:{2}".format(self.offset, self.size, self.digest)


class FSNode(Node):

    def __init__(self, name, nodeKind,
            fileSize=None, mTime=None,
            partialHashes=None, fullHash=None):
        Node.__init__(self, name)
        self.active = True
        self.nodeKind = nodeKind
        self.fileSize = fileSize
        self.mTime = mTime
        self.partialHashes=partialHashes
        self.fullHash = fullHash


    def isDir(self):
        return self.nodeKind == FSNodeKind.DIRECTORY


    def isFile(self):
        return self.nodeKind == FSNodeKind.FILE


    def __repr__(self):
        fmt = "{{{0}|{1}|'{2}'|"
        if self.isDir():
            fmt += "{3}"
        else:
            fmt += "{4}|{5}"
        fmt += "}}"
        return fmt.format(self.active, 
            self.nodeKind, self.getPath(),
            len(self.children),
            self.getKeyPartial(),
            self.getKeyFull())


    def getPath(self, relative=True):
        if relative and self.parent is None:
            return ""
        path = self.name
        parent = self.parent
        while parent is not None:
            if relative and parent.parent is None:
                break
            path = os.path.join(parent.name, path)
            parent = parent.parent
        return path


    def compare(self, pHashes):
        if len(self.partialHashes) != len(pHashes):
            return False
        for i in range(0, len(self.partialHashes)):
            x = self.partialHashes[i]
            y = pHashes[i]
            if (x.offset != y.offset or
                x.size != y.size or
                x.digest != y.digest):
                return False
        #
        return True


    def getKeyPartial(self):
        if self.partialHashes is None:
            return None
        assert len(self.partialHashes) > 0
        hk = ",".join(str(h) for h in self.partialHashes)
        key = (self.fileSize, hk)
        return key


    def getKeyFull(self):
        if self.fullHash is None:
            return None
        key = (self.fileSize, self.fullHash)
        return key
