'''

@author: adrian
'''

import logging as log
import Log, Util
from TreeHash import TreeHash, FSNodeKind
from Mirror import Mirror



def Test1():
    Log.start()
    p = "/home/adrian/websites/cs673-2007"
    log.info(p)
    th = TreeHash(p)
    th.printTree()
    Util.pickleSave(th, "th.bz2")
    pass


def Test2():
    Log.start()
    pA = "/home/adrian/work/code/treesyncpy/test1/A"
    pB = "/home/adrian/work/code/treesyncpy/test1/B"
    tA = TreeHash(pA)
    #tA.printTree()
    Util.pickleSave(tA, "tA.bin.bz2")
    tB = TreeHash(pB)
    #tB.printTree()
    Util.pickleSave(tB, "tB.bin.bz2")
    mr = Mirror(tA, tB)
    mr.copy()
    pass


def Test3():
    Log.start()
    p = "test1"
    #t = TreeHash(p)
    t = TreeHash.load("target.bin.bz2")
    t.update()
    t.printTree()
    Util.pickleSave(t, "target.bin.bz2")
    pass


#
#
#
if __name__ == '__main__':
    Test3()
    pass
