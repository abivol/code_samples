'''

@author: adrian
'''

import os
import logging as log


class Node():
    '''
    classdocs
    '''


    def __init__(self, name):
        '''
        Constructor
        '''
        self.name = name
        self.parent = None
        self.children = {}
        
        
    def addChild(self, node):
        assert node.name not in self.children
        self.children[node.name] = node
        node.parent = self
        

class Tree():
    '''
    classdocs
    '''


    def __init__(self, root):
        '''
        Constructor
        '''
        assert isinstance(root, Node)
        self.root = root

    # traverse handler:
    # handler([self], tree, node)
    def traverse(self, handler, preorder=True):
        self.__traverse(self.root, handler, preorder)


    def __traverse(self, node, handler, preorder):
        #log.debug("traverse: {0}".format(node))
        if preorder:
            handler(self, node)
        for c in node.children:
            self.__traverse(node.children[c], handler, preorder) # recursive !!!
        if not preorder:
            handler(self, node)

    
    def __printTreeHandler(self, tree, node):
        log.info("print: {0}".format(node))


    def printTree(self):
        log.info("print tree: {0}".format(self))
        self.traverse(self.__printTreeHandler)


    def listNodes(self, preorder=True):
        for n in self.__listNodes(self.root, preorder):
            yield n


    def __listNodes(self, node, preorder):
        if preorder:
            yield node
        for c in node.children:
            for n in self.__listNodes(node.children[c], preorder): # recursive !!!
                yield n
        if not preorder:
            yield node
