'''

@author: adrian
'''

import os, hashlib
import logging as log
from Tree import Tree
from FSNode import FSNode, FSNodeKind, FSHash
import Util


#
# set the 'active' attribute on tree nodes
#
class ActiveHandler():
    def __init__(self, alive):
        self.active = alive
    
    def handler(self, tree, node):
        log.debug("active handler: {0}".format(node))
        node.active = self.active


#
# remove inactive nodes from tree
#
class PruneHandler():
    def __init__(self):
        pass
    
    def handler(self, tree, node):
        assert isinstance(tree, TreeHash)
        assert isinstance(node, FSNode)
        # must unlink bottom-up to not alter iteration!!!
        log.debug("prune handler: {0}".format(node))
        # remove children from tree & hashTable
        for n in node.children.keys():
            c = node.children[n]
            if c.active:
                continue
            if c.isFile():
                key = c.getKeyPartial()
                if key in tree.hashTable:
                    tree.hashTable[key].remove(c)
            log.debug("prune child: {0}".format(node.children[n]))
            del node.children[n]
        pass


#
# represents file system tree with file info
#
class TreeHash(Tree):
    VERSION = 0430
    
    def __repr__(self):
        return "TreeHash('{0}', {1})".format(self.getRootPath(), self.hashAlgo)

    
    def setRootPath(self, path):
        self.root.name = path


    def getRootPath(self):
        return self.root.name

    
    #
    # 'relative': to tree root
    #
    def findNode(self, path, relative=True):
        if not relative:
            assert path[0:len(self.getRootPath())] == self.getRootPath()
            # make relative
            path = path[len(self.getRootPath()):]
        if len(path) == 0:
            return self.root
        # split to root
        segments = list()
        while len(path) > 0:
            bn = os.path.basename(path)
            if len(bn) <= 0:
                break
            segments.append(bn)
            path = os.path.dirname(path)
        #
        node = self.root
        for s in reversed(segments):
            if s not in node.children:
                return None
            node = node.children[s]
        return node


    # return list of partial file hashes
    # hash beginning, middle and end of file
    def hashPartial(self, path, fileSize):
        if fileSize <= 0:
            return None
        hl = []
        with open(path, "rb") as f:
            while True:
                # beginning
                offset = 0
                hasher = hashlib.new(self.hashAlgo)
                data = f.read(self.hashSize)
                assert len(data) > 0
                hasher.update(data)
                hl.append(FSHash(offset, len(data),  hasher.hexdigest()))
                log.debug("hash begin: {0}".format(hl[0]))
                # offset
                if fileSize <= self.hashSize:
                    break
                hasher = hashlib.new(self.hashAlgo)
                offset = fileSize - self.hashSize
                f.seek(offset)
                data = f.read(self.hashSize)
                assert len(data) == self.hashSize
                hasher.update(data)
                hl.append(FSHash(offset, len(data),  hasher.hexdigest()))
                log.debug("hash end: {0}".format(hl[1]))
                # middle
                if fileSize <= 2 * self.hashSize:
                    break
                hasher = hashlib.new(self.hashAlgo)
                offset = fileSize/2 - self.hashSize/2
                f.seek(offset)
                data = f.read(self.hashSize)
                assert len(data) == self.hashSize
                hasher.update(data)
                hl.append(FSHash(offset, len(data),  hasher.hexdigest()))
                log.debug("hash mid: {0}".format(hl[2]))
                break
            #
        #
        return hl


    # return full file hash
    def hashFull(self, path):
        with open(path, "rb") as f:
            hasher = hashlib.new(self.hashAlgo)
            while True:
                data = f.read(self.blockSize)
                if not data:
                    break
                hasher.update(data)
        return hasher.hexdigest()
    

    def addFile(self, node):
        # must match both file size and hash!
        key = node.getKeyPartial()
        # sets deal with duplicates
        try:
            self.hashTable[key].add(node)
        except KeyError:
            self.hashTable[key] = set([node])
        pass
    
    
    def findFiles(self, key):
        try:
            return self.hashTable[key]
        except KeyError:
            return set()
        pass
        

    def __init__(self, rootPath, hashAlgo="sha1"):
        '''
        Constructor
        '''
        rootNode = FSNode(rootPath, FSNodeKind.DIRECTORY)
        Tree.__init__(self, rootNode)
        self.hashTable = {}
        self.hashAlgo = hashAlgo
        self.blockSize = 2**20
        self.hashSize = self.blockSize


    #
    # walk the file system tree, and update our view/info
    #
    def update(self, handler=None):
        # enumerate tree recursively
        assert os.path.isdir(self.getRootPath())
        for root, dirs, files in os.walk(self.getRootPath(), topdown=True, followlinks=False):
            log.debug("root: '{0}'".format(root))
            #
            if handler is not None:
                handler(self)
            #
            parent = self.findNode(root, relative=False)
            assert isinstance(parent, FSNode)
            assert parent.isDir()
            log.debug("parent: '{0}'".format(parent.getPath()))
            # update nodes for all child dirs
            for d in dirs:
                p = os.path.join(root, d)
                # add child node?
                if d in parent.children:
                    log.debug("skip dir: '{0}'".format(p))
                    # mark as active
                    node = parent.children[d]
                    node.active = True
                    continue
                log.debug("add dir: '{0}'".format(p))
                parent.addChild(FSNode(d, FSNodeKind.DIRECTORY))
            # update nodes for all child files
            for f in files:
                self.updateNode(root, f, parent)

        # prune old, inactive nodes
        self.prune()
        pass


    #
    # update node if the file has changed
    #
    def updateNode(self, root, fName, parent):
        path = os.path.join(root, fName)
        #log.debug("update path: '{0}'".format(path))                
        mTime = os.path.getmtime(path)
        fileSize = os.path.getsize(path)
        # pHashes = self.hashPartial(path, fileSize)
        # add child node?
        if fName in parent.children:
            child = parent.children[fName]
            # identical?
            if mTime == child.mTime and \
                fileSize == child.fileSize and \
                True:
                # faster -- without hash comparison!
                # child.compare(pHashes):
                log.info("SKIP\t{0}".format(path))                
                # mark as active
                node = parent.children[fName]
                node.active = True
                return
            else:
                log.debug("del node: '{0}'".format(parent.children[fName]))
                del(parent.children[fName])
        # skip -- too slow !!
        pHashes = self.hashPartial(path, fileSize)
        fHash = None
        #fHash = self.hashFull(path)
        child = FSNode(fName, FSNodeKind.FILE,
            fileSize, mTime,
            partialHashes=pHashes, fullHash=fHash)
        parent.addChild(child)
        #
        log.info("UPDATE\t{0}".format(path))
        self.addFile(child)        
        pass


    # prune tree = delete inactive nodes
    # also delete empty directories???
    def prune(self):
        # unlink inactive nodes from tree
        ph = PruneHandler()
        # must prune bottom-up (remove children first) !!!
        self.traverse(handler=ph.handler, preorder=False)
        # remove empty keys from hashTable
        for k in self.hashTable.keys():
            if len(self.hashTable[k]) <= 0:
                del(self.hashTable[k])
        pass


    @staticmethod
    def load(fn):
        t = Util.pickleLoad(fn)
        assert isinstance(t, TreeHash)
        # unmark all nodes as 'active'
        ah = ActiveHandler(False)
        t.traverse(handler=ah.handler)
        # root is always active
        t.root.active = True
        return t


    def printHashTable(self):
        log.info("printHashTable")
        for k in self.hashTable:
            log.info("key: {0}".format(k))
            for n in self.hashTable[k]:
                log.info("\tnode: {0}".format(n))
        pass


    def splicePath(self, path):
        return os.path.join(self.getRootPath(), path)


    #
    #
    #
    def checkDups(self):
        # assume the tree is loaded and updated (partial hashes)
        dg = 0
        for key in self.hashTable:
            setFiles = self.hashTable[key]
            assert len(setFiles) > 0
            # no dups?
            if len(setFiles) <= 1:
                continue
            # check full hash
            fhTable = {}
            for n in setFiles:
                p = n.getPath(relative=False)
                fh = self.hashFull(p)
                if fh not in fhTable:
                    fhTable[fh] = set([n])
                else:
                    fhTable[fh].add(n)
            # find dups in full hash table and report them
            for key in fhTable:
                setFiles = fhTable[key]
                assert len(setFiles) > 0
                # no dups?
                if len(setFiles) <= 1:
                    continue
                # report
                dg += 1
                log.info("dup group {0}, {1}, {2}".format(dg, n.fileSize, key))
                for n in setFiles:
                    log.info("DUP\t{0}\t{1}".format(dg, n.getPath(relative=False)))
