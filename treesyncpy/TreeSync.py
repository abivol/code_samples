#!/usr/bin/python

'''

@author: adrian
'''

import sys, getopt, os, time
import logging as log
import Log, Util
from TreeHash import TreeHash
from Mirror import Mirror


#
# save tree periodically to avoid complete data loss,
# on error or interruption
#
class SaveHandler():
    def __init__(self, savePath, maxDelta=5):
        self.savePath = savePath
        self.startTime = 0
        self.maxDelta = maxDelta
    
    def handler(self, tree):
        #log.debug("active handler: {0}".format(node))
        curTime = time.clock()
        if self.startTime > 0 and curTime - self.startTime > self.maxDelta:
            Util.pickleSave(tree, self.savePath)
        #
        self.startTime = curTime
        #


class TreeSync(object):
    '''
    classdocs
    '''


    def __init__(self, params):
        '''
        Constructor
        '''
        pass


#
#
#

def cmdCapture(dirPath, filePath):
    if os.path.isfile(filePath):
        t = TreeHash.load(filePath)
        t.setRootPath(dirPath)
    else:
        t = TreeHash(dirPath)
    #
    sh = SaveHandler(filePath)
    t.update(sh.handler)
    #t.printTree()
    #t.printHashTable()
    Util.pickleSave(t, filePath)
    return t
    

def cmdTreeSync(sim, sourcePath, targetPath,
    sourceFile="source.bin.bz2", targetFile="target.bin.bz2"):
    # first, take snapshots
    treeA = cmdCapture(sourcePath, sourceFile)
    treeB = cmdCapture(targetPath, targetFile)
    #
    mir = Mirror(treeA, treeB, simulate=sim)
    mir.copy()
    mir.delete()
    pass


def cmdTreeDupCheck(dirPath, filePath):
    # first, take snapshots
    tree = cmdCapture(dirPath, filePath)
    tree.checkDups()

    
def cmdTreeDump(filePath):
    t = TreeHash.load(filePath)
    t.printTree()
    t.printHashTable()


def cmdUsage():
    print "Usage:"
    print "capture:\ntreesync.py -c dir file"
    print "treesync:\ntreesync.py -s simulated source target"
    print "dup check:\ntreesync.py -u dir file"
    print "dump:\ntreesync.py -d file"


# main
if __name__ == "__main__":
    Log.start(level=log.INFO)
    log.info("RUN_BEGIN".format())
    
    # test!
    #cmdTreeSync("testA", "testB")

    options = "csud"
    opts, args = getopt.getopt(sys.argv[1:], options)
    # print opts, args
    if len(opts) <= 0:
        cmdUsage()

    for o, v in opts:
        if o in ("-c"):
            dirPath = args[0]
            filePath = args[1]
            cmdCapture(dirPath, filePath)
        elif o in ("-s"):
            sim = Util.str2bool(args[0]) 
            sourcePath = args[1]
            targetPath = args[2]
            cmdTreeSync(sim, sourcePath, targetPath)
        elif o in ("-u"):
            dirPath = args[0]
            filePath = args[1]
            cmdTreeDupCheck(dirPath, filePath)
        elif o in ("-d"):
            filePath = args[0]
            cmdTreeDump(filePath)
        else:
            cmdUsage()

    log.info("RUN_END".format())
