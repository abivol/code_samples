#!/usr/bin/env python2.7

import sys, csv


#
#
#
def gene_link(gene):
    url = "http://www.genecards.org/cgi-bin/carddisp.pl?gene={0}".format(gene)
    return url


#
#
#
def pathway_link(pathway):
    url = "{0}.html".format(pathway)
    return url


def html_anchor(target, text):
    return "<a target=\"_blank\" href=\"{0}\">{1}</a>".format(target, text)


#
#
#
def read_genes(fn):
    gls = set()
    with open(fn, "r") as f:
        for ln in f:
            g = ln.strip()
            gls.add(g)
    return gls


#
#
#
def print_cell(s):
    print "<td>{0}</td>".format(s)

#
# fn_rank = .csv w/ gene symbol and rank/score
# markup = collection of gene sets to be marked up
# fn_out = output (HTML) file
#
def parse_rank(fn_rank, markup):
    with open(fn_rank, "r") as f_rank:
        csv_rank = csv.reader(f_rank, delimiter="\t")
        print "<html><body><table border=\"1\">"
        print "<thead>"
        print "<tr><th>Rank</th><th>Gene</th><th>Tag</th><th>Score</th></tr>"
        print "</thead>"
        print "<tbody>"
        r = 0
        for row in csv_rank:
            r += 1
            gene = row[0]
            score = row[1]
            print "<tr>"
            # rank
            print_cell( r )
            # gene
            print_cell( "<a target=\"_blank\" href=\"{0}\">{1}</a>".format(gene_link(gene), gene) )
            # markup
            print_cell( ",".join(k if gene in markup[k] else "" for k in markup) )
            # score
            print_cell( score )
            #
            print "</tr>"
        print "</tbody>"
        print "</table></body></html>"


#
#
def parse_pathway(fn_in, markup):
    with open(fn_in, "r") as f_in:
        csv_in = csv.reader(f_in, delimiter="\t")
        print "<html><body><table border=\"1\">"
        print "<thead>"
        print "<tr><th>Gene</th><th>Tag</th><th>Pathway</th></tr>"
        print "</thead>"
        print "<tbody>"
        # read the pathways
        row = csv_in.next()
        pathways = row
        # read the genes
        for row in csv_in:
            print "<tr>"
            # gene
            gene = row[0]
            print_cell( html_anchor(gene_link(gene), gene) )
            # markup
            print_cell( ",&nbsp;".join(k if gene in markup[k] else "" for k in markup) )
            # pathways
            pws = [pathways[i] for i in range(1, len(row)) if row[i] != "NA"]
            print_cell( "&nbsp;&nbsp;,&nbsp;&nbsp;".join(html_anchor(pathway_link(p), p) for p in pws) )
            #
            print "</tr>"
        print "</tbody>"
        print "</table></body></html>"


def test_rank():
    mk_desc = {"AR50":"AR_sgn_50.gmx", "AR300":"AR_sgn_300.gmx"}
    markup = dict()
    for k in mk_desc:
        markup[k] = read_genes(mk_desc[k])
    parse_rank("fisher_pos.rnk", markup)


def test_pathway():
    mk_desc = {"AR50":"AR_sgn_50.gmx", "AR300":"AR_sgn_300.gmx"}
    markup = dict()
    for k in mk_desc:
        markup[k] = read_genes(mk_desc[k])
    parse_pathway("D_median_n.csv", markup)


if __name__ == '__main__':
    test_pathway()
    pass
