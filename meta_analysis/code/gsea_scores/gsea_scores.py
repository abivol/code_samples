#!/usr/bin/env python2.7
'''
@author: Adrian Bivol

Aggregate GSEA pathway enrichment results for meta-analysis:
a) by gene, for each dataset  [gene X pathway matrix]
    genes are sorted by their ranks (in dataset) [rows]
    pathways are sorted by their q-values [cols]
b) by meta-analysis method  [method X pathway matrix]
    methods are not sorted [rows]
    pathways are sorted by their aggregated (median) q-values [cols]

Total pathway counts per gene and respectively per method are added in the last column.

'''

import sys, string, csv, random, os, numpy


def orderByVal(d, reverse=True):
    """
    order dict by value
    """
    for k, v in sorted(d.iteritems(), key=lambda(k,v): v, reverse=reverse):
        yield (k, v)
    pass


class GeneRanks(object):
    """
    read gene ranks file and sort genes by rank
    """
    def __init__(self, path):
        """
        """
        self.path = path
        self.geneRanks = dict()
        with open(path, "rb") as fd_1:
            csv_1 = csv.reader(fd_1, delimiter="\t")
            # skip header
            csv_1.next()
            for row in csv_1:
                self.geneRanks[row[0]] = float(row[4])
    
    # sorted by rank
    def geneOrder(self, reverse=True):
        for t in orderByVal(self.geneRanks, reverse):
            yield t[0] # keys only
        pass


class GSEA_Score(object):
    """
    read pathway score file and genes for each (significant) pathway
    
    """

    def __init__(self, name, path, ranks, qvalCutoff=1e-1):
        #print "score: {0}".format(path)
        self.name = name
        self.geneRanks = ranks
        self.qvalCutoff = qvalCutoff
        self.allGenes = set()
        self.pathwayQval = dict()
        self.pathwayGenes = dict()
        with open(path, "rb") as fd_1:
            csv_1 = csv.reader(fd_1, delimiter="\t")
            # skip header
            csv_1.next()
            for row in csv_1:
                pname = row[0]
                qval = float(row[7]) # "FDR q-val"
                if qval > self.qvalCutoff:
                    continue
                print "pathway: {0}, qval: {1}".format(pname, qval)
                self.pathwayQval[pname] = qval
                # now get the genes
                self.pathwayGenes[pname] = set()
                pp = os.path.join(os.path.dirname(path), "{0}.xls".format(pname))
#                if not os.path.exists(pp):
#                    print "path not found: {0}".format(pp)
#                    continue
                with open(pp, "rb") as fd_2:
                    csv_2 = csv.reader(fd_2, delimiter="\t")
                    # skip header
                    csv_2.next()
                    for r in csv_2:
                        g = r[1] # gene (PROBE)
                        self.allGenes.add(g)
                        self.pathwayGenes[pname].add(g)
                    #
                print "pathway: {0}, genes: {1}".format(pname, len(self.pathwayGenes[pname]))
                #
                #print "genes: {0}".format(",".join(self.pathwayGenes[pname]))
            #
        print "pathways: {0}, genes: {1}".format(len(self.pathwayGenes), len(self.allGenes))
        if len(self.pathwayGenes) <= 0:
            return

        # output gene X pathway matrix
        with open(name+".csv", "w") as fd_3:
            csv_3 = csv.writer(fd_3, delimiter="\t")
            # header
            row = [""]
            row.extend(self.pathwayOrder())
            #row.append("TOTAL")
            csv_3.writerow(row)
            # records [rows]
            for g in self.geneRanks.geneOrder():
                # filter out
                if g not in self.allGenes:
                    continue
                row = [g]
                # http://stackoverflow.com/questions/4260280/python-if-else-in-list-comprehension
                q = [self.geneRanks.geneRanks[g] if g in self.pathwayGenes[p] \
                     else "NA" for p in self.pathwayOrder()]
                row.extend(q)
                #row.append(sum(1 for x in q if x != "NA"))
                csv_3.writerow(row)
        # done
        

    def pathwayOrder(self, reverse=False):
        """
        sorted by rank
        """
        for t in orderByVal(self.pathwayQval, reverse):
            yield t[0] # keys only
        pass



class GSEA_Run(object):
    """
    read GSEA run results; build 2 (positive & negative) score sets,
    unless the direction was already specified (name ends in "_p" or "_n"),
    in which case only consider the positive GSEA score set
    """

    def __init__(self, root):
        # parse the name (meta-analysis method)
        toks = os.path.basename(root).split(".")
        self.name = toks[0]
        print "run: {0}".format(self.name)
        self.direction = None
        if self.name[-2:] == "_p":
            self.direction = "+"
        elif self.name[-2:] == "_n":
            self.direction = "-"
        self.stamp = toks[-1]
        # read gene ranks
        fn = "ranked_gene_list_na_pos_versus_na_neg_{0}.xls".format(self.stamp)
        path = os.path.join(root, fn)
        self.geneRanks = GeneRanks(path)
        # read positive direction first
        fn = "gsea_report_for_na_pos_{0}.xls".format(self.stamp)
        print "fname: {0}".format(fn)
        path = os.path.join(root, fn)
        self.scorePos = GSEA_Score(self.name+"_p", path, self.geneRanks)
        # read negative if needed
        if self.direction is None:
            fn = "gsea_report_for_na_neg_{0}.xls".format(self.stamp)
            print "fname: {0}".format(fn)
            path = os.path.join(root, fn)
            self.scoreNeg = GSEA_Score(self.name+"_n", path, self.geneRanks)
            pass
        pass


class GSEA_Meta(object):
    """
    reads all meta-analysis methods' scores
    creates 2 matrices (method X pathway), for positive and negative directions
    """

    def __init__(self, root):
        self.runs = list()
        for d in os.listdir(root):
            p = os.path.join(root,d)
            if not os.path.isdir(p):
                continue
            print "path: {0}".format(p)
            gsp = GSEA_Run(p)
            #print gsp
            self.runs.append(gsp)

        # create the meta matrices: method X pathway

        # positive direction

        # output matrix
        self.outputMetaMatrix("positive.csv", [r.scorePos for r in self.runs \
            if len(r.scorePos.pathwayQval) > 0])

        # negative direction

        # output matrix
        self.outputMetaMatrix("negative.csv", [r.scoreNeg for r in self.runs \
            if r.direction is None \
            and len(r.scoreNeg.pathwayQval) > 0])
        pass


    def outputMetaMatrix(self, fn, scores):
        pathways = set()
        for s in scores:
            for p in s.pathwayGenes:
                pathways.add(p)
        #
        with open(fn, "w") as fd_1:
            csv_1 = csv.writer(fd_1, delimiter="\t")
            # header
            row = [""]
            row.extend(pathways)
            #row.append("TOTAL")
            csv_1.writerow(row)
            for s in scores:
                row = [s.name]
                # http://stackoverflow.com/questions/4260280/python-if-else-in-list-comprehension
                q = [s.pathwayQval[p] if p in s.pathwayGenes else "NA" for p in pathways]
                row.extend(q)
                #row.append(sum(q))
                csv_1.writerow(row)
            #
        pass


def test_1():
    g = GSEA_Meta("/home/adrian/ucsc_rot1/rot1/results/2012-12-10/rank_pos/output/")
    pass


if __name__ == '__main__':
    test_1()
    pass
