
#
# http://www.broadinstitute.org/gsea/doc/GSEAUserGuideTEXT.htm#_Running_GSEA_from_the%20Command%20Line
#
# java -cp full-path/gsea2.jar -Xmx512m gsea-tool  parameters
#

#
# Actual sample:
#
# java -Xmx512m xtools.gsea.GseaPreranked -gmx gseaftp.broadinstitute.org://pub/gsea/gene_sets/c2.cp.v3.1.symbols.gmt -collapse false -mode Max_probe -norm meandiv -nperm 1000 -rnk d_mean.rnk -scoring_scheme weighted -rpt_label my_analysis -include_only_symbols true -make_sets true -plot_top_x 200 -rnd_seed timestamp -set_max 500 -set_min 15 -zip_report false -out gsea_home/output/nov19 -gui false
#

GSEA_JAR=~/prog/gsea/gsea2-2.0.13.jar
GSEA_TOOL=xtools.gsea.GseaPreranked
GSEA_GENESETS=gseaftp.broadinstitute.org://pub/gsea/gene_sets/c2.cp.v3.1.symbols.gmt
GSEA_NPERM=1000
GSEA_TITLE=MyAnalysis
GSEA_TOPX=200
GSEA_MISC="-collapse false -mode Max_probe -norm meandiv -scoring_scheme weighted -include_only_symbols true -make_sets true -rnd_seed timestamp -set_max 500 -set_min 15 -zip_report false -gui false"

IN_RNK=./rank_combined.rnk
OUT_DIR=./output

#java -Djava.util.Arrays.useLegacyMergeSort=true 
java -cp ${GSEA_JAR} -Xmx1g ${GSEA_TOOL} ${GSEA_MISC} -gmx ${GSEA_GENESETS} -nperm ${GSEA_NPERM} -rnk ${IN_RNK} -rpt_label ${GSEA_TITLE} -plot_top_x ${GSEA_TOPX} -out ${OUT_DIR}
