
printf = function(...) {
    print(sprintf(...))
}


#
# returns proportion of outliers (signal) in distribution of x
#
z.signal = function(x, m=1) {
    x = na.omit(x)
    return( length(which(abs(x - mean(x)) > m*sd(x))) / length(x) );
}


# remove empty and NA items in x
clean.vec = function(x) {
    return( x[ x!="" & (!is.na(x)) ] )
}


#
# convert .gmt file from matrix to list
#
mx.to.list = function(mx, sz.min=15, sz.max=500) {
    # make it a (var. length) list
    mx = as.matrix(mx)
    l.res = list()
    for(r in rownames(mx)) {
        print(r)
        s = clean.vec(mx[r, ])
        if(length(s) >= sz.min & length(s) <= sz.max) {
            l.res[[r]] = s
        }
    }
    return(l.res)
}


#
# read variable row length file (.gmt)
#
read.varflds = function(fn, row.names=1) {
    n.col <- max(count.fields(fn, sep = "\t"))
    df.x = read.delim(fn, fill=TRUE,
        header=FALSE,
        as.is=T, sep="\t", check.names=F, quote="",
        stringsAsFactors=FALSE,
        row.names=row.names,
        col.names=paste("V", seq_len(n.col)))
    return(df.x)
}


#
#
#
save.pathway = function(l.sets, v.names) {
    # fix file name (alphanum)
    fix.fn = function(s) {
        return( gsub("[^[:alnum:]]", "_", s) )
    }
    #
    for(pn in v.names) {
        print(pn)
        pset = l.sets[[pn]]
        fn = fix.fn(pn)
        fn = sprintf("%s.tab", fn)
        #
        mx = matrix(rep(1, length(pset)), byrow=FALSE)
        rownames(mx) = pset
        colnames(mx) = c(pn)
        #
        write.table(mx, fn, quote=F, sep = "\t", col.names=NA, row.names=T)
    }
}


#
# compute GSEA statistic, in parallel (matrix)
# TODO: only works w/ p=0 (KS statistic)
#
# mx = gene metric matrix
# l.sets = list of gene sets (pathways)
#
gsea.stat.par = function(mx, l.sets, p=0) {
    mx.ES = matrix(NA, nrow=length(l.sets), ncol=ncol(mx),
        dimnames=list(rown=names(l.sets), coln=colnames(mx)))
    #
    len.x = nrow(mx)
    len.s = length(l.sets)
    # rows = x ; cols = l.sets
    s.hit = matrix(0, nrow=len.x, ncol=len.s)
    s.miss = matrix(0, nrow=len.x, ncol=len.s)
    t.hit = rep(0, len.s)
    t.miss = rep(0, len.s)
    mx.hit = matrix(FALSE, nrow=len.x, ncol=len.s)
    #
    for(k in 1:ncol(mx)) {
        print(k)
        x = mx[, k]
        x = sort(x, decreasing=T)
        
        # init
        s.hit = s.hit - s.hit
        s.miss = s.miss - s.miss
        t.hit = t.hit - t.hit
        t.miss = t.miss - t.miss
        
        # pre-compute all hit flags
        mx.hit = mx.hit - mx.hit
        for(j in 1:len.s) {
            mx.hit[, j] = names(x) %in% l.sets[[j]]
        }

        # begin
        i = 1
        s.hit[i, ] = 1 * mx.hit[i, ]
        s.miss[i, ] = 1 * (1 - mx.hit[i, ])
        
        # input vector x must be traversed sequentially, in sorted order
        for(i in 2:len.x) {
            # print(sprintf("i: %d", i))
            #
            s.hit[i, ] = s.hit[i-1, ] + 1 * mx.hit[i, ]
            s.miss[i, ] = s.miss[i-1, ] + 1 * (1 - mx.hit[i, ])
        }
        
        #    
        t.hit = s.hit[i, ]
        p.hit = t( apply(s.hit, 1, function(x) { return(x/t.hit); } ) )
        
        t.miss = s.miss[i, ]
        p.miss = t( apply(s.miss, 1, function(x) { return(x/t.miss); } ) )
        
        v.delta = p.hit - p.miss
        a.delta = abs(v.delta)
        v.max = apply(a.delta, 2, max)
        v.ES = vapply(1:len.s, function(j) { return(v.delta[which(a.delta[,j]==v.max[j])[1], j]);  }, 0 )
        names(v.ES) = names(l.sets)
        #
        mx.ES[, k] = v.ES
    }
    return(mx.ES)
}


#
# compute normalized GSEA score (NES)
#
# ES.obs = vector of ES scores for pathways
# ES.null = matrix of null ES values (rows = sets; cols = null samples)
#
gsea.norm = function(ES.obs, ES.null) {
    # compute NES separately for + and -

    fun.mean.pos = function(x) {
        return(mean(x[x >= 0]))
    }

    fun.mean.neg = function(x) {
        return(mean(x[x < 0]))
    }

    # null means
    mean.pos = apply(ES.null, 1, fun.mean.pos)
    mean.neg = apply(ES.null, 1, fun.mean.neg)

    # re-scale + and - parts separately
    fun.scale = function(x) {
        ix.pos = which(x >= 0)
        ix.neg = which(x < 0)
        x[ix.pos] = x[ix.pos] / mean.pos[ix.pos]
        x[ix.neg] = x[ix.neg] / abs(mean.neg[ix.neg])
        return(x)
    }

    # re-scale obs -->  NES
    NES.obs = fun.scale(ES.obs)
    # re-scale null -->  NES
    NES.null = ES.null
    for(i in 1:ncol(NES.null)) {
        NES.null[ , i] = fun.scale(NES.null[ , i])
    }
    
    # compute FDR
    fun.fdr.pos = function(Z, v.obs, v.null) { 
        r.1 = length(which(v.null >= Z)) / length(v.null)
        r.2 = length(which(v.obs >= Z)) / length(v.obs)
        fdr = r.1 / r.2
        print(sprintf("Z: %f, r.1: %f, r.2: %f, fdr: %f", Z, r.1, r.2, fdr))
        return(min(fdr, 1))
    }
    fun.fdr.neg = function(Z, v.obs, v.null) {
        r.1 = length(which(v.null <= Z)) / length(v.null)
        r.2 = length(which(v.obs <= Z)) / length(v.obs)
        fdr = r.1 / r.2
        print(sprintf("Z: %f, r.1: %f, r.2: %f, fdr: %f", Z, r.1, r.2, fdr))
        return(min(fdr, 1))
    }

    FDR = rep(1, length(NES.obs))
    names(FDR) = names(NES.obs)
    v.null = as.vector(NES.null)
    for(s in names(NES.obs)) {
        Z = NES.obs[s]
        if(is.na(Z)) {
            next
        }
        if(Z >= 0) {
            FDR[s] = fun.fdr.pos(Z, NES.obs[NES.obs >= 0], v.null[v.null >= 0])
        } else {
            FDR[s] = fun.fdr.neg(Z, NES.obs[NES.obs < 0], v.null[v.null < 0])
        }
    }
    
    return(list(NES=NES.obs, FDR=FDR))
}


#
# compute p-values from observed and null (permutation)
# ES statistic, using empirical null distribution
#
# ES.obs = 
#
gsea.pval = function(ES.obs, ES.null) {

    # compute one-tailed p-value, per row (pathway)

    #
    #    To address this issue, we determine significance and adjust for multiple
    #    hypotheses testing by independently using the positive and negative sides of the observed
    #    and null bimodal ES distributions. In this way, the significance tests [nominal P value,
    #    familywise-error rate (FWER), and false discovery rate (FDR)] are single tail tests on the
    #    appropriate (positive/negative) side of the null distribution.
    #
    # See: Gene set enrichment analysis
    # www.pnas.org͞cgi͞doi͞10.1073͞pnas.0506580102
    #

    pval.one.tail = function(i) {
        x = ES.obs[i]
        y = ES.null[i, ]
        # no gene set overlap
        if(is.na(x)) {
            return(NA)
        }
        # print(x)
        # hist(y, main=x)
        if(x >= 0) {
            y = y[y>=0]
        } else {
            y = y[y<0]
            x = -x
            y = -y
        }
        return(length(which(y >= x)) / length(y))
    }

    pval = vapply(1:length(ES.obs), pval.one.tail, -1)
    names(pval) = names(ES.obs)
    qval = p.adjust(pval, "fdr")

    return(list(pval=pval, qval=qval))
}


#
# compute gsea scores (ES) for an experiment
#
# l.obs = list of observed dstat vectors, one per dataset
# l.null = list of null dstat matrices, one per dataset
# l.sets = list of sets (pathways)
#
gsea.exper = function(l.obs, l.null, l.sets) {
    # names must match
    stopifnot(all(names(l.obs) == names(l.null)))
    l.exper = list()

    for(i in names(l.obs)) {
        print(i)
        obs = l.obs[[i]]
        null = l.null[[i]]
        # names must match
        stopifnot(all(names(obs) == rownames(null)))
        # compute ES values
        ES.obs = gsea.stat.par(cbind(NULL, obs), l.sets) [, 1]
        ES.null = gsea.stat.par(null, l.sets)
        # compute p- and q- values
        ES.pv = gsea.pval(ES.obs, ES.null)
        # compute NES statistic and FDR (GSEA)
        ES.norm = gsea.norm(ES.obs, ES.null)
        # save results
        l.exper[[i]] = list(obs=ES.obs, null=ES.null,
            pval=ES.pv$pval, qval=ES.pv$qval,
            NES=ES.norm$NES, FDR=ES.norm$FDR
            )
    }
    return(l.exper)
}


#
# fix zero qvalues by replacing with closest (lower or equal) power of 10
#
qval.fix = function(x) {
    m = min(x[x > 0])
    r = 10 ^ floor(log10(m))
    x[x == 0] = r
    return(x)
}


#
# create matrix of gsea pvals for datasets
#
qval.matrix = function(l.exper) {
    mx = matrix(nrow=length(l.exper[[1]]$pval$qval), ncol=length(l.exper))
    x = NULL
    for(i in 1:length(l.exper)) {
        x = l.exper[[i]]$pval$qval
        # update
        mx[, i] = x
    }
    colnames(mx) = names(l.exper)
    rownames(mx) = names(x)

    # fix qvalues
    for(i in 1:ncol(mx)) {
        x = mx[ , i]
        mx[ , i] = qval.fix(x)
    }

    return(mx)
}


#
# create list of sets (pathways) from matrix (data frame)
#
# mx.sets = read.delim("df_nrsets.gmt", header=F, as.is=T, sep="\t", check.names=F, quote="")
# the input file must be a valid (complete) matrix!
#
parse.sets = function(mx.sets) {
    # faster!
    mx.sets = as.matrix(mx.sets)

    # fix the names
    # nms = 

    # parse sets
    genes = NULL
    l.sets = list()
    print(sprintf("BEGIN: read sets"))
    for(i in 1:nrow(mx.sets)) {
        print(sprintf("set: %d", i))
        # 1st column is pathway name
        pr = mx.sets[i, 2:ncol(mx.sets)]
        # strip empty ids
        pr = pr[(pr != "") & (!is.na(pr))]
        # !!! restrict to study background !!!
        # pr = intersect(pr, df.heat$NODE)
        l.sets[[mx.sets[i, 1]]] = pr
        #
        genes = union(genes, pr)
    }
    print(sprintf("END: read sets, count: %d", length(l.sets)))
    print(sprintf("gene count: %d", length(genes)))

    return(l.sets)
}



#
# combine scores across datasets using aggregate function(s)
#
combine.scores = function(l.obs, l.null, fun=mean, overlap=NULL) {
    r.obs = NULL
    r.null = NULL
    
    n.obs = length(l.obs)
    n.null = ncol(l.null[[1]])
    #
    # find overlap in features
    if(is.null(overlap)) {
        overlap = names(l.obs[[1]])
    }
    for(i in names(l.obs)) {
        # stopifnot(all())
        overlap = intersect(overlap, names(l.obs[[i]]))
    }
    # filter input by overlap
    for(i in names(l.obs)) {
        l.obs[[i]] = l.obs[[i]] [overlap]
        l.null[[i]] = l.null[[i]] [overlap, ]
    }

    # build matrices to aggregate
    mx.obs = matrix(NA, nrow=length(overlap), ncol=n.obs,
        dimnames=list(rown=overlap, coln=names(l.obs)))
    mx.null = matrix(NA, nrow=length(overlap), ncol=(n.obs * n.null),
        dimnames=list(rown=overlap, coln=NULL))
    for(i in 1:n.obs) {
        mx.obs[, i] = l.obs[[i]]
        mx.null[, (((i-1)*n.null+1) : (i*n.null)) ] = l.null[[i]]
    }
    # compute the obs aggregate
    r.obs = apply(mx.obs, 1, fun)
    # sample the extended null matrix, and aggregate
    r.null = matrix(NA, nrow=length(overlap), ncol=n.null,
        dimnames=list(rown=overlap, coln=NULL))
    # take the same number of combined nulls (n.null)
    for(i in 1:n.null) {
        # take one sample from the nulls in each dataset (merged)
        cols = sample(1:n.null, n.obs, replace=T)
        cols = cols + n.null * ((1:n.obs) - 1)
        print(cols)
        r.null[, i] = apply(mx.null[, cols], 1, fun)
    }

    return(list(obs=r.obs, null=r.null))
}


#
# combine scores across combinations of datasets
#
scores.combn = function(l.obs, l.null, n.choose, fun=mean, overlap=NULL) {
    stopifnot(n.choose <= length(l.obs))
    comb.mx = combn(length(l.obs), n.choose)
    stopifnot(ncol(comb.mx) < 100)

    r.obs = list()
    r.null = list()

    for(i in 1:ncol(comb.mx)) {
        v.comb = comb.mx[, i]
        ix.comb = sprintf("C_%s", paste(v.comb, collapse="_"))
        print(ix.comb)
        #
        r.combn = combine.scores(l.obs[v.comb], l.null[v.comb], fun, overlap)
        r.obs[[ix.comb]] = r.combn$obs
        r.null[[ix.comb]] = r.combn$null
    }
    return(list(obs=r.obs, null=r.null))
}
