#!/usr/bin/python

import sys, os


#
# fix tab-delimited file
# add tabs to fill in all rows
#
def fix_gmt(fn_in, s_min=5):
    # 1st pass - read max
    ncol = 0
    nrow = 0
    with open(fn_in, "r") as f_in:
        for ln in f_in:
            ln = ln.rstrip()
            # skip empty lines
            if len(ln) <= 0:
                continue
            # 
            nrow += 1
            toks = ln.split("\t")
            if len(toks) > ncol:
                ncol = len(toks)
    #
    print >> sys.stderr, "nrow: {0}, ncol: {1}".format(nrow, ncol-1)

    assert ncol > 0
    nln = 0
    with open(fn_in, "r") as f_in:
        for ln in f_in:
            nln += 1
            ln = ln.rstrip()
            # skip empty lines
            if len(ln) <= 0:
                continue
            # 
            toks = ln.split("\t")
            lt = len(toks)
            # filter sets
            if lt < 1 + s_min:
                print >> sys.stderr, "small set ({0}), line: {1}".format(lt, nln)
                continue
            # fill in tabs to end of row
            nt = ncol - lt
            suff = nt * "\t"
            print "{0}{1}".format("\t".join(toks), suff)



#
# Params: <fn_in>
#
def main(args):
    fix_gmt(args[1], s_min=int(args[2]))


if __name__ == "__main__":
    main(sys.argv)
