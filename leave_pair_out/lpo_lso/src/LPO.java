import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weka.classifiers.functions.LibSVM;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.RemoveRange;


public class LPO {

    private Instances mData, mTrainData, mTestData;

    Map<String, List<Integer>> mTrainClass = new HashMap<String, List<Integer>>(),
        mTestClass = new HashMap<String, List<Integer>>();

    public LPO(Instances data) {
        mData = data;
    }


    private Map<String, List<Integer>> parseClass(Instances pData) {
        // group the instances by class
        Map<String, List<Integer>> classMap = new HashMap<String, List<Integer>>();
        Attribute clAtt = pData.classAttribute();
        for(int i = 0; i < pData.numInstances(); i++) {
            Instance ins = pData.instance(i);
            String clVal = ins.stringValue(clAtt);
            // System.out.println("index: " + i + ", inst: {" + ins + "}, class: " + clVal);
            // add it to hashtable
            List<Integer> clInst = classMap.get(clVal);
            if(clInst == null) {
                clInst = new ArrayList<Integer>();
                classMap.put(clVal, clInst);
            }
            clInst.add(i);
        }

        for(String k : classMap.keySet()) {
            System.out.println("class: " + k + ", ix: " + classMap.get(k));
        }
        return classMap;
    }


    private double Heaviside(double x, double y) {
        if(Math.abs(x - y) <= 1e-6) {
            return(0.5);
        }
        if(x > y) {
            return(1);
        }
        return 0;
    }


    private LibSVM train(Instances data) throws Exception {
        String[] opt = new String[4];
        int i = 0;
        opt[i++] = "-S";
        opt[i++] = "0";
        opt[i++] = "-K";
        opt[i++] = "0";
        LibSVM svm = new LibSVM();
        svm.setOptions(opt);
        svm.buildClassifier(data);
        // System.out.println("getProbabilityEstimates: " + svm.getProbabilityEstimates());
        
        return(svm);
    }


    public static Instances pasteInst(Instances A, Instances B) {
        // assume A > B
        Instances X = null,
            Y = null;
        // deep copy!
        if(A.size() > B.size()) {
            X = new Instances(A);
            Y = B;
        } else {
            X = new Instances(B);
            Y = A;
        }
        // paste Y onto X
        for(int i = 0; i < Y.numInstances(); i++) {
            X.add(Y.instance(i));
        }

        return(X);
    }


    public static int[] arrayInt(List<Integer> pInts) {
        int[] ints = new int[pInts.size()];
        for(int i=0; i < pInts.size(); i++) {
            ints[i] = pInts.get(i);
        }
        return(ints);
    }


    public static Instances rmvAttrib(Instances pInst, int pIx) throws Exception {
        List<Integer> ixs = new ArrayList<Integer>();
        ixs.add(pIx);
        return rmvAttrib(pInst, ixs, false);
    }


    public static Instances rmvAttrib(Instances pInst, List<Integer> pIxs) throws Exception {
        return rmvAttrib(pInst, pIxs, false);
    }


    public static Instances rmvAttrib(Instances pInst, List<Integer> pIxs, boolean pInvert) throws Exception {
        int[] ixs = arrayInt(pIxs);
        Remove rmv = new Remove();
        rmv.setAttributeIndicesArray(ixs);
        //
        rmv.setInputFormat(pInst);
        rmv.setInvertSelection(pInvert);
        Instances newInst = Filter.useFilter(pInst, rmv);
        return(newInst);
    }


    public static Instances rmvInst(Instances pInst, List<Integer> instRmv) throws Exception {
        return(rmvInst(pInst, instRmv, false));
    }


    public static Instances rmvInst(Instances pInst, List<Integer> instRmv, boolean pInvert) throws Exception {
        String[] ropts = new String[2];
        ropts[0] = "-R";
        // indices are 1-based (in strings)!
        String si = Integer.toString(instRmv.get(0) + 1);
        for(int i=1; i < instRmv.size(); i++) {
            si = si + "," + Integer.toString(instRmv.get(i) + 1);
        }
        ropts[1] = si;
        //
        RemoveRange rmv = new RemoveRange();
        rmv.setOptions(ropts);
        rmv.setInvertSelection(pInvert);
        rmv.setInputFormat(pInst);
        Instances newData = Filter.useFilter(pInst, rmv);
        //
        return(newData);
    }


    public static Instances rmvInstAttr(Instances pInst, int pAttrIx, String pAttrVal) throws Exception {
        return(rmvInstAttr(pInst, pAttrIx, pAttrVal, false));
    
    }


    public static Instances rmvInstAttr(Instances pInst, int pAttrIx, String pAttrVal, boolean pInvert) throws Exception {
        Attribute clAtt = pInst.attribute(pAttrIx);
        List<Integer> instRmv = new ArrayList<Integer>();
        for(int i = 0; i < pInst.numInstances(); i++) {
            Instance ins = pInst.instance(i);
            // System.out.println(ins);
            String attrVal = ins.stringValue(clAtt);
            // System.out.println(clVal);
            // add it to remove list
            if (pAttrVal.equals(attrVal)) {
                instRmv.add(i);
            }
        }
        Instances newData = rmvInst(pInst, instRmv, pInvert);
        return(newData);
    }


    /**
     * per study leave-pair-out cross-validation
     * @throws Exception
     */
    private void evalLSO() throws Exception {

        mTrainClass = parseClass(mTrainData);
        mTestClass = parseClass(mTestData);
        // binary classification only!
        if(mTrainClass.keySet().size() < 2 || mTestClass.keySet().size() < 2 ) {
            System.out.println("Insufficient data!");
            return;
        }
        assert(mTrainClass.keySet().size() == 2 && mTestClass.keySet().size() == 2 &&
                mTrainClass.keySet().equals(mTestClass.keySet()));

        double AUC = 0;
        // train / test for each pair left out (a,b) from each class (A,B)
        String[] keys = mTestClass.keySet().toArray(new String[2]);
        List<Integer> A = mTestClass.get(keys[0]);
        assert(A != null);
        assert(A.size() > 0);
        List<Integer> B = mTestClass.get(keys[1]);
        assert(B != null);
        assert(B.size() > 0);

        // test
        for(int a : A) {
            for(int b : B) {
                // make new dataset for training
                List<Integer> ixs = new ArrayList<Integer>();
                ixs.add(a);
                ixs.add(b);
                Instances outTrainData = rmvInst(mTestData, ixs);
                Instances trainData = pasteInst(mTrainData, outTrainData);

                // train
                LibSVM svm = train(trainData);
                // System.out.println("train sz: " + trainData.size());

                Instance ins = mTestData.instance(a);
                double tva = ins.classValue();
                double pva = svm.classifyInstance(ins);
                System.out.println("==) A Index: " + a + ", True: " + tva + ", Pred: " + pva);

                ins = mTestData.instance(b);
                double tvb = ins.classValue();
                double pvb = svm.classifyInstance(ins);
                System.out.println("==) B Index: " + b + ", True: " + tvb + ", Pred: " + pvb);

                // a is always positive class (1.0)
                // b is "negative" (0.0)
                AUC += Heaviside(pva, pvb);
            }
        }
        // adjust AUC
        AUC /= (A.size() * B.size());
        System.out.println("==] AUC: " + AUC);
    }


    /**
     * plain study out cross-validation
     * @throws Exception
     */
    private void evalStudy() throws Exception {
        mTrainClass = parseClass(mTrainData);
        mTestClass = parseClass(mTestData);

        // binary classification only!
        if(mTrainClass.keySet().size() < 2 || mTestClass.keySet().size() < 2 ) {
            System.out.println("Insufficient data!");
            return;
        }
        assert(mTrainClass.keySet().size() == 2 && mTestClass.keySet().size() == 2 &&
                mTrainClass.keySet().equals(mTestClass.keySet()));

        // train
        LibSVM svm = train(mTrainData);
        // System.out.println("train sz: " + trainData.size());

        // evaluate
        int TP = 0, TN = 0, FP = 0, FN = 0;
        String[] keys = mTestClass.keySet().toArray(new String[2]);
        // A is always positive class (1.0)
        // B is "negative" class (0.0)
        List<Integer> A = mTestClass.get(keys[0]);
        assert(A != null);
        assert(A.size() > 0);
        List<Integer> B = mTestClass.get(keys[1]);
        assert(B != null);
        assert(B.size() > 0);

        // test
        for(int a : A) {
            Instance ins = mTrainData.instance(a);
            double tva = ins.classValue();
            double pva = svm.classifyInstance(ins);
            System.out.println("==) A Index: " + a + ", True: " + tva + ", Pred: " + pva);
            if(pva == tva) {
                TP ++;
            } else {
                FN ++;
            }
        }

        for(int b : B) {
            Instance ins = mTrainData.instance(b);
            double tvb = ins.classValue();
            double pvb = svm.classifyInstance(ins);
            System.out.println("==) B Index: " + b + ", True: " + tvb + ", Pred: " + pvb);

            if(pvb == tvb) {
                TN ++;
            } else {
                FP ++;
            }
        }
        // accuracy, etc
        double Accuracy = (TP + TN) / mTestData.size(),
            Precision = TP / (TP + FP),
            Recall = TP / (TP + FN),
            F1 = 2 * (Precision * Recall) / (Precision + Recall)
            ;
        
        System.out.println("Accuracy: " + Accuracy + ", F1: " + F1);
    }



    /**
     * per study leave-pair-out cross-validation
     * @throws Exception
     */
    public void runLSO() throws Exception {

        // pick class to predict
        String[] classList = {"Node", "Bone", "Liver"};
        for(String cl: classList) {
            System.out.println("==] class: " + cl);
            // remove other class attribs.
            List<Integer> ixs = new ArrayList<Integer>();
            for(String c2: classList) {
                if(c2.equals(cl)) {
                    continue;
                }
                ixs.add(mData.attribute(c2).index());
            }
            Instances data1 = rmvAttrib(mData, ixs),
                    data2 = null;
            
            // leave study out
            Attribute attrStudy = data1.attribute("Study");
            for (Enumeration<String> e = attrStudy.enumerateValues(); e.hasMoreElements();) {
                String v = e.nextElement();
                System.out.println("==] study: " + v);
                // remove study
                data2 = rmvInstAttr(data1, attrStudy.index(), v, false);
                mTrainData = rmvAttrib(data2, attrStudy.index());
                mTrainData.setClassIndex(mTrainData.numAttributes() - 1);
                System.out.println("train: " + mTrainData.size());
                // keep study
                data2 = rmvInstAttr(data1, attrStudy.index(), v, true);
                mTestData = rmvAttrib(data2, attrStudy.index());
                mTestData.setClassIndex(mTestData.numAttributes() - 1);
                System.out.println("test: " + mTestData.size());
                // evaluate classifier
                evalLSO();
            }
        }
    }


    /**
     * plain leave-pair-out cross-validation
     * only uses mTrain* members !
     * @throws Exception
     */
    private void evalLPO() throws Exception {

        mTrainClass = parseClass(mTrainData);
        // binary classification only!
        if(mTrainClass.keySet().size() < 2) {
            System.out.println("Insufficient data!");
            return;
        }
        assert(mTrainClass.keySet().size() == 2);

        double AUC = 0;
        // train / test for each pair left out (a,b) from each class (A,B)
        String[] keys = mTrainClass.keySet().toArray(new String[2]);
        List<Integer> A = mTrainClass.get(keys[0]);
        assert(A != null);
        assert(A.size() > 0);
        List<Integer> B = mTrainClass.get(keys[1]);
        assert(B != null);
        assert(B.size() > 0);

        // test
        for(int a : A) {
            for(int b : B) {
                // make new dataset for training
                List<Integer> ixs = new ArrayList<Integer>();
                ixs.add(a);
                ixs.add(b);
                Instances trainData = rmvInst(mTrainData, ixs);

                // train
                LibSVM svm = train(trainData);
                // System.out.println("train sz: " + trainData.size());

                Instance ins = mTrainData.instance(a);
                double tva = ins.classValue();
                double pva = svm.classifyInstance(ins);
                System.out.println("==) A Index: " + a + ", True: " + tva + ", Pred: " + pva);

                ins = mTrainData.instance(b);
                double tvb = ins.classValue();
                double pvb = svm.classifyInstance(ins);
                System.out.println("==) B Index: " + b + ", True: " + tvb + ", Pred: " + pvb);

                // a is always positive class (1.0)
                // b is "negative" (0.0)
                AUC += Heaviside(pva, pvb);
            }
        }
        // adjust AUC
        AUC /= (A.size() * B.size());
        System.out.println("==] AUC: " + AUC);
    }


    /**
     * plain leave-pair-out cross-validation
     * @throws Exception
     */
    public void runLPO() throws Exception {

        // pick class to predict
        String[] classList = {"Node", "Bone", "Liver"};
        for(String cl: classList) {
            System.out.println("==] class: " + cl);
            // remove other class attribs.
            List<Integer> ixs = new ArrayList<Integer>();
            for(String c2: classList) {
                if(c2.equals(cl)) {
                    continue;
                }
                ixs.add(mData.attribute(c2).index());
            }
            Instances data1 = rmvAttrib(mData, ixs);

            // remove study attr
            Attribute attrStudy = data1.attribute("Study");
                mTrainData = rmvAttrib(data1, attrStudy.index());
                mTrainData.setClassIndex(mTrainData.numAttributes() - 1);
                // System.out.println("train: " + mTrainData.size());
                // evaluate classifier
                evalLPO();
        }
    }
}

