

import java.util.ArrayList;
import java.util.List;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.RemoveRange;
import weka.classifiers.functions.LibSVM;



public class wekat1 {


//
//
//

    
public static void main_2(String[] args) throws Exception {
    DataSource source = new DataSource("/home/adrian/tmp/weka/1/breast-cancer.arff");
    Instances data = source.getDataSet();
    // set class attribute
    data.setClassIndex(data.numAttributes() - 1);

    String[] ropts = new String[2];
    ropts[0] = "-R";
    String il = "";
    for(int i = 0; i < data.numInstances(); i++) {
        Instance ins = data.instance(i);
        String v = ins.stringValue(ins.attribute(8));
        System.out.println(v);
        if(v.equals("yes")) {
            System.out.println("A");
            if (! il.equals("")) {
                il += ",";
            }
            il += Integer.toString(i+1);
        }
    }
    ropts[1] = il;
    System.out.println(ropts[1]);
    //
    RemoveRange rm = new RemoveRange();
    rm.setOptions(ropts);
    rm.setInputFormat(data);
    Instances newData = Filter.useFilter(data, rm);

    for(int i = 0; i < newData.numInstances(); i++) {
        Instance ins = newData.instance(i);
        System.out.println(ins);
    }
    
    
    
    String[] opt = new String[6];
    int i = 0;
    opt[i++] = "-S";
    opt[i++] = "0";
    opt[i++] = "-K";
    opt[i++] = "0";
    opt[i++] = "-B";
    opt[i++] = "1";
    LibSVM svm = new LibSVM();
    svm.setOptions(opt);
    svm.buildClassifier(data);
    
    i = 3;
    Instance ins = data.instance(i);
    double p = svm.classifyInstance(ins);
    System.out.println(ins);
    System.out.println(p);
    
    i = 43;
    ins = data.instance(i);
    p = svm.classifyInstance(ins);
    System.out.println(ins);
    System.out.println(p);
}


public static void main_1(String[] args) throws Exception {
    List<String> ropts = new ArrayList<String>();
    ropts.add("-R");
    ropts.add("3");
    ropts.add("5");
    String[] ols = ropts.toArray(new String[ropts.size()]);
    System.out.println(ols);
}
    

public static void main(String[] args) throws Exception {

    DataSource source = new DataSource("gexp_mets.arff");
    Instances data1 = source.getDataSet();
    // remove first attribute (Sample)
    Instances data2 = LPO.rmvAttrib(data1, 0);

    LPO lpo = new LPO(data2);
    lpo.runLPO();
}

}
