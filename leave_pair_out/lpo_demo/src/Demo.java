
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;


public class Demo {


public static void main(String[] args) throws Exception {

    DataSource source = new DataSource("gexp_tst.arff");
    Instances data1 = source.getDataSet();
    data1.setClass(data1.attribute("class"));

    LPO lpo = new LPO(data1);
    lpo.eval();
}

}
