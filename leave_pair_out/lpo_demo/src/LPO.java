import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weka.classifiers.Classifier;
import weka.classifiers.rules.ZeroR;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.RemoveRange;


public class LPO {

    private Instances mData;
    private Map<String, List<Integer>> mDataClass;

    public LPO(Instances data) {
        mData = data;
    }


    private void parseClass() {
        // group the instances by class
        mDataClass = new HashMap<String, List<Integer>>();
        Attribute clAtt = mData.classAttribute();
        for(int i = 0; i < mData.numInstances(); i++) {
            Instance ins = mData.instance(i);
            String clVal = ins.stringValue(clAtt);
            // System.out.println("index: " + i + ", inst: {" + ins + "}, class: " + clVal);
            // add it to hashtable
            List<Integer> clInst = mDataClass.get(clVal);
            if(clInst == null) {
                clInst = new ArrayList<Integer>();
                mDataClass.put(clVal, clInst);
            }
            clInst.add(i);
        }

        for(String k : mDataClass.keySet()) {
            System.out.println("class: " + k + ", ix: " + mDataClass.get(k));
        }
    }


    private double Heaviside(double x, double y) {
        if(Math.abs(x - y) <= 1e-6) {
            return(0.5);
        }
        if(x > y) {
            return(1);
        }
        return 0;
    }


    private Classifier train(Instances data) throws Exception {
        ZeroR zrr = new ZeroR();
        zrr.buildClassifier(data);
        return(zrr);
    }


    public static int[] arrayInt(List<Integer> pInts) {
        int[] ints = new int[pInts.size()];
        for(int i=0; i < pInts.size(); i++) {
            ints[i] = pInts.get(i);
        }
        return(ints);
    }


    public static Instances rmvAttrib(Instances pInst, int pIx) throws Exception {
        List<Integer> ixs = new ArrayList<Integer>();
        ixs.add(pIx);
        return rmvAttrib(pInst, ixs, false);
    }


    public static Instances rmvAttrib(Instances pInst, List<Integer> pIxs) throws Exception {
        return rmvAttrib(pInst, pIxs, false);
    }


    public static Instances rmvAttrib(Instances pInst, List<Integer> pIxs, boolean pInvert) throws Exception {
        int[] ixs = arrayInt(pIxs);
        Remove rmv = new Remove();
        rmv.setAttributeIndicesArray(ixs);
        //
        rmv.setInputFormat(pInst);
        rmv.setInvertSelection(pInvert);
        Instances newInst = Filter.useFilter(pInst, rmv);
        return(newInst);
    }


    public static Instances rmvInst(Instances pInst, List<Integer> instRmv) throws Exception {
        return(rmvInst(pInst, instRmv, false));
    }


    public static Instances rmvInst(Instances pInst, List<Integer> instRmv, boolean pInvert) throws Exception {
        String[] ropts = new String[2];
        ropts[0] = "-R";
        // indices are 1-based (in strings)!
        String si = Integer.toString(instRmv.get(0) + 1);
        for(int i=1; i < instRmv.size(); i++) {
            si = si + "," + Integer.toString(instRmv.get(i) + 1);
        }
        ropts[1] = si;
        //
        RemoveRange rmv = new RemoveRange();
        rmv.setOptions(ropts);
        rmv.setInvertSelection(pInvert);
        rmv.setInputFormat(pInst);
        Instances newData = Filter.useFilter(pInst, rmv);
        //
        return(newData);
    }


    public static Instances rmvInstAttr(Instances pInst, int pAttrIx, String pAttrVal) throws Exception {
        return(rmvInstAttr(pInst, pAttrIx, pAttrVal, false));
    }


    public static Instances rmvInstAttr(Instances pInst, int pAttrIx, String pAttrVal, boolean pInvert) throws Exception {
        Attribute clAtt = pInst.attribute(pAttrIx);
        List<Integer> instRmv = new ArrayList<Integer>();
        for(int i = 0; i < pInst.numInstances(); i++) {
            Instance ins = pInst.instance(i);
            // System.out.println(ins);
            String attrVal = ins.stringValue(clAtt);
            // System.out.println(clVal);
            // add it to remove list
            if (pAttrVal.equals(attrVal)) {
                instRmv.add(i);
            }
        }
        Instances newData = rmvInst(pInst, instRmv, pInvert);
        return(newData);
    }


    /**
     * plain leave-pair-out cross-validation
     * See: http://dx.doi.org/10.1016/j.csda.2010.11.018
     * @throws Exception
     */
    public void eval() throws Exception {

        parseClass();
        // binary classification only!
        if(mDataClass.keySet().size() < 2) {
            System.out.println("Insufficient data!");
            return;
        }
        assert(mDataClass.keySet().size() == 2);

        double AUC = 0;
        // train / test for each pair left out (a,b) from each class (A,B)
        String[] keys = mDataClass.keySet().toArray(new String[2]);
        List<Integer> A = mDataClass.get(keys[0]);
        assert(A != null);
        assert(A.size() > 0);
        List<Integer> B = mDataClass.get(keys[1]);
        assert(B != null);
        assert(B.size() > 0);

        // test
        for(int a : A) {
            for(int b : B) {
                // make new dataset for training
                List<Integer> ixs = new ArrayList<Integer>();
                ixs.add(a);
                ixs.add(b);
                Instances trainData = rmvInst(mData, ixs);

                // train
                Classifier classif = train(trainData);

                Instance ins = mData.instance(a);
                double tva = ins.classValue();
                double pva = classif.classifyInstance(ins);
                System.out.println("A Index: " + a + ", True: " + tva + ", Pred: " + pva);

                ins = mData.instance(b);
                double tvb = ins.classValue();
                double pvb = classif.classifyInstance(ins);
                System.out.println("B Index: " + b + ", True: " + tvb + ", Pred: " + pvb);

                // a is always positive class (1.0)
                // b is "negative" (0.0)
                AUC += Heaviside(pva, pvb);
            }
        }
        // adjust AUC
        AUC /= (A.size() * B.size());
        System.out.println("AUC: " + AUC);
    }
}
