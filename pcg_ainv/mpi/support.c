/* File:    support.c
 *
 * Purpose: Support routines for linear algebra operations on sparse and dense
 * matrices & vectors.
 *
 * Input:   []
 * Output:  []
 *
 * Algorithm: various: dot product, daxpy, create, destroy, etc.
 *
 * Note:
 *
 */

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <mpi.h>

#include "support.h"


const BOOL TRUE = 1,
		FALSE = 0;
const int ROOT = 0;


PVECTOR VectorCreate(SIZE_T tag, SIZE_T count)
{
	// compute size
	SIZE_T sz = sizeof(VECTOR) + // VECTOR struct
		count * sizeof(ITEM_SCALAR); // items array

	//
	PVECTOR v = (PVECTOR) malloc(sz);
	assert(v != NULL);
	memset(v, 0, sz);

	// update sizes
	v->size = sz;
	v->capacity = count;

	return v;
}


void VectorDestroy(PVECTOR *pv)
{
	VALID(pv);
	PVECTOR v = *pv;
	if(v == NULL) {
		return;
	}
	free(v);
	v = 0;
}


/*
 * Set the value of an item in a sparse vector.
 */
void VectorSet(PVECTOR v, SIZE_T index, SIZE_T pos, FLOAT val)
{
	assert(index < v->capacity);
//	assert(val != 0);

	// update
	v->items[index].pos = pos;
	v->items[index].val = val;
	v->end = MAX(v->end, index + 1);
}


void VectorPrint(const PVECTOR v)
{
	if(v == NULL) {
		printf("NULL\n");
		return;
	}
	SIZE_T i = 0;
	for(i = 0; i < v->end; i++) {
		printf("[:%d] "FFMT" ", v->items[i].pos, v->items[i].val);
	}
	printf("\n");
}


void VectorTrace(const PVECTOR v)
{
#ifdef TRACE_ON
	VectorPrint(v);
#endif // TRACE_ON
}


void DVectorPrint(const PFLOAT v, SIZE_T n)
{
	SIZE_T i = 0;
	for(i = 0; i < n; i++) {
		printf(FFMT" ", v[i]);
	}
	printf("\n");
}


void DVectorTrace(const PFLOAT v, SIZE_T n)
{
#ifdef TRACE_ON
	DVectorPrint(v, n);
#endif // TRACE_ON
}


PMATRIX MatrixCreate(SIZE_T count)
{
	// compute size
	SIZE_T sz = sizeof(MATRIX) + // MATRIX struct
		count * sizeof(ITEM_VECTOR); // items array

	//
	PMATRIX m = (PMATRIX) malloc(sz);
	assert(m != NULL);
	memset(m, 0, sz);

	// update sizes
	m->size = sz;
	m->capacity = count;

	return m;
}


void MatrixDestroy(PMATRIX *pm)
{
	VALID(pm);
	PMATRIX m = *pm;
	if(m == NULL) {
		return;
	}
	SIZE_T i = 0;
	// destroy vectors
	for(i = 0; i < m->end; i++) {
		PVECTOR *pv = &m->items[i].vec;
		if(*pv != NULL) {
			VectorDestroy(pv);
		}
	}
	free(m);
	m = 0;
}


/*
 * Store a vector in a sparse matrix.
 */
void MatrixSet(PMATRIX m, SIZE_T index, SIZE_T pos, PVECTOR vec)
{
	assert(index < m->capacity);
	assert(vec != NULL && vec->capacity > 0);

	// update
	m->items[index].pos = pos;
	m->items[index].vec = vec;
	m->end = MAX(m->end, index + 1);
}


void MatrixPrint(const PMATRIX m)
{
	SIZE_T i = 0;
	for(i = 0; i < m->end; i++) {
		printf("[%d:], ", m->items[i].pos);
		VectorPrint(m->items[i].vec);
	}
	printf("\n");
}


void MatrixTrace(const PMATRIX m)
{
#ifdef TRACE_ON
	MatrixPrint(m);
#endif // TRACE_ON
}


// check that vector is compact and sorted
void VectorVerify(const PVECTOR v, FLOAT tol)
{
	assert(v->size > 0);
	assert(v->capacity > 0);
	assert(v->end <= v->capacity);
	int i = 0,
		pos = -1;
	for(i = 0; i < v->end; i++) {
		// sorted?
		assert(v->items[i].pos > pos);
		pos = v->items[i].pos;
		// compact?
		assert(ToleranceTest(v->items[i].val, tol));
	}
}


// (serial) sparse vector dot-product
// assumes vectors are compressed/contiguous and sorted!
FLOAT SparseDotProduct(const PVECTOR v1, const PVECTOR v2)
{
	SIZE_T it1 = 0,
		it2 = 0;
	FLOAT dp = 0.0;
	while(it1 < v1->end && it2 < v2->end) {
		if(v2->items[it2].pos < v1->items[it1].pos) {
			it2 ++;
			continue;
		}
		if(v2->items[it2].pos > v1->items[it1].pos) {
			it1 ++;
			continue;
		}
		// found match!
		assert(v2->items[it2].pos == v1->items[it1].pos);
		dp += v1->items[it1].val * v2->items[it2].val;
		// next
		it1 ++;
		it2 ++;
	}

	return dp;
}


// wipe contents, keep structure
void VectorZero(PVECTOR v)
{
	SIZE_T sz = v->capacity * sizeof(ITEM_SCALAR);
	memset(v->items, 0, sz);
	v->end = 0;
}


void VectorUnit(PVECTOR v, SIZE_T pos)
{
	assert(pos < v->capacity);
	VectorZero(v);
	VectorSet(v, 0, pos, 1.0);
}


// copy contents between vectors
void VectorCopy(PVECTOR dst, const PVECTOR src)
{
	assert(dst->capacity >= src->end);
	VectorZero(dst);
	assert(dst->size >= src->size);
	memcpy(&dst->items, &src->items, src->end * sizeof(ITEM_SCALAR));
	dst->end = src->end;
}


static int CompareScalar(const void *p1, const void *p2)
{
	const PITEM_SCALAR s1 = (PITEM_SCALAR) p1;
	const PITEM_SCALAR s2 = (PITEM_SCALAR) p2;
	assert(s1->pos != s2->pos);
	return (s1->pos - s2->pos);
}


// compress and sort sparse vector into contiguous area at start of array
void VectorCompact(PVECTOR v, FLOAT tol)
{
	// compress (shift-left) over zeros
	SIZE_T i = 0,
		zfirst = 0, // first zero
		zcount = 0; // zero count
//		end = v->end; // new end
	for(i = 0; i < v->end; i++) {
		// found zero?
		if(! ToleranceTest(v->items[i].val, tol)) {
			TRACE(("compact: %d, val: "FFMT"\n", i, v->items[i].val));
			// update zeros
			if(zcount <= 0) {
				zfirst = i;
			}
			zcount ++;
			continue;
		}

		// if zero gap, shift
		if(zcount > 0) {
			v->items[zfirst] = v->items[i];
			// update zero gap
			zfirst ++;
		}
		// otherwise, skip it
	}

	assert(v->end - zcount >= 0);
	// update end
	v->end -= zcount;
	// zero out slack
	memset(&v->items[v->end], 0, zcount * sizeof(ITEM_SCALAR));

	// sort it
	qsort(v->items, v->end, sizeof(ITEM_SCALAR), CompareScalar);
}


// serial daxpy
// assumes sorted, compact, sparse vectors!
// y += a * x
void SparseDaxpy(PVECTOR y, const PVECTOR x, FLOAT a, FLOAT tol)
{
	SIZE_T it_y = 0,
		it_x = 0,
		end_y = y->end,
		end_x = x->end;
	FLOAT res = 0.0;

	// copy or append x to y
	while(it_x < end_x) {

		// only in x (before current pos) ?
		if(x->items[it_x].pos < y->items[it_y].pos) {
			// append x to y
			res = a * x->items[it_x].val;
			if(ToleranceTest(res, tol)) {
				VectorSet(y, y->end, x->items[it_x].pos, res);
			}
			// next x
			it_x ++;
			continue;
		}

		// only in x (after current pos) ?
		if(x->items[it_x].pos > y->items[it_y].pos) {
			// next y
			it_y ++;
			if(it_y >= end_y) {
				// append x to y
				res = a * x->items[it_x].val;
				if(ToleranceTest(res, tol)) {
					VectorSet(y, y->end, x->items[it_x].pos, res);
				}
				it_y = end_y - 1; // fix y iterator
				// next x
				it_x ++;
			}
			continue;
		}

		// match!
		assert(x->items[it_x].pos == y->items[it_y].pos);
		// update y: sum
		res = y->items[it_y].val + a * x->items[it_x].val;
		// always compute! (even zero)
		VectorSet(y, it_y, y->items[it_y].pos, res);
		// next y
		if(it_y + 1 < end_y) {
			it_y ++;
		}
		// next x
		it_x ++;
	}

	// compact dst
	VectorCompact(y, tol);
}


/*
 Allocates and reads sparse matrix from file.

 Sparse matrix file format:
 Vectors must be non-empty and in sorted order.
 Items must be non-zero and in sorted order.

 # FORMAT_BEGIN
 vector_count
 pos item_count (vector 0)
 pos0 item0 pos1 item1 [...] (vector 0)
 pos item_count (vector 1)
 pos0 item0 pos1 item1 [...] (vector 1)
 [...]
 # FORMAT_END
*/
//
PMATRIX MatrixRead(FILE *f)
{
	int vec_cnt = 0,
		item_cnt = 0,
		vec_pos = 0,
		vec_pos_old = -1,
		item_pos = 0,
		item_pos_old = -1,
		i = 0,
		j = 0;
	PMATRIX M = NULL;
	PVECTOR v = NULL;
	FLOAT t = 0.0;

	// vector count
	fscanf(f, "%d", &vec_cnt);
	assert(vec_cnt > 0);
	M = MatrixCreate(vec_cnt);
	VALID(M);

	for(i = 0; i < vec_cnt; i++) {
		// vector pos
		fscanf(f, "%d", &vec_pos);
		assert(vec_pos >= 0);
		// sorted
		assert(vec_pos > vec_pos_old);
		vec_pos_old = vec_pos;
		// vector item count
		fscanf(f, "%d", &item_cnt);
		assert(item_cnt > 0);

		v = VectorCreate(vec_pos, item_cnt);
		VALID(v);
		MatrixSet(M, i, vec_pos, v);

		// read vector items
		item_pos_old = -1;
		for(j = 0; j < item_cnt; j++) {
			// item pos
			fscanf(f, "%d", &item_pos);
			assert(item_pos >= 0);
			// sorted
			assert(item_pos > item_pos_old);
			item_pos_old = item_pos;
			// item val
			fscanf(f, "%le", &t);
			assert(t != 0.0);
			VectorSet(v, j, item_pos, t);
		}
	}

	return M;
}



// create and read sparse matrix from dense matrix format
PMATRIX MatrixDRead(FILE *f, SIZE_T order)
{
	int	i = 0,
		j = 0;
	PMATRIX M = NULL;
	PVECTOR v = NULL;
	FLOAT t = 0.0;

	M = MatrixCreate(order);
	VALID(M);

	for(i = 0; i < order; i++) {
		v = VectorCreate(i, order);
		VALID(v);

		// read vector items
		for(j = 0; j < order; j++) {
			// value
			fscanf(f, "%le", &t);
			if(t != 0.0) {
				VectorSet(v, v->end, j, t);
			}
		}

		if(v->end > 0) {
			VectorCompact(v, FLOAT_MIN);
			MatrixSet(M, M->end, i, v);
		} else {
			VectorDestroy(&v);
		}
	}

	// MatrixCompact(m);
	return M;
}



// Allocates and reads dense vector from file.
//
PFLOAT DVectorRead(FILE *f, SIZE_T cnt)
{
	int i = 0;
	PFLOAT v = calloc(cnt, sizeof(FLOAT));
	VALID(v);

	for(i = 0; i < cnt; i++) {
		fscanf(f, "%le", &v[i]);
	}

	return v;
}




void UT_1()
{
	PVECTOR v = VectorCreate(0, 7);
	VectorSet(v, 2, 15, 1.5);
	VectorSet(v, 1, 14, 3.5);
	VectorPrint(v);
	VectorDestroy(&v);
}


void UT_2()
{
	PVECTOR v1 = VectorCreate(0, 2),
		v2 = VectorCreate(0, 3);
	VectorSet(v1, 0, 1, 1.5);
	VectorSet(v1, 1, 3, 3.5);
	VectorPrint(v1);
	VectorSet(v2, 0, 0, 5);
	VectorSet(v2, 1, 1, 4);
	VectorSet(v2, 2, 3, 7);
	VectorPrint(v2);
	FLOAT dp = SparseDotProduct(v1, v2);
	printf("dotp: "FFMT"\n", dp);
}


void UT_3()
{
	PVECTOR v1 = VectorCreate(0, 2),
		v2 = VectorCreate(0, 3);
	VectorUnit(v1, 0);
	VectorUnit(v2, 2);
	VectorPrint(v1);
	VectorPrint(v2);
}


void UT_4()
{
	PVECTOR v1 = VectorCreate(0, 4);
	VectorSet(v1, 0, 2, 1.5);
	VectorSet(v1, 1, 0, 4);
	VectorSet(v1, 3, 3, 3.5);
	VectorPrint(v1);
	VectorCompact(v1, FLOAT_MIN);
	VectorPrint(v1);
}


void UT_5()
{
	PVECTOR v1 = VectorCreate(0, 5),
		v2 = VectorCreate(0, 5);

	VectorSet(v1, 0, 0, -2.807383e+00);
	VectorSet(v1, 1, 2, -2.148568e-02);
	VectorSet(v1, 2, 3, 1.699880e-02);
	printf("v1:\n");
	VectorPrint(v1);

	VectorSet(v2, 0, 0, -2.807697e+00);
	VectorSet(v2, 1, 3, 1.699880e-02);
	VectorSet(v2, 2, 4, 1.0);
	printf("v2:\n");
	VectorPrint(v2);

	SparseDaxpy(v1, v2, 1, FLOAT_MIN);
	printf("v1:\n");
	VectorPrint(v1);
}


void UT_6()
{
	PVECTOR v1 = VectorCreate(0, 5),
		v2 = VectorCreate(0, 5);

	VectorSet(v1, 0, 0, -2);
	VectorSet(v1, 1, 2, -1);
	VectorSet(v1, 2, 3, -6);
	printf("v1:\n");
	VectorPrint(v1);

	VectorSet(v2, 0, 2, 4);
	VectorSet(v2, 1, 3, 3);
	VectorSet(v2, 2, 4, 2);
	printf("v2:\n");
	VectorPrint(v2);

	SparseDaxpy(v1, v2, 2, FLOAT_MIN);
	printf("v1:\n");
	VectorPrint(v1);
}


void UT_7()
{
	FILE *f = fopen("matrix.txt", "r");
	VALID(f);
	PMATRIX m = MatrixRead(f);
	VALID(m);
	fclose(f);

	MatrixTrace(m);
	MatrixDestroy(&m);

	f = fopen("vector.txt", "r");
	VALID(f);
	PFLOAT v = DVectorRead(f, 5);
	VALID(v);
	fclose(f);

	DVectorTrace(v, 5);
	free(v);
	v = NULL;
}




static int UT_main()
{
//	UT_1();
//	UT_2();
//	UT_3();
//	UT_4();
//	UT_5();
	UT_7();

	return 0;
}
