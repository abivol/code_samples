
# process count
declare -i p=$1
#echo p= $p

# matrix order
declare -i n=$2
#echo n= $n

rm out*.log

mpiexec -l -n $p ./prog5 $n 100 1.0e-50 matrix.txt vector.txt 1>out.log 2>&1

for (( i=0; i<p; i++ ))
do
    #echo $i
    grep ^$i: out.log > out$i.log
done

