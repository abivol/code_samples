/*
 * support.h
 *
 */

#ifndef SUPPORT_H_
#define SUPPORT_H_

#include <float.h>


//#define TRACE_ON
#ifdef TRACE_ON
#define TRACE(x) do { printf x; /*fflush(stdout);*/ } while(0);
#else
#define TRACE(x) ;
#endif // TRACE_ON


// float format (printf)
#define FFMT "%1.7e"

#define FLOAT_MIN DBL_MIN


typedef unsigned char BYTE, *PBYTE;
typedef int SIZE_T, *PSIZE_T;
typedef double FLOAT, *PFLOAT;
typedef int INT;
typedef int BOOL;


extern const BOOL TRUE,
		FALSE;
extern const int ROOT;


#define RANK(r) (rank == r)
#define ISROOT(r) (r == ROOT)
#define IFROOT(x) ISROOT(rank) ? x : 0
#define IFROOT2(x, y) ISROOT(rank) ? x : y
#define VERIFY(err) { assert(err == MPI_SUCCESS); }
#define VALID(ptr) { assert(ptr != NULL); }


#define MIN(a,b) (a < b ? a : b)
#define MAX(a,b) (a > b ? a : b)


// scalar item
typedef struct _ITEM_SCALAR {
	FLOAT __attribute__ ((aligned (sizeof(FLOAT)))) val; // non-zero value
	SIZE_T pos; // nnz position
} ITEM_SCALAR, *PITEM_SCALAR;


// sparse vector (row/column)
typedef struct _VECTOR {
	SIZE_T tag; // id
	SIZE_T size; // mem size, in bytes
	SIZE_T capacity; // allocated count of items
	SIZE_T end; // after last used item
	// scalar array
	ITEM_SCALAR items[1];
} VECTOR, *PVECTOR;


// vector item
typedef struct _ITEM_VECTOR {
	PVECTOR vec; // vector
	SIZE_T pos; // position
} ITEM_VECTOR, *PITEM_VECTOR;


// sparse matrix: a sparse collection of sparse vectors
typedef struct _MATRIX {
	SIZE_T size; // mem size, in bytes
	SIZE_T capacity; // allocated count of items
	SIZE_T end; // after last used item
	// vector array
	ITEM_VECTOR items[1];
} MATRIX, *PMATRIX;


void VectorUpdate(PVECTOR v);
PVECTOR VectorCreate(SIZE_T tag, SIZE_T length);
void VectorDestroy(PVECTOR *v);
void VectorSet(PVECTOR v, SIZE_T index, SIZE_T pos, FLOAT val);
void VectorPrint(const PVECTOR v);
void VectorTrace(const PVECTOR v);
void VectorZero(PVECTOR v);
void VectorUnit(PVECTOR v, SIZE_T pos);
void VectorCopy(PVECTOR dst, const PVECTOR src);
void VectorCompact(PVECTOR v, FLOAT tol);


void DVectorPrint(const PFLOAT v, SIZE_T n);
void DVectorTrace(const PFLOAT v, SIZE_T n);
PFLOAT DVectorRead(FILE *f, SIZE_T cnt);


void SparseDaxpy(PVECTOR y, const PVECTOR x, FLOAT a, FLOAT tol);
FLOAT SparseDotProduct(const PVECTOR v1, const PVECTOR v2);


PMATRIX MatrixCreate(SIZE_T count);
void MatrixDestroy(PMATRIX *m);
void MatrixSet(PMATRIX m, SIZE_T index, SIZE_T pos, PVECTOR vec);
void MatrixPrint(const PMATRIX m);
void MatrixTrace(const PMATRIX m);
PMATRIX MatrixRead(FILE *f);
PMATRIX MatrixDRead(FILE *f, SIZE_T order);


static inline BOOL ToleranceTest(FLOAT f, FLOAT tol)
{
	// NOT abs() !
	return fabs(f) > tol;
}



#endif /* SUPPORT_H_ */
