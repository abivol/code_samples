/* File:    main.c
 *
 * Purpose: entry point and implementation of PCG with AINV
 *
 * Input:
 *    Parameters:
 * 1: matrix order
 * 2: max iterations
 * 3: min tolerance
 * 4: matrix file
 * 5: vector file
 * 6: pivot heuristic (optional)
 * 7: Z tolerance (optional)
 *
 * Output:  solution
 *
 * Algorithm: PCG with AINV
 *
 * Note:
 * build: 'build.sh'
 * see README for details
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#include "support.h"


// global static variables

static int rank = -1,
	comm_sz = 0,
	i = -1,
	loc_i = -1,
	j = -1,
	loc_j = -1;
static MPI_Comm comm;

static PMATRIX glob_A = NULL;
static PFLOAT glob_b = NULL; // dense vector

static int n = 0,
	max_vec = 0; // vector capacity == n

static int sub_cnt_max = 0,
	sub_cnt = 0,
	sub_begin = 0,
	sub_end = 0;

// AINV
static FLOAT Z_tolerance = 1.0e-03;
static PMATRIX Z_subm = NULL;
static PFLOAT P = NULL;
#define HEUR_1 1
#define	HEUR_2 2
static int Pivot_Heur = HEUR_1; // pivot heuristic

// Preconditioned CG
static int max_iter = 100;
static FLOAT min_tol = 1.0e-40;
static PMATRIX A_subm = NULL;
static PFLOAT x = NULL,
		r = NULL,
		r_old = NULL,
		r_full = NULL,
		z = NULL,
		z_old = NULL,
		p = NULL,
		p_full = NULL,
		s = NULL;


/**
 * Function:    MVMultCols
 * Purpose:     parallel multiplication, by columns, of sparse Matrix by dense Vector
 * Input:       matrix, vector
 * Output:      vector
 *
 */
// parallel multiplication, by columns, of sparse Matrix by dense Vector
// y = M * x
// assumes all data is already distributed on processors
// each processor has a column block (of M), a subvector of x,
// and a local (full) vector y
// assumes the sparse structures are compact and sorted
// local vectors y (on each processor) must be added together to produce
// the final result!
void MVMultCols(PFLOAT y, const PMATRIX M, const PFLOAT x)
{
	int it_M = 0,
		it_col = 0,
		pos_M = 0,
		pos_x = 0,
		pos_y = 0;

	// y = 0
	// y += M[,i] * y[i], for column block (Daxpy)

	memset(y, 0, n * sizeof(FLOAT)); // y = 0

	// compute Daxpy (only for non-zero positions)
	for(it_M = 0; it_M < M->end; it_M++) {

		pos_M = M->items[it_M].pos;
		pos_x = pos_M - sub_begin; // sub-vector
		pos_y = pos_M;

		FLOAT a = x[pos_x];
		if(a == 0.0) {
			continue;
		}

		PVECTOR col = M->items[it_M].vec;
//		TRACE(("pos: %d, a: "FFMT", c: \n", pos_x, a));
//		VectorTrace(c);
//		Daxpy(y, c, a, FLOAT_MIN);
		for(it_col = 0; it_col < col->end; it_col++) {
			pos_y = col->items[it_col].pos;
			y[pos_y] += a * col->items[it_col].val;
		}
//		TRACE(("pos: %d, y: \n", pos_x));
//		VectorTrace(y);
	}
}


/**
 * Function:    MVMultRows
 * Purpose:     parallel multiplication, by rows, of sparse Matrix by dense Vector
 * Input:       matrix, vector
 * Output:      vector
 *
 */
// parallel multiplication, by rows, of sparse Matrix by dense Vector
// y = M * x
// assumes all data is already distributed on processors
// each processor has a row block (of M), a (full) vector x,
// and a sub-vector of y
// assumes the sparse structures are compact and sorted
// Result: subvector of final (result) dense vector y
void MVMultRows(PFLOAT y, const PMATRIX M, const PFLOAT x)
{
	int it_M = 0,
		it_row = 0,
		pos_M = 0,
		pos_x = 0,
		pos_y = 0;

//	TRACE(("MVMultRows(), rank: %d\n", rank));
	// y = 0
	// y[i] = M[i,] * x, for row block (dot product)

	memset(y, 0, sub_cnt * sizeof(FLOAT)); // y = 0

	// compute dot product for each row
	for(it_M = 0; it_M < M->end; it_M++) {
		pos_M = M->items[it_M].pos;
		PVECTOR r = M->items[it_M].vec; // row vector of M
//		TRACE(("M row vector:\n"));
//		VectorTrace(r);
//		TRACE(("x vector:\n"));
//		VectorTrace(x);
		// dot-product
		FLOAT dp = 0;
		for(it_row = 0; it_row < r->end; it_row++) {
			pos_x = r->items[it_row].pos;
			dp += r->items[it_row].val * x[pos_x];
		}
		//
		pos_y = pos_M - sub_begin; // sub-vector
		y[pos_y] = dp;
	}
	// done!
}


// multiply dense column subvector y by diagonal (matrix) d
void DiagMult(PFLOAT y, const PFLOAT d)
{
	SIZE_T i = 0;
	for(i = 0; i < sub_cnt; i++) {
		y[i] *= d[i];
	}
}


// dense dot product
FLOAT DotProduct(const PFLOAT v1, const PFLOAT v2)
{
	int i = 0;
	FLOAT dp = 0.0;
	for(i = 0; i < sub_cnt; i++) {
		dp += v1[i] * v2[i];
	}
	//
	MPI_Allreduce(MPI_IN_PLACE, &dp, 1, MPI_DOUBLE, MPI_SUM, comm);

	return dp;
}



/**
 * Function:    M_inv_Mult_r
 * Purpose:     implements M inverse * r in PCG, on mixed sparse-dense structures
 * Input:       M inverse, r
 * Output:      z vector
 *
 * Notes:
 * M inverse is used implicitly, as Z * D_inverse * Z_transpose
 */
void M_inv_Mult_r()
{
	// z = M^(-1) * r, or:
	// z = (Z * (D^(-1) * (Z' * r)))

	// gather r into r_full on all processors
	// necessary for M-V multiplication w/ Z transpose, by row
	MPI_Allgather(r, sub_cnt, MPI_DOUBLE, r_full, sub_cnt, MPI_DOUBLE, comm);
	TRACE(("rank: %d, r_full:\n", rank));
	DVectorTrace(r_full, n);

	// z = Z' * r
//	TRACE(("rank: %d, Zsubm:\n", rank));
//	MatrixTrace(Z_subm);
//	TRACE(("rank: %d, vector r:\n", rank));
//	VectorTrace(r);
	MVMultRows(z, Z_subm, r_full);
	TRACE(("rank: %d, step 1, vector z:\n", rank));
	DVectorTrace(z, sub_cnt);

	// z = D^(-1) * z
	DiagMult(z, P);
	TRACE(("rank: %d, step 2, vector z:\n", rank));
	DVectorTrace(z, sub_cnt);

	// r_full = Z * z (by column)
	// result is partial full column vectors on each processor:
	// need to add them together to obtain final vector!
	MVMultCols(r_full, Z_subm, z);
	TRACE(("rank: %d, step 3, vector r_full:\n", rank));
	DVectorTrace(r_full, n);

	// these full r vectors need to be added up into result vector!
	// MPI_Reduce w/ MPI_IN_PLACE fails on Penguin cluster (bug)!
	// sum r_full (on all) to p_full (on ROOT)
	MPI_Reduce(r_full, // sendbuf
		ISROOT(rank) ? p_full : NULL, // recvbuf
		n, MPI_DOUBLE, MPI_SUM, ROOT, comm);
	if(ISROOT(rank)) {
		// copy p_full to r_full
		memcpy(r_full, p_full, n * sizeof(FLOAT));
		TRACE(("rank: %d, step 4, vector r_full:\n", rank));
		DVectorTrace(r_full, n);
	}

	// z = subvector of r_full
	MPI_Scatter(r_full, sub_cnt, MPI_DOUBLE, z, sub_cnt, MPI_DOUBLE, ROOT, comm);
	TRACE(("rank: %d, recv z:\n", rank));
	DVectorTrace(z, sub_cnt);
}


/**
 * Function:    A_Mult_p
 * Purpose:     implements s = A * p in PCG, on mixed sparse-dense structures
 * Input:       A, p
 * Output:      s vector
 *
 * Notes:
 */
void A_Mult_p()
{
	// gather p into p_full on all processors
	// necessary for M-V multiplication w/ A, by row
	MPI_Allgather(p, sub_cnt, MPI_DOUBLE, p_full, sub_cnt, MPI_DOUBLE, comm);
	TRACE(("rank: %d, p_full:\n", rank));
	DVectorTrace(p_full, n);

	MVMultRows(s, A_subm, p_full);
	TRACE(("rank: %d, vector s:\n", rank));
	DVectorTrace(s, sub_cnt);
}


// y += a * x
void Daxpy(PFLOAT y, const PFLOAT x, FLOAT a)
{
	for(i = 0; i < sub_cnt; i++) {
		y[i] += a * x[i];
	}
}


/**
 * Function:    Preconditioner
 * Purpose:     AINV preconditioner
 * Input:       sparse matrix A
 * Output:      M inverse, as Z * D_inverse * Z_transpose
 *
 */
void Preconditioner()
{
	// AINV preconditioner
	// each processor takes a (continguous) block of Z columns,
	// and computes z[j] and p[j] for each column

	// configuration
	const FLOAT machine_epsilon = DBL_EPSILON;
	const FLOAT P_min = sqrt(machine_epsilon);
	const FLOAT P_fix = 1.2e-21;
	FLOAT P_max = machine_epsilon;

	// allocate per-processor storage

	// A row
	PVECTOR A_row = NULL;
	// A is already on ROOT
	if(! ISROOT(rank)) {
		// A row
		A_row = VectorCreate(0, max_vec); // max sz!
		assert(A_row != NULL);
	}

	// create Z columns
	Z_subm = MatrixCreate(sub_cnt);
	VALID(Z_subm);
	for(loc_j = 0; loc_j < sub_cnt; loc_j++) {
		j = sub_begin + loc_j;
		PVECTOR z = VectorCreate(j, max_vec); // max sz!
		VALID(z);
		VectorUnit(z, j);
		MatrixSet(Z_subm, loc_j, loc_j, z);
	}

	// Z[i] is needed on all processes
	PVECTOR Zi = VectorCreate(0, max_vec); // max sz!
	VALID(Zi);
	PVECTOR Zip = VectorCreate(0, max_vec); // private Zi
	VALID(Zip);

	// create p (scalars): follow Z distribution
	P = calloc(sub_cnt, sub_cnt * sizeof(FLOAT));
	VALID(P);


	// outer loop: rows of A
	SIZE_T A_it = 0;
	for(i = 0; i < n; i++) {
		// processors execute this loop in lockstep (synchronized)

		// each outer loop takes one row of A
		// assumes A is compressed and sorted!
		int A_row_sz = 0;
		// optional reset:
		if(! ISROOT(rank)) {
			VectorZero(A_row);
		}
		// find i-th row of A
		if(ISROOT(rank)) {
			while(glob_A->items[A_it].pos < i) {
				A_it ++;
			}
			// found row?
			if(glob_A->items[A_it].pos == i) {
				A_row = glob_A->items[A_it].vec;
				A_row->tag = i; // row index
				A_row_sz = A_row->size;
			}
		}

		// broadcast the row size
		MPI_Bcast(&A_row_sz, 1, MPI_INT, ROOT, comm);
//		TRACE(("rank: %d, Arow_sz: %d\n", rank, Arow_sz));
		// broadcast the current row from root to all (column) processes
		if(A_row_sz > 0) {
			assert(A_row != NULL);
			MPI_Bcast(A_row, A_row_sz, MPI_BYTE, ROOT, comm);
		}

		// print row in every process
//		TRACE(("rank: %d, A row: %d, Arow_sz: %d\n", rank, i, A_row_sz));
//		if(Arow_sz > 0) {
//			TRACE(("Arow:\n"));
//			VectorTrace(Arow);
//		}

		P_max = sqrt(machine_epsilon);
		// compute p[j] for column block
		// p[j] = a[i] * z[j]
		for(j = MAX(i, sub_begin); j < sub_end; j++) { // column = i, i+1, ..., n
			loc_j = j - sub_begin;
//			TRACE(("j: %d, loc_j: %d, Arow_sz: %d, Arow: %p\n",
//					j, loc_j, Arow_sz, Arow));
			// empty row?
			if(A_row_sz <= 0) {
				P[loc_j] = 0;
			} else {
				P[loc_j] = SparseDotProduct(A_row, Z_subm->items[loc_j].vec);
			}

			// zero pivots break preconditioner!
			switch(Pivot_Heur) {
				case HEUR_1:
					if(P[loc_j] == 0.0) {
						TRACE(("fix zero pivot at %d\n", j));
						P[loc_j] = P_fix;
					}
				break;

				case HEUR_2:
					if(P[loc_j] < P_min) {
						TRACE(("fix pivot at %d\n", j));
						P[loc_j] = MAX(P_min, 0.1 * P_max * 5);
					}
					// update P_max
					if(P[loc_j] < P_max) {
						P_max = P[loc_j];
					}
				break;

				default:
					assert(FALSE && "invalid heuristic value!");
					break;
			}
		}

		// last iteration?
		if(i == n - 1) {
			break;
		}

		// broadcast Zi from its process to the other processes
		int Zi_sz = 0;
		int Zi_rank = i / sub_cnt_max; // rank of processor with Zi
//		TRACE(("rank: %d, Zi_rank: %d\n", rank, Zi_rank));
		if(RANK(Zi_rank)) {
			loc_i = i - sub_begin;
			PVECTOR loc_Zi = Z_subm->items[loc_i].vec;
			VectorCompact(loc_Zi, Z_tolerance); // compact Zi
			VectorCopy(Zi, loc_Zi);
			Zi_sz = Zi->size;
//			TRACE(("source: rank: %d, i: %d, Zi->end: %d, Zi:\n",
//				rank, i, Zi->end));
//			VectorTrace(Zi);
		}

		// broadcast Zi size
		MPI_Bcast(&Zi_sz, 1, MPI_INT, Zi_rank, comm);
		// broadcast Zi
		MPI_Bcast(Zi, Zi_sz, MPI_BYTE, Zi_rank, comm);
//		TRACE(("Zi bcast: rank: %d, i: %d, Zi->end: %d, Zi:\n",
//			rank, i, Zi->end));
//		VectorTrace(Zi);

//		TRACE(("rank: %d, A row: %d, Zi_sz: %d\n", rank, i, Zi_sz));

		// broadcast P[i]
		FLOAT Pi = 0.0;
		int Pi_rank = i / sub_cnt_max; // rank of processor with Pi
		if(RANK(Pi_rank)) { // get Pi
			loc_i = i - sub_begin;
			Pi = P[loc_i];
		}
		MPI_Bcast(&Pi, 1, MPI_DOUBLE, Pi_rank, comm);
//		TRACE(("Pi bcast: rank: %d, row: %d, Pi: "FFMT":\n",
//			rank, i, Pi));

		// compute Z[j] for column block
		for(j = MAX(i + 1, sub_begin); j < sub_end; j++) { // j = i+1, ..., n
			loc_j = j - sub_begin;
			// Z[j] -= (p[j] / p[i]) * Zi
//			TRACE(("Z[j] before:\n"));
//			VectorTrace(Zsubm->items[loc_j].vec);
			// copy Zi
			VectorCopy(Zip, Zi);
//			TRACE(("Pi: "FFMT", P[loc_j]: "FFMT"\n", Pi, P[loc_j]));
			SparseDaxpy(Z_subm->items[loc_j].vec, Zip, (-1.0) * (P[loc_j] / Pi), Z_tolerance);
			// update pos
			Z_subm->items[loc_j].pos = j; // global column
//			TRACE(("Z[j] after:\n"));
//			VectorTrace(Zsubm->items[loc_j].vec);
		}
	}

	// compute D^(-1) : invert P's : P[j] = 1/P[j]
	for(loc_j = 0; loc_j < sub_cnt; loc_j++) {
		assert(P[loc_j] != 0.0); // process breaks down if pivot is zero!
		P[loc_j] = 1 / P[loc_j];
	}


	// print Z & P
	for(loc_j = 0; loc_j < sub_cnt; loc_j++) {
		TRACE(("Z[%d]:\n", Z_subm->items[loc_j].pos));
		VectorTrace(Z_subm->items[loc_j].vec);
		TRACE(("P[%d]:"FFMT"\n", sub_begin + loc_j, P[loc_j]));
	}

	// END of AINV preconditioner

	// clean up this mess
	if(! ISROOT(rank)) {
		VectorDestroy(&A_row);
	}
	VectorDestroy(&Zi);
	VectorDestroy(&Zip);

}


/**
 * Function:    ConjugateGradient
 * Purpose:     Conjugate Gradient algorithm
 * Input:       sparse matrix A, dense vector b
 * Output:      solution vector x, tolerance
 *
 */
void ConjugateGradient()
{
	// Preconditioned CG

	x = calloc(sub_cnt, sizeof(FLOAT)); // x = 0
	VALID(x);
	r = calloc(sub_cnt, sizeof(FLOAT)); // r = 0
	VALID(r);
	r_old = calloc(sub_cnt, sizeof(FLOAT)); // r = 0
	VALID(r_old);
	r_full = calloc(n, sizeof(FLOAT)); // r_full = 0
	VALID(r_full);
	z = calloc(sub_cnt, sizeof(FLOAT)); // z = 0
	VALID(z);
	z_old = calloc(sub_cnt, sizeof(FLOAT)); // z = 0
	VALID(z_old);
	p = calloc(sub_cnt, sizeof(FLOAT)); // p = 0
	VALID(p);
	p_full = calloc(n, sizeof(FLOAT)); // p_full = 0
	VALID(p_full);
	s = calloc(sub_cnt, sizeof(FLOAT)); // s = 0
	VALID(s);


	// first, distribute rows of A
	A_subm = MatrixCreate(sub_cnt);
	VALID(A_subm);
	for(loc_i = 0; loc_i < sub_cnt; loc_i++) {
		i = sub_begin + loc_i;
		PVECTOR a = VectorCreate(i, max_vec); // max sz!
		VALID(a);
		MatrixSet(A_subm, loc_i, loc_i, a);
	}
	// send/receive A rows
	for(i = 0; i < n; i++) {
		int dest = i / sub_cnt_max, // destination rank
			rsz = 0;
		loc_i = i - sub_begin;
//		TRACE(("send A rows, rank: %d, i: %d, dest: %d\n",
//				rank, i, dest));
		if(ISROOT(rank)) { // send
			PVECTOR r = glob_A->items[i].vec;
			// within ROOT, just copy
			if(ISROOT(dest)) {
				assert(A_subm->items[loc_i].vec->capacity >= r->end);
				VectorCopy(A_subm->items[loc_i].vec, r);
				continue;
			}
			// between ROOT and other process
			assert(dest != ROOT);
			rsz = r->size;
//			TRACE(("send A rows, rsz: %d\n", rsz));
			MPI_Send(&rsz, 1, MPI_INT, dest, 0, comm); // row size
			MPI_Send(r, rsz, MPI_BYTE, dest, 0, comm); // row buffer
		} else if(RANK(dest)) { // receive
			MPI_Recv(&rsz, 1, MPI_INT, ROOT, 0, comm, MPI_STATUS_IGNORE); // row size
//			TRACE(("recv A rows, rsz: %d, loc_i: %d\n", rsz, loc_i));
//			assert(rsz <= Asubm->items[loc_i].vec->capacity);
			// local row
			MPI_Recv(A_subm->items[loc_i].vec, rsz, MPI_BYTE, ROOT, 0, comm, MPI_STATUS_IGNORE); // row buffer
			A_subm->items[loc_i].pos = i;
			TRACE(("rank: %d, received A row %d:\n", rank, A_subm->items[loc_i].pos));
			VectorTrace(A_subm->items[loc_i].vec);
		}
	}


	// r = b
	// distribute sub-vectors of b into r
	MPI_Scatter(glob_b, sub_cnt, MPI_DOUBLE, r, sub_cnt, MPI_DOUBLE, ROOT, comm);
	TRACE(("rank: %d, recv r:\n", rank));
	DVectorTrace(r, sub_cnt);

	// z = M_inv * r
	M_inv_Mult_r();

	// p = z
	memcpy(p, z, sub_cnt * sizeof(FLOAT));

	j = 0;
	FLOAT tol = min_tol,
		alpha = 0.0,
		beta = 0.0;
	while(j < max_iter && tol >= min_tol) {
		// update old values
		memcpy(r_old, r, sub_cnt * sizeof(FLOAT)); // r_old = r
		memcpy(z_old, z, sub_cnt * sizeof(FLOAT)); // z_old = z

		// s = A * p
		// uses p_full (as temp)
		A_Mult_p();

		// alpha =
		alpha = DotProduct(r, z) / DotProduct(s, p);

		// x += alpha * p
		Daxpy(x, p, alpha);

		// r -= alpha * s
		Daxpy(r, s, (-1.0) * alpha);

		// z = M_inv * r
		// uses p_full (as temp)
		M_inv_Mult_r();

		// beta =
		beta = DotProduct(r, z) / DotProduct(r_old, z_old);

		// p = z + beta * p
		memset(s, 0, sub_cnt * sizeof(FLOAT)); // s = 0
		Daxpy(s, p, beta); // s += beta * p
		Daxpy(s, z, 1); // s += z
		memcpy(p, s, sub_cnt * sizeof(FLOAT)); // p = s

		// tol =
		tol = DotProduct(r, r);
		TRACE(("PCG it: %d, alpha: "FFMT", beta: "FFMT", tol: "FFMT"\n",
			j, alpha, beta, tol));

		// next
		j++;

		// sync
		MPI_Barrier(comm);
	}

	if(ISROOT(rank)) {
		printf("iterations: %d, tolerance: "FFMT"\n", j, tol);
	}
}


void Cleanup()
{
	// sparse
	MatrixDestroy(&Z_subm);
	MatrixDestroy(&A_subm);

	// dense
	free(P);
	P = NULL;
	free(x);
	x = NULL;
	free(r);
	r = NULL;
	free(r_old);
	r_old = NULL;
	free(r_full);
	r_full = NULL;
	free(z);
	z = NULL;
	free(z_old);
	z_old = NULL;
	free(p);
	p = NULL;
	free(p_full);
	p_full = NULL;
	free(s);
	s = NULL;
}



/**
 * Function:    main
 * Purpose:     read input, start other routines
 * Input:       program input
 * Output:      program output
 *
 */
int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
	comm = MPI_COMM_WORLD;
	MPI_Comm_size(comm, &comm_sz);
	MPI_Comm_rank(comm, &rank);
	TRACE(("\nrank: %d\n", rank));

	if(ISROOT(rank)) {

		// read command line

		if(argc <= 5) {
			printf("Parameters:\n1: matrix order\n2: max iterations\n"
				"3: min tolerance\n4: matrix file\n5: vector file\n"
				"6: pivot heuristic (optional)\n7: Z tolerance (optional)\n");
			return(~0);
		}

		// matrix order
		assert(argc > 1 && "1: matrix order");
		sscanf(argv[1], "%d", &n);
		assert(n > 1);
		max_vec = n;

		// max iterations
		assert(argc > 2 && "2: max iterations");
		sscanf(argv[2], "%d", &max_iter);
		assert(max_iter > 1);

		// min tolerance
		assert(argc > 3 && "3: min tolerance");
		sscanf(argv[3], "%le", &min_tol);
		assert(min_tol > FLOAT_MIN);

		// matrix file
		assert(argc > 4 && "4: matrix file");
		const char *matrix_file = argv[4];

		// vector file
		assert(argc > 5 && "5: vector file");
		const char *vector_file = argv[5];

		// pivot heuristic (optional)
		if(argc > 6) {
			sscanf(argv[6], "%d", &Pivot_Heur);
			assert(Pivot_Heur >= HEUR_1);
		}

		// Z tolerance (optional)
		if(argc > 7) {
			sscanf(argv[7], "%le", &Z_tolerance);
			assert(Z_tolerance > FLOAT_MIN);
		}

		// generate / read input data

		FILE *f = fopen(matrix_file, "r");
		VALID(f);
		glob_A = MatrixDRead(f, n);
		VALID(glob_A);
		fclose(f);
		TRACE(("matrix A:\n"));
		MatrixTrace(glob_A);

		f = fopen(vector_file, "r");
		VALID(f);
		glob_b = DVectorRead(f, n);
		VALID(glob_b);
		fclose(f);
		TRACE(("vector b:\n"));
		DVectorTrace(glob_b, n);
	}

	// broadcast config
	MPI_Bcast(&n, 1, MPI_INT, ROOT, comm); // 1
	MPI_Bcast(&max_vec, 1, MPI_INT, ROOT, comm); // 1
	MPI_Bcast(&max_iter, 1, MPI_INT, ROOT, comm); // 2
	MPI_Bcast(&min_tol, 1, MPI_DOUBLE, ROOT, comm); // 3
	MPI_Bcast(&Pivot_Heur, 1, MPI_INT, ROOT, comm); // 6
	MPI_Bcast(&Z_tolerance, 1, MPI_DOUBLE, ROOT, comm); // 7


	// per-processor distribution
	sub_cnt_max = ceil((FLOAT)n / (FLOAT)comm_sz);
	assert(sub_cnt_max == n/comm_sz && "matrix order must be multiple of process count");
	sub_cnt = sub_cnt_max;
	sub_begin = sub_cnt * rank;
	sub_end = sub_begin + sub_cnt;
	TRACE(("rank: %d, sub_cnt_max: %d, sub_cnt: %d, sub_begin: %d, sub_end: %d\n",
		rank, sub_cnt_max, sub_cnt, sub_begin, sub_end));
	assert(sub_cnt_max > 0);
	assert(sub_cnt > 0);
	assert(sub_cnt <= sub_cnt_max);
	assert(sub_begin >= 0);
	assert(sub_begin <= sub_end);


	//
	Preconditioner();

	//
	ConjugateGradient();


	TRACE(("vector x:\n"));
	DVectorTrace(x, sub_cnt);

	// sync
	MPI_Barrier(comm);

	// gather x subvectors into r_full on root
	MPI_Gather(x, sub_cnt, MPI_DOUBLE, r_full, sub_cnt, MPI_DOUBLE, ROOT, comm);
	if(ISROOT(rank)) {
		printf("solution:\n");
		DVectorPrint(r_full, n);
	}

	//
	Cleanup();

	MPI_Finalize();

	return 0;
}
