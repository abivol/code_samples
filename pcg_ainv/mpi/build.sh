#!/bin/bash

# optimized
mpicc -g -lm -Wall -O3 -o prog5 support.c main.c

# debug
# mpicc -g -lm -Wall -O0 -DTRACE_ON=1 -o prog5 support.c main.c

