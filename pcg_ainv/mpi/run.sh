
# Parameters:
# 1: matrix order
# 2: max iterations
# 3: min tolerance
# 4: matrix file
# 5: vector file
#6: pivot heuristic (optional)
#7: Z tolerance (optional)

echo
echo Heuristic 1:
mpiexec -n 4 ./prog5 32 100 1.0e-50 matrix.txt vector.txt 1

echo
echo Heuristic 2:
mpiexec -n 4 ./prog5 32 100 1.0e-50 matrix.txt vector.txt 2

echo
echo Expected solution:
cat solution.txt

