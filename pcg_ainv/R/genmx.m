
# Matlab / Octave
# Generate Sparse random SPD matrix

# order
n = 16
# density
d = 0.213

# random sparse matrix
M1 = sprand(n, n, d)

# SPD
M2 = M1' * M1

# full format
fM = full(M2)

# inverse
iM = inv(fM)

# random vector
b = rand(1, n)

# solution x
x = iM * b'


# save all
save -ascii matrix.txt fM
save -ascii vector.txt b
x1 = x'
save -ascii solution.txt x1

