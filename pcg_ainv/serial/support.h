
#include <stdio.h>

#ifndef SUPPORT_H_
#define SUPPORT_H_

#define TRACE_ON
#ifdef TRACE_ON
#define TRACE(x) do { printf x; /*fflush(stdout);*/ } while(0);
#else
#define TRACE(x) ;
#endif // TRACE_ON

#define TRACE_FN() TRACE(("%s\n", __FUNCTION__));
#define PRINT_FN() printf("%s\n", __FUNCTION__);

#define countof(x) (sizeof(x) / sizeof((x)[0]))

typedef int SIZE_T, *PSIZE_T;
typedef double FLOAT, *PFLOAT;
typedef int INT, *PINT;
typedef int BOOL, *PBOOL;
typedef unsigned char BYTE, *PBYTE;

extern const BOOL TRUE,
		FALSE;
extern const int ROOT;

typedef struct _VECTOR {
	PFLOAT start; // contiguous in memory
	SIZE_T size;
} VECTOR, *PVECTOR;

// Compressed Sparse Column matrix format
// http://web.eecs.utk.edu/~dongarra/etemplates/node374.html
typedef struct _MATRIX {
	SIZE_T nrow; // number of rows
	SIZE_T ncol; // number of columns
	SIZE_T nnzero; // number of non-zeros
	PFLOAT val; // array of non-zeros (in column-major order), concatenated
	PSIZE_T row_ind; // row indexes for val (in matrix)
	PSIZE_T col_ptr; // index in val where column begins (in matrix)
} MATRIX, *PMATRIX;


// CCS iterator (in column-major order)
typedef struct _CCS_ITERATOR {
	PMATRIX mat;
	SIZE_T col; // column
	SIZE_T row; // row
	SIZE_T xrow; // row index
	SIZE_T xrow_begin;
	SIZE_T xrow_end;
	PFLOAT val;

} CCS_ITERATOR, *PCCS_ITERATOR;


#define MAX(a, b) (a > b ? a : b)


PMATRIX ReadMatrix(FILE* f);
PVECTOR ReadVector(FILE* f, SIZE_T sz);
void DestroyMatrix(PMATRIX *pm);
void DestroyVector(PVECTOR *pv);
PVECTOR CreateVector(SIZE_T sz);
PVECTOR GenerateRandomVector();
PMATRIX GenerateRandomMatrix();
void PrintMatrix(const PMATRIX m);
void PrintVector(const PVECTOR v);
void TraceMatrix(const PMATRIX m);
void TraceCRF(const PMATRIX m);
void TraceVector(const PVECTOR v);

int UT_support();


#endif /* SUPPORT_H */
