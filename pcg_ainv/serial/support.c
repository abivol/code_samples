
/* File:    support.c
 *
 * Purpose:
 * Sparse matrix and vector primitives
 *
 * Input:   []
 * Output:  []
 *
 * Algorithm:
 *
 * Note:
 *
 */

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <memory.h>
#include <math.h>


#include "support.h"


const BOOL TRUE = 1,
		FALSE = 0;
const int ROOT = 0;


inline FLOAT round(FLOAT x)
{
    return (x > 0.0) ? floor(x + 0.5) : ceil(x - 0.5);
}


// read generic list from file
void ReadList(FILE *f, const char *fmt, SIZE_T sz, SIZE_T cnt, void *buf)
{
    PBYTE p0 = (PBYTE) buf,
        p = NULL;
    for(p = p0; p < p0 + sz * cnt; p += sz) {
            fscanf(f, fmt, p);
    }
}


static void AllocMatrix(const PMATRIX m)
{
    // alloc space for lists
    m->val = (PFLOAT) calloc(m->nnzero, sizeof(FLOAT));
    m->row_ind = (PSIZE_T) calloc(m->nnzero, sizeof(SIZE_T));
    m->col_ptr = (PSIZE_T) calloc(m->ncol + 1, sizeof(SIZE_T));
}


// create matrix
PMATRIX CreateMatrix(SIZE_T nrow, SIZE_T ncol, SIZE_T nnzero)
{
    PMATRIX m = (PMATRIX) calloc(1, sizeof(MATRIX));
    m->nrow = nrow;
    m->ncol = ncol;
    m->nnzero = nnzero;
    // alloc space
    AllocMatrix(m);

    return m;
}


PMATRIX ReadMatrix(FILE *f)
{
    PMATRIX m = (PMATRIX) calloc(1, sizeof(MATRIX));
    // read matrix order
    fscanf(f, "%u", &m->nrow);
/*
    fscanf(f, "%u", &m->ncol);
*/
    // assume square matrix
    m->ncol = m->nrow;
    // read number of nonzeros
    fscanf(f, "%u", &m->nnzero);
    // alloc space for lists
    AllocMatrix(m);
    // read the lists
    ReadList(f, "%le", sizeof(FLOAT), m->nnzero, m->val);
    ReadList(f, "%u", sizeof(SIZE_T), m->nnzero, m->row_ind);
    ReadList(f, "%u", sizeof(SIZE_T), m->nrow + 1, m->col_ptr);

    return m;
}


static void AllocVector(const PVECTOR v)
{
    // alloc space for lists
    v->start = (PFLOAT) calloc(v->size, sizeof(FLOAT));
}


// create vector
PVECTOR CreateVector(SIZE_T sz)
{
    PVECTOR v = (PVECTOR) calloc(1, sizeof(VECTOR));
    v->size = sz;
    // alloc space
    AllocVector(v);

    return v;
}


void ZeroVector(PVECTOR v)
{
	size_t sz = v->size * sizeof(FLOAT);
	memset(v->start, 0, sz);
}


// read vector from file
PVECTOR ReadVector(FILE* f, SIZE_T sz)
{
    PVECTOR v = CreateVector(sz);
    // read the list
    ReadList(f, "%le", sizeof(FLOAT), v->size, v->start);

    return v;
}


void DestroyMatrix(PMATRIX* pm)
{
    assert(pm != NULL);
    PMATRIX m = *pm;
    assert(m != NULL);
    free(m->val);
    free(m->row_ind);
    free(m->col_ptr);
    //
    free(m);
    *pm = NULL;
}


void DestroyVector(PVECTOR* pv)
{
    assert(pv != NULL);
    PVECTOR v = *pv;
    assert(v != NULL);
    free(v->start);
    free(v);
    *pv = NULL;
}


static inline int RandomInt(int min, int max)
{
	assert(min <= max);
	// y = a * x + b
	FLOAT a = (FLOAT)(max - min) / (FLOAT)(RAND_MAX - 0),
			b = min;
	int x = random(),
			y = round(a * x + b);
	assert(y >= min && y <= max);
	return y;
}


static inline FLOAT RandomFloat()
{
	int sign = (random() > RAND_MAX / 2) ? 1 : -1; // 50% positive
    FLOAT x = sign * 10 * (FLOAT)random() / (FLOAT)RAND_MAX;
    int p = RandomInt(-4, 4);
//    TRACE(("%s() x: %g, p: %d\n",__FUNCTION__, x, p));
    return x * pow(10.0, p);
}


// generate random list of FLOATs
static void GenerateRandomFloats(SIZE_T cnt, PFLOAT buf)
{
    PFLOAT f = NULL;

    // generate
    for(f = buf; f < buf + cnt; f++) {
        *f = RandomFloat();
    }
}


// generate random vector into v
PVECTOR GenerateRandomVector(SIZE_T sz)
{
    PVECTOR v = (PVECTOR) calloc(1, sizeof(VECTOR));
    v->size = sz;
    // alloc space
    AllocVector(v);
    // fill
    GenerateRandomFloats(v->size, v->start);

    return v;
}


// generate random matrix
PMATRIX GenerateRandomMatrix(SIZE_T nrow, SIZE_T ncol, FLOAT pnz)
{
//    TRACE_FN();
    if(pnz <= 0 || pnz >= 0.5) {
    	pnz = 0.12633;
    }
    FLOAT p = 0;
    SIZE_T i = 0,
            j = 0,
            k = 0;
    PMATRIX m = (PMATRIX) calloc(1, sizeof(MATRIX));
    // read matrix order
    m->nrow = nrow;
    m->ncol = ncol;
    m->nnzero = m->nrow * m->ncol * pnz * 2.5; // worst case
    TRACE(("%s() nrow: %d, ncol: %d, nnzero: %d\n",__FUNCTION__, m->nrow, m->ncol, m->nnzero));
    // alloc space for lists
    AllocMatrix(m);
    // fill
    GenerateRandomFloats(m->nnzero, m->val);
    // distribute non-zeros at random distances
    // ignore excess non-zeros
    for(i = 0; i < m->ncol; i++) {
        BOOL col_start = TRUE;
        for(j = 0; j < m->nrow; j++) {
            p = (float)random() / RAND_MAX;
            if(p < pnz) { // non-zero?
                m->row_ind[k] = j;
                if(col_start) {
                    m->col_ptr[i] = k;
                    col_start = FALSE;
                }
//                TRACE(("nonzero (r): k: %d, row: %d, col: %d, val: %g\n", k, i, j, m->vals[k]));
                k++;
                assert(k < m->nnzero);
            }
        }
        // allow zero columns
        if(col_start) {
        	m->col_ptr[i] = k;
        }

/*
        // don't allow empty columns
        if(col_start) {
            m->row_ind[k] = RandomInt(0, m->ncol - 1);
            m->col_ptr[i] = k;
//            TRACE(("nonzero (e): k: %d, row: %d, col: %d, val: %g\n", k, i, m->cols[k], m->vals[k]));
            k++;
            assert(k < m->nnzero);
        }
*/
    }
    //
    m->col_ptr[i] = k;
    //
    m->nnzero = k; // truncate
    TRACE(("%s() nrow: %d, ncol: %d, nnzero: %d\n", __FUNCTION__, m->nrow, m->ncol, m->nnzero));
    return m;
}


static inline void PrintFloat(FLOAT f)
{
    const char FMT[] = "%1.6e ";
    printf(FMT, f);
}


// prints transpose!
void TransposeMatrix(const PMATRIX m, PMATRIX t);
void PrintMatrix(const PMATRIX m)
{
    SIZE_T i = 0,
        j = 0,
        k = 0,
        kx = 0;
    FLOAT f = 0;

    // transpose
    PMATRIX t = CreateMatrix(m->ncol, m->nrow, m->nnzero);
	TransposeMatrix(m, t);

	// print t
    for(i = 0; i < t->ncol; i++) { // cols
        k = t->col_ptr[i];
        kx = t->col_ptr[i+1];
/*
        TRACE(("k: %d, kx: %d\n", k, kx));
*/
        for(j = 0; j < t->nrow; j++) { // rows
            // assuming rows are in order
            if(k < kx && j == t->row_ind[k]) {
                f = t->val[k];
                k++;
            } else {
                f = 0;
            }
            PrintFloat(f);
        }
        printf("\n");
    }

    // free
    DestroyMatrix(&t);
}


void TraceMatrix(const PMATRIX m)
{
#ifdef TRACE_ON
	PrintMatrix(m);
#endif // TRACE_ON
}


void PrintVector(const PVECTOR v)
{
    PFLOAT f = NULL;
    for(f = v->start; f < v->start + v->size; f++) {
        PrintFloat(*f);
    }
    printf("\n");
}


void PrintCCS(const PMATRIX m)
{
    // header
    printf("%d\n", m->nrow);
    printf("%d\n", m->nnzero);
    // lists
    PFLOAT f = NULL;
    for(f = m->val; f < m->val + m->nnzero; f++) {
        PrintFloat(*f);
    }
    printf("\n");
    PSIZE_T d = 0;
    for(d = m->row_ind; d < m->row_ind + m->nnzero; d++) {
        printf("%d ", *d);
    }
    printf("\n");
    for(d = m->col_ptr; d < m->col_ptr + m->nrow + 1; d++) {
        printf("%d ", *d);
    }
    printf("\n");
}


void TraceCCS(const PMATRIX m)
{
#ifdef TRACE_ON
	PrintCCS(m);
#endif // TRACE_ON
}


void TraceVector(const PVECTOR v)
{
#ifdef TRACE_ON
    PrintVector(v);
#endif // TRACE_ON
}


FLOAT DotProduct(const PVECTOR v1, const PVECTOR v2)
{
	size_t i = 0;
	assert(v1->size == v2->size);
	FLOAT dp = 0.0;
	for(i = 0; i < v1->size; i++) {
		dp += v1->start[i] * v2->start[i];
	}

	return dp;
}


FLOAT Norm(const PVECTOR v)
{
	return sqrt(DotProduct(v, v));
}


// y = y + x * alpha
void Saxpy(const PVECTOR x, FLOAT a, PVECTOR y)
{
	assert(x->size == y->size);
	size_t i = 0,
		n = x->size;
	for(i = 0; i < n; i++) {
		y->start[i] += x->start[i] * a;
	}
}


// M-V mult by column
void MVMult(const PMATRIX m, const PVECTOR x, PVECTOR y)
{
    SIZE_T i = 0,
        j = 0,
        k = 0,
        row_begin = 0,
        row_end = 0;

	// y = 0
	ZeroVector(y);

    // compute vector y as linear combination of column vectors of A
    for(i = 0; i < m->ncol; i++) { // cols
        row_begin = m->col_ptr[i];
        row_end = m->col_ptr[i+1];
/*
        TRACE(("k: %d, kx: %d\n", k, kx));
*/
        for(k = row_begin; k < row_end; k++) { // rows
        	j = m->row_ind[k]; // row
			// update y
			y->start[j] += m->val[k] * x->start[i];
        }
    }
}


void CopyVector(PVECTOR dst, const PVECTOR src)
{
	assert(dst->size = src->size);
	memcpy(dst->start, src->start, src->size * sizeof(FLOAT));
}



void CCSIterator(const PMATRIX mat, PCCS_ITERATOR it)
{
	memset(it, 0, sizeof(CCS_ITERATOR));
	it->mat = mat;
	it->col = -1; // before begin
}


BOOL CCSUpdate(PCCS_ITERATOR it)
{
	const PMATRIX mat = it->mat;
	do {
		it->xrow = it->xrow_begin = mat->col_ptr[it->col];
		it->xrow_end = mat->col_ptr[it->col + 1];
		// zero column?
		if(it->xrow_begin >= it->xrow_end) {
			it->col++; //skip
			continue;
		}
		// update
		it->row = mat->row_ind[it->xrow];
		it->val = &mat->val[it->xrow];
		return TRUE;

	} while(it->col < it->mat->ncol);

	return FALSE;
}


BOOL CCSNextNZ(PCCS_ITERATOR it)
{
	const PMATRIX mat = it->mat;

	// begin?
	if(it->col < 0) {
		it->col = 0;
		return CCSUpdate(it);
	}

	// next iteration
	it->xrow ++;
	if(it->xrow >= it->xrow_end) {
		it->col++;
		if(it->col >= mat->ncol) {
			return FALSE;
		}
		// update
		return CCSUpdate(it);
	}

	// update
	it->row = mat->row_ind[it->xrow];
	it->val = &mat->val[it->xrow];
	return TRUE;
}


BOOL CCSSeek(PCCS_ITERATOR it, SIZE_T col)
{
	it->col = col;
	return CCSUpdate(it);
}


/*
BOOL CCSNext(PCCS_ITERATOR it)
{
	const PMATRIX mat = it->mat;

	// save current position
	SIZE_T c_col = it->col,
		c_row = it->row,
		n_col = 0,
		n_row = 0,
		i = 0,
		j = 0;
	// loop cols and rows until next non-zero
	BOOL nnz = CCSNextNZ(it);
	if(nnz) {
		n_col = it->col;
		n_row = it->row;
	} else { // to end
		n_col = mat->ncol - 1;
		n_row = mat->nrow - 1;
	}
	// loop
	for(i = c_col; i <= n_col; i++) {

	}
}
*/


// challenge: transpose matrix directly in CCS format!
// not efficient, but very rarely used..
void TransposeMatrix(const PMATRIX m, PMATRIX t)
{
	CCS_ITERATOR it = {0};
	SIZE_T row = 0,
		r = 0,
		t_val = 0,
		t_row_ind = 0,
		t_col_ptr = 0;

	assert(t->nnzero == m->nnzero);
	assert(t->nrow == m->ncol);
	assert(t->ncol == m->nrow);

	PFLOAT column = calloc(t->nrow, sizeof(FLOAT));
	assert(column != NULL);
	while(row < m->nrow) {
		TRACE(("row: %d\n", row));
		// sweep the m matrix and save all elements in a row of m (-> column of t)
		CCSIterator(m, &it);
		while(CCSNextNZ(&it)) {
//			TRACE(("m col: %d, row: %d, val: %1.3e\n", it.col, it.row, *it.val));
			//
			if(it.row == row) {
				//
				TRACE(("column[%d]: %1.3e\n", it.col, *it.val));
				column[it.col] = *it.val;
			}
		}
		// add column to t
		BOOL col_start = TRUE;
		for(r = 0; r < t->nrow; r++) {
			// non-zero?
			if(column[r] != 0) {
				t->val[t_val] = column[r];
				t->row_ind[t_row_ind] = r;
				if(col_start) {
					TRACE(("col_start: %d, %d\n", t_col_ptr, t_val));
					t->col_ptr[t_col_ptr] = t_val;
					t_col_ptr ++;
					col_start = FALSE;
				}
				TRACE(("nnz @ %d, val: %1.3e\n", r, column[r]));
				// next
				t_val ++;
				t_row_ind ++;
			}

		}
		// handle empty column!
		if(col_start) {
			TRACE(("empty col: %d, %d\n", t_col_ptr, t_val));
			t->col_ptr[t_col_ptr] = t_val;
			t_col_ptr++;
		}

		// next row
		row++;
		memset(column, 0, t->nrow * sizeof(FLOAT));
	}
    // fix last col_ptr
    t->col_ptr[t_col_ptr] = t_val;

    free(column);
}


// challenge: trace of matrix directly in CCS format!
// not efficient, but very rarely used..
FLOAT TraceOfMatrix(const PMATRIX m)
{
	CCS_ITERATOR it = {0};
	FLOAT tr = 0;

	assert(m->ncol == m->nrow); // square!

	// sweep the matrix and add all elements on the main diagonal
	CCSIterator(m, &it);
	while(CCSNextNZ(&it)) {
//			TRACE(("m col: %d, row: %d, val: %1.3e\n", it.col, it.row, *it.val));
		if(it.col != it.row) {
			continue;
		}
		tr += *it.val;
	}

	return tr;
}


//
// M-M multiplication directly in CCS format!
void MMMult(const PMATRIX a, const PMATRIX b, PMATRIX *c)
{
	CCS_ITERATOR it = {0};
	SIZE_T nnz = MAX(a->nnzero, b->nnzero),
		row = 0,
		r = 0,
		t_val = 0,
		t_row_ind = 0,
		t_col_ptr = 0;

	assert(a->ncol == b->nrow);
	PMATRIX t = CreateMatrix(a->nrow, b->ncol, nnz);

	PFLOAT column = calloc(t->nrow, sizeof(FLOAT));
	assert(column != NULL);

}

///////////////////////////////////////////////////////////////////////////////


static void UT_1()
{
    PRINT_FN();
    PMATRIX m = GenerateRandomMatrix(4, 7, 0.2);
    PrintMatrix(m);
    DestroyMatrix(&m);
}


static void UT_2()
{
    PRINT_FN();
    PMATRIX m = GenerateRandomMatrix(7, 7, 0.2);
    PrintMatrix(m);
    PrintCCS(m);
    DestroyMatrix(&m);
}


static void UT_3()
{
    PRINT_FN();
    FILE *f = fopen("matrix1.txt", "r");
    PMATRIX m = ReadMatrix(f);
    PrintCCS(m);
    PrintMatrix(m);
    DestroyMatrix(&m);
    fclose(f);
}


static void UT_4()
{
    PRINT_FN();
    PVECTOR v = GenerateRandomVector(7);
    PrintVector(v);
    DestroyVector(&v);
}


static void UT_5()
{
    PRINT_FN();
    FILE *f = fopen("vector1.txt", "r");
    PVECTOR v = ReadVector(f, 7);
    PrintVector(v);
    DestroyVector(&v);
    fclose(f);
}


static void UT_6()
{
    PRINT_FN();
    PMATRIX m = GenerateRandomMatrix(7, 7, 0.2);
    PVECTOR x = GenerateRandomVector(7),
    	y = GenerateRandomVector(7);
    printf("matrix:\n");
    PrintMatrix(m);
    printf("vector x:\n");
    PrintVector(x);
    MVMult(m, x, y);
    printf("vector y:\n");
    PrintVector(y);
    DestroyMatrix(&m);
    DestroyVector(&x);
    DestroyVector(&y);
}


static void UT_7()
{
    PRINT_FN();
    PMATRIX m = GenerateRandomMatrix(7, 5, 0.3),
    		t = CreateMatrix(5, 7, m->nnzero);
    PrintMatrix(m);
    PrintCCS(m);
    TransposeMatrix(m, t);
    PrintMatrix(t);
    DestroyMatrix(&m);
}


static void UT_8()
{
    PRINT_FN();
    PMATRIX m = GenerateRandomMatrix(5, 5, 0.42);
    PrintMatrix(m);
    PrintCCS(m);
    FLOAT tr = TraceOfMatrix(m);
    printf("trace: %1.3e\n", tr);
    DestroyMatrix(&m);
}



static int main()
{
    PRINT_FN();
    UT_2();
/*
    UT_1();
    UT_3();
    UT_4();
    UT_5();
    UT_6();
    UT_7();
    UT_8();
*/

    return 0;
}
