import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.bson.types.ObjectId;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;


/**
 * @author abivol
 *
 * Pair of Object, DBObject as loaded from DB.
 */
class Serialized {
	Object object;
	DBObject dbobj;
	public Serialized(Object object, DBObject dbobj) {
		super();
		this.object = object;
		this.dbobj = dbobj;
	}
}


/**
 * @author abivol
 *
 * Manages object serialization to/from Mongo DB.
 * 
 * References:
 * http://tutorials.jenkov.com/java-reflection/generics.html
 * http://www.angelikalanger.com/GenericsFAQ/JavaGenericsFAQ.html#Reflection
 * 
 */
public class MongoORM {
	static Logger LOG = Logger.getLogger("MongoORM");
	static final String FLD_ID = "_id";
	static final String FLD_TYPE = "_$type";
	static final String FLD_OID = "_$oid";
	static final String FLD_OCN = "_$ocn";

    /** use map<Object,ObjectId> to track whether we have pickled the object to avoid cycles
     *  references to objects with @MongoCollection annot always convert to ObjectId
     */
    protected Map<Object, ObjectId> pickled = new HashMap<Object, ObjectId>();
    /** track objects as we load from mongo; don't reload, reuse if we can */
    protected Map<ObjectId, Object> depickled = new HashMap<ObjectId, Object>();
 
    protected DB db;
 
    public MongoORM(DB db) { this.db = db; }
    

    /**
     * @param clz
     * @return
     * 
     * Returns unique collection name for type clz.
     */
    static String getCollName(Class<?> clz) {
//    	assert(clz.isAnnotationPresent(MongoCollection.class));
    	//
    	MongoCollection annot = clz.getAnnotation(MongoCollection.class);
//    	assert(annot != null);
    	if(annot == null || annot.value() == null || annot.value().length() <= 0) {
    		return clz.getName();
    	}
    	return annot.value();
    }
    

    static String getFldName(Field fld) {
    	assert(fld.isAnnotationPresent(MongoField.class));
    	//
    	MongoField annot = fld.getAnnotation(MongoField.class);
    	assert(annot != null);
    	if(annot.value() == null || annot.value().length() <= 0) {
    		return fld.getName();
    	}
    	return annot.value();
    }

    
    /**
     * @param clz
     * @return
     * 
     * Enumerate all annotated fields in a class,
     * recursively for all superclasses.
     */
    static List<Field> getAllAnnotFields(Class<?> clz) {
    	List<Field> fldz = new ArrayList<Field>();
    	internalGetAllAnnotFields(fldz, clz);
    	return fldz;
    }

    
    /**
     * recursive!
     */
    private static void internalGetAllAnnotFields(List<Field> fldz, Class<?> clz) {
		for (Field field: clz.getDeclaredFields()) {
			if (field.isAnnotationPresent(MongoField.class)) {
				fldz.add(field);
			}
		}
		// recursive
		if (clz.getSuperclass() != null) {
			internalGetAllAnnotFields(fldz, clz.getSuperclass());
		}
    }
    
    
    /**
     * @param fld
     * @param obj
     * @return
     * 
     * non-null reference field to either a generic type, or any annotated type
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     */
    static boolean isRefField(Field fld, Object obj) throws IllegalArgumentException, IllegalAccessException {
        if (fld.getGenericType() instanceof ParameterizedType) {
        	return true;
        }
        if(! fld.getType().isAnnotationPresent(MongoCollection.class)) {
        	return false;
        }
        //
        return true;
    }


    static boolean isPrimField(Field fld, Object obj) throws IllegalArgumentException, IllegalAccessException {
		if (! fld.isAnnotationPresent(MongoField.class) ||
    		fld.getGenericType() instanceof ParameterizedType ||
            fld.getType().isAnnotationPresent(MongoCollection.class)) {
    		return false;
    	}
        //
        return true;
    }

    
    /**
     * @param obj
     * @throws Exception
     * 
     * Save object graph starting from obj
     */
    public void save(Object obj) throws Exception {
    	// allow incremental saves (updates)
    	// pickled.clear();

    	saveInternal(obj);    	
    }
    
    
    /**
     * @param obj
     * @throws Exception
     * 
     * Save object subgraph from obj
     */
    void saveInternal(Object obj) throws Exception {
    	savePass1(obj);    	
    	savePass2();
    }

    
    /**
     * @param obj
     * @throws Exception
     * Traverse the object graph from obj, create/save DB object shells
     * (missing references to other objects)
     */
    void savePass1(Object obj) throws Exception {
//        LOG.info("save obj: " + obj);
        // avoid cycles
        if (pickled.containsKey(obj)) {
    		LOG.info("skip: " + obj);
    		return;
        }
        
    	// handle primitive fields
    	ObjectId oid = savePrim(obj);

    	// mark visited
    	pickled.put(obj, oid);
    	
        // handle reference fields
        
        Class<?> clz = obj.getClass();
        for (Field fld: getAllAnnotFields(clz)) {
            // skip?
            if (! isRefField(fld, obj) ||
            	fld.get(obj) == null) {
        		continue;
        	}
	        	
            LOG.info("save ref. field: " + fld.getName());
        	// save, recursively
        	savePass1(fld.get(obj));
        }
    
        // handle collection items
        
    	if (obj instanceof Collection<?>) {
    		// items
    		Collection<?> collection = (Collection<?>) obj;
    		for (Object i : collection) {
//	            LOG.info("item: " + i);
	            // only save annotated types
	            if(! i.getClass().isAnnotationPresent(MongoCollection.class)) {
	            	continue;
	            }
            	// save, recursively
	            LOG.info("save coll. item: " + i);
            	savePass1(i);
    		}
    		
    	} else if (obj instanceof Map<?,?>) {
    	}
	}


    /**
     * @param obj
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException
     * 
     * Save object to DB and return ObjectId
     * handles only basic (value) fields
     * does not save any references (to other objects)
     */
    ObjectId savePrim(Object obj) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clz = obj.getClass();
        
        // open db collection by class name
        DBCollection coll = db.getCollection(getCollName(clz));
        
        // push into db
        BasicDBObject doc = new BasicDBObject();
        
        for (Field fld: getAllAnnotFields(clz)) {
        	// skip?
        	if (! isPrimField(fld, obj) ||
            	fld.get(obj) == null) {
        		continue;
        	}
            
            // save annotated basic field
            LOG.info("save primitive field: " + fld.getName());
            doc.put(getFldName(fld), fld.get(obj));
        }
        // save
        coll.insert(doc);
        ObjectId oid = (ObjectId) doc.get(FLD_ID);
        return oid;
    }

    
    /**
     * @param obj
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws NoSuchMethodException 
     * @throws IllegalArgumentException 
     * @throws SecurityException 
     * @throws ClassNotFoundException 
     * @throws Exception
     * 
     * Fill out the reference fields with DB object ids
     */
    void savePass2() throws SecurityException, IllegalArgumentException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {        
        for (Object obj : pickled.keySet()) {
        	updateSave(obj);
        }
    }

    
    /**
     * @param obj
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws NoSuchMethodException 
     * @throws IllegalArgumentException 
     * @throws SecurityException 
     * 
     * update object reference fields to DB ids
     * @throws ClassNotFoundException 
     */
    void updateSave(Object obj) throws SecurityException, IllegalArgumentException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
    	Class<?> clz = obj.getClass();
    	ObjectId oid = pickled.get(obj);

        // reload
        DBCollection coll = db.getCollection(getCollName(clz));
        DBObject doc = (DBObject) loadPrim(oid, clz).dbobj;
		boolean flagDirty = false;

    	// handle reference fields
		
        for (Field fld: getAllAnnotFields(clz)) {
        	// skip?
        	if (! isRefField(fld, obj) ||
            	fld.get(obj) == null) {
        		continue;
        	}

            LOG.info("save-update ref. field: " + fld.getName());
        	Object flo = fld.get(obj);
            // update object reference -> ObjectId
            doc.put(fld.getName(), pickled.get(flo));
            // update type
            doc.put(fld.getName() + FLD_TYPE, flo.getClass().getName());
            flagDirty = true;
        }

        // handle collection items

    	if (obj instanceof Collection<?>) {
    		// items
    		Collection<?> collection = (Collection<?>) obj;
    		// support polymorphism in containers
    		List<ObjectId> oidList = new ArrayList<ObjectId>(); // object ids
    		List<String> ocnList = new ArrayList<String>(); // class names
    		for (Object i : collection) {
	            LOG.info("item: " + i);
	            // only save annotated types
	            if(! i.getClass().isAnnotationPresent(MongoCollection.class)) {
	            	continue;
	            }
	            // save item (id, type)
	            LOG.info("save-update coll. item: " + i);
	            oidList.add(pickled.get(i));
	            ocnList.add(i.getClass().getName());
    		}
    		doc.put(FLD_OID, oidList);
    		doc.put(FLD_OCN, ocnList);
    		flagDirty = true;
    		
    	} else if (obj instanceof Map<?,?>) {
    	}

    	// check
        if (! flagDirty) {
        	return;
        }
        // update DB
        coll.save(doc);
    }

    
    /**
     * @param oid
     * @param clz
     * @return
     * @throws NoSuchMethodException 
     * @throws SecurityException 
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws IllegalArgumentException
     * 
     * Load object from DB,
     * ignoring references to other objects (null).
     * @throws ClassNotFoundException 
     */
    Serialized loadPrim(ObjectId oid, Class<?> clz) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        // open db collection by class name
        DBCollection coll = db.getCollection(getCollName(clz));

        // read doc from DB
        DBObject query = new BasicDBObject(FLD_ID, oid);
        DBObject doc = coll.findOne(query);

        // create new object
        Object obj = clz.newInstance();
        Serialized res = new Serialized(obj, doc);
        
        // handle primitive fields

        for (Field fld: getAllAnnotFields(clz)) {
        	// skip?
        	if (! isPrimField(fld, obj)) {
        		continue;
        	}
            // load primitive field
            else  {
                LOG.info("load primitive field: " + fld.getName());
                fld.set(obj, doc.get(getFldName(fld)));
        	}        
        }
        
        return res;
    }

    
    /**
     * @param clz
     * @return
     * @throws Exception
     * 
     * Load all saved objects of a type.
     */
    public <T> List<T> loadAll(Class<T> clz) throws Exception {
    	// allow incremental loads
    	// depickled.clear();
    	
    	List<T> loadedList = new ArrayList<T>();
        DBCollection coll = db.getCollection(getCollName(clz));

        // enumerate all object ids
        DBObject query = new BasicDBObject();
        query.put("", "");
        query.put(FLD_ID, 1);
        
        DBCursor cur = coll.find();
        while(cur.hasNext()) {
        	ObjectId oid = (ObjectId) cur.next().get(FLD_ID);
        	loadInternal(oid, clz);
        	loadedList.add((T) depickled.get(oid));
        }
        
        return loadedList;
    }


    /**
     * @param oid
     * @param clz
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws ClassNotFoundException 
     * 
     * 
     */
    void loadInternal(ObjectId oid, Class<?> clz) throws SecurityException, IllegalArgumentException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException {
    	loadPass1(oid, clz);
    	loadPass2();
    }

    
    /**
     * @param oid
     * @param clz
     * @return
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     * @throws InvocationTargetException 
     * @throws NoSuchMethodException 
     * @throws IllegalArgumentException 
     * @throws SecurityException
     * 
     * Load the object graph from oid
     * @throws ClassNotFoundException 
     */
    void loadPass1(ObjectId oid, Class<?> clz) throws InstantiationException, IllegalAccessException, SecurityException, IllegalArgumentException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException {
        // skip?
        if (oid == null ||
        	depickled.containsKey(oid)) {
    		LOG.info("skip: " + oid);
    		return;
        }

        // handle primitive fields

    	Serialized ser = loadPrim(oid, clz); 
    	Object obj = ser.object;
    	DBObject doc = ser.dbobj;

    	// mark
    	depickled.put(oid, obj);    	

    	// handle reference fields

        for (Field fld: getAllAnnotFields(clz)) {
        	// skip?
        	if (! isRefField(fld, obj) ||
            	fld.get(obj) != null) {
        		continue;
        	}

        	LOG.info("load ref. field: " + fld.getName());
            // load field type
            String className = (String) doc.get(fld.getName() + FLD_TYPE);
            // skip - missing
            if(className == null) {
            	continue;
            }
            Class<?> ftp = Class.forName(className);
            // load field, recursively
            ObjectId newoid = (ObjectId) doc.get(fld.getName());
        	// load, recursively
        	loadPass1(newoid, ftp);
        }
        
        // handle collection items
        
    	if (obj instanceof Collection<?>) {
    		// items
    		ArrayList<ObjectId> oidList = (ArrayList<ObjectId>) doc.get(FLD_OID);
    		ArrayList<String> ocnList = (ArrayList<String>) doc.get(FLD_OCN);
    		for(int i = 0; i < oidList.size(); i++) {
                LOG.info("load coll. item: " + i);
    			loadPass1(oidList.get(i), Class.forName(ocnList.get(i)));
    		}
    		
    	} else if (obj instanceof Map<?,?>) {
    		// items
    	}
    }


    /**
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * 
     * Update reference fields in loaded objects
     * @throws ClassNotFoundException 
     */
    void loadPass2() throws SecurityException, IllegalArgumentException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        for (ObjectId oid : depickled.keySet()) {
        	updateLoad(oid);
        }
    }
    
    
    /**
     * @param oid
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * 
     * Update reference fields in loaded object
     * @throws ClassNotFoundException 
     */
    void updateLoad(ObjectId oid) throws SecurityException, IllegalArgumentException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
    	Object obj = depickled.get(oid);
    	Class<?> clz = obj.getClass();
 
        //  reload
    	DBObject doc = (DBObject) loadPrim(oid, clz).dbobj;

    	// handle reference fields
    	
        for (Field fld: getAllAnnotFields(clz)) {
            // skip?
        	if (! isRefField(fld, obj) ||
            	fld.get(obj) != null) {
        		continue;
        	}
            LOG.info("update-load ref. field: " + fld.getName());
            // update object reference
            ObjectId new_oid = (ObjectId) doc.get(fld.getName());
            // skip - missing
            if (new_oid == null) {
            	continue;
            }
        	fld.set(obj, depickled.get(new_oid));
        }
        
        // handle collection items
        
    	if (obj instanceof Collection<?>) {
    		Collection<Object> collection = (Collection<Object>) obj;
    		// avoid duplicates (on shared references)
    		if (collection.isEmpty()) {
	    		// items
	    		ArrayList<ObjectId> oidList = (ArrayList<ObjectId>) doc.get(FLD_OID);
	    		for(ObjectId i : oidList) {
	                LOG.info("update-load coll. item: " + i);
	    			collection.add(depickled.get(i));
	    		}
    		}
    		
    	} else if (obj instanceof Map<?,?>) {
    		// items
    	}

    }
}
