
public class TestORM {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		String dbName = "TestORM";
		TestSave.run(dbName);
		TestLoad.run(dbName);
	}
}
