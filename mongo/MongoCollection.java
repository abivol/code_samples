import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * @author abivol
 *
 * Annotations are just markers.
 */


@Retention(RetentionPolicy.RUNTIME)
public @interface MongoCollection {
	String value() default "";
}
