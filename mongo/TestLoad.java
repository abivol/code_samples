import java.text.SimpleDateFormat;
import java.util.List;

import com.mongodb.DB;
import com.mongodb.Mongo;


public class TestLoad {
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void run(String dbName) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
		
		// open mongo
		Mongo mo = new Mongo();
		DB db = mo.getDB(dbName);
		MongoORM morm = new MongoORM(db);

		// load object subgraphs (which should end up connected)
		List<Manager> mgrz = morm.loadAll(Manager.class);
		assert mgrz.size() == 2;
		
		// verify
		
		// mgr1
		Manager mgr1 = mgrz.get(0);
		assert mgr1.name.equals("mgr1");
		assert mgr1.yearlySalary == 5432.10;
		assert mgr1.ignoredField == 0;
		assert mgr1.parkingSpot == 666;
		assert mgr1.ignoredField == 0;
		assert mgr1.ignoredObj == null;
		assert mgr1.ignoredProjs == null;
		assert mgr1.projects.size() == 2;
		// projects
		Project proj1 = null, proj2 = null;
		Classified classif1 = null;
		for (Object p : mgr1.projects) {
			if(p instanceof Classified) {
				classif1 = (Classified) p;
				continue;
			}
			if(p instanceof Project) {
				proj1 = (Project) p;
				continue;
			}
		}
		assert proj1.name.equals("proj1");
		assert proj1.begin.equals(formatter.parse("01/01/11"));
		assert proj1.end == null;
		assert classif1.name == null;
		assert classif1.begin == null;
		assert classif1.end.equals(formatter.parse("03/03/13"));

		// employees
		Employee emp1 = null, emp2 = null;
		assert mgr1.directReports.size() == 3;
		assert mgr1.directReports.get(0) == mgr1;
		emp1 = mgr1.directReports.get(1);
		emp2 = mgr1.directReports.get(2);
		//
		assert emp1.name.equals("emp1");
		assert emp1.yearlySalary == 1234.56;
		assert emp1.manager == mgr1;
		assert mgr1.manager == emp1;
		assert emp1.ignoredField == 0;
		assert emp1.projects == null;
		//
		assert emp2.name.equals("emp2");
		assert emp2.yearlySalary == 0.0;
		assert emp2.manager == emp2;
		assert emp2.ignoredField == 0;
		assert emp2.projects == mgr1.projects;

		// mgr2
		Manager mgr2 = mgrz.get(1);
		assert mgr2.name.equals("mgr2");
		assert mgr2.yearlySalary == 6789.01;
		assert mgr2.manager == mgr1;
		assert mgr2.ignoredObj == null;
		assert mgr2.parkingSpot == 999;
		assert mgr2.projects.size() == 2;
		// projects
		assert mgr2.projects.contains(proj1);
		for (Object p : mgr2.projects) {
			if(p == proj1) {
				continue;
			} else {
				proj2 = (Project) p;
				continue;
			}
		}
		assert proj2.name.equals("proj2");
		assert proj2.begin.equals(proj2.end);
		assert proj2.end.equals(formatter.parse("02/02/12"));
		// reports
		assert mgr2.directReports == mgr1.directReports;
	}
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		TestLoad.run(args[0]);
	}
}
