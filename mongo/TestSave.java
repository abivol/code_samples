import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class TestSave {
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void run(String dbName) throws Exception {
		// create test objects
		
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
		
		Project proj1 = new Project();
		proj1.name = "proj1";
		proj1.begin = formatter.parse("01/01/11");
		// proj1.end = uninitialized

		Project proj2 = new Project();
		proj2.name = "proj2";
		proj2.begin = formatter.parse("02/02/12");
		proj2.end = proj2.begin;

		Classified classif1 = new Classified();
		// classif1.name = uninitialized
		// classif1.begin = uninitialized
		classif1.end = formatter.parse("03/03/13");

		Employee emp1 = new Employee();
		emp1.name = "emp1";
		emp1.yearlySalary = 1234.56;
		// emp1.manager = mgr1
		// emp1.projects = uninitialized
		emp1.ignoredField = 111;
		 
		Employee emp2 = new Employee();
		emp2.name = "emp2";
		// emp2.yearlySalary = uninitialized
		emp2.manager = emp2; // cycle
		emp2.projects = new HashSet<Project>();
		emp2.projects.add(proj1);
		emp2.projects.add(classif1); // polymorphism (coll)
		emp2.ignoredField = 222;
		

		// another object graph
		Manager mgr1 = new Manager();
		mgr1.name = "mgr1";
		mgr1.yearlySalary = 5432.10;
		emp1.manager = mgr1; // polymorphism (member)
		mgr1.manager = emp1; // cycle
		mgr1.projects = emp2.projects; // shared
		mgr1.parkingSpot = 666;
		mgr1.ignoredField = 333;
		mgr1.ignoredObj = emp1;
		mgr1.ignoredProjs = new ArrayList<Project>();
		mgr1.ignoredProjs.add(proj2);
		mgr1.directReports = new ArrayList<Employee>();
		mgr1.directReports.add(mgr1); // cycle + polymorphism (coll)
		mgr1.directReports.add(emp1);
		mgr1.directReports.add(emp2);		

		// another object graph
		Manager mgr2 = new Manager();
		mgr2.name = "mgr2";
		mgr2.yearlySalary = 6789.01;
		mgr2.manager = mgr1; // polymorphism + shared
		mgr2.projects = new HashSet<Project>();
		mgr2.projects.add(proj1); // shared
		mgr2.projects.add(proj2);
		mgr2.ignoredObj = mgr1;
		mgr2.parkingSpot = 999;
		mgr2.directReports = mgr1.directReports; // shared
		
		// open mongo
		Mongo mo = new Mongo();
		DB db = mo.getDB(dbName);
		MongoORM morm = new MongoORM(db);

		// save multiple object subgraphs (which should end up connected in the db)
		morm.save(emp2);
		morm.save(mgr1);
		morm.save(mgr2);
	}
	

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		TestSave.run(args[0]);
	}
}
