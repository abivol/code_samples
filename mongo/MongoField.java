import java.lang.annotation.*;


/**
 * @author abivol
 *
 * Annotations are just markers.
 */


@Retention(RetentionPolicy.RUNTIME)
public @interface MongoField {
	String value() default "";
}
