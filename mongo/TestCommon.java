import java.util.Date;
import java.util.List;
import java.util.Set;

@MongoCollection("empl")
class Employee {
	@MongoField String name;
	@MongoField("salary") double yearlySalary; // must be double not float
	@MongoField Employee manager; // must avoid cyclic pickling
	@MongoField Set<Project> projects; // must avoid cyclic pickling
	int ignoredField;
}
 
@MongoCollection
class Project {
    @MongoField String name;
    @MongoField Date begin;
    @MongoField Date end;
}

@MongoCollection("mgr")
class Manager extends Employee {
    @MongoField int parkingSpot;
    @MongoField List<Employee> directReports; // must avoid cyclic pickling
	Object ignoredObj;
	List<Project> ignoredProjs;
}

@MongoCollection
class Classified  extends Project {
	String ignore;
}
