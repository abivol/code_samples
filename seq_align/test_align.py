#!/usr/bin/env python2.7
'''

@author: Adrian Bivol
'''

import sys, argparse
from align import local_aligner, global_aligner
from FastaSeq import FastaSeq


def parse_args(args):
    """Parse command line arguments.
    Return list of options.
    """
    # cmd line parser
    parser = argparse.ArgumentParser(description=globals()['__doc__'])
    parser.add_argument("-s", "--subst_matrix", help="subst_matrix", action="store", \
        dest="subst", required=False, default="BLOSUM62")
    parser.add_argument("-a", "--align", help="align", action="store", \
        dest="align", required=False, default="local")
    parser.add_argument("-o", "--open", help="open", action="store", \
        dest="open", required=False, default=12)
    parser.add_argument("-e", "--extend", help="extend", action="store", \
        dest="extend", required=False, default=1)
    parser.add_argument("-d", "--double_gap", help="double_gap", action="store", \
        dest="double", required=False, default=2)
    parser.add_argument("-p", "--pretty", help="pretty", action="store_const", \
        dest="pretty", required=False, default=False, const=True)
    return parser.parse_args()


def main(args):
    """
    Read params, dispatch commands
    """
    # opt contains the params & values
    opt = parse_args(args)
#    print opt
    aligner = None
    gaps = {"open":opt.open, "extend":opt.extend, "double":opt.double}
    if opt.align.lower() == "local":
        aligner = local_aligner(opt.subst, gaps)
    else:
        aligner = global_aligner(opt.subst, gaps)
    #
    FastaSeq.setAlphabet(aligner.alphabet)
#    print FastaSeq.delChars

    rdr = FastaSeq.readFile(sys.stdin)
    # master seq
    s = rdr.next()
    x = s.sequence
    print ">{0}\n{1}".format(s.identifier, x)
    # slaves
    for s in rdr:
        y = s.sequence
        score = aligner.align(x, y)
        print ">{0}".format(s.identifier)
        a2m = aligner.traceback_yseq()
        if opt.pretty:
            (s1, s2) = aligner.convertA2M(x, a2m)
            print "{0}\n{1}".format(s1, s2)
        else:
            print a2m
        sys.stderr.write("{0}\n".format(score))



if __name__ == "__main__" :
    try:
        sys.exit(main(sys.argv))
    except EnvironmentError as (errno,strerr):
        sys.stderr.write("ERROR: " + strerr + "\n")
        sys.exit(errno)

