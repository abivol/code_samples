'''
Manipulate Fasta sequences: read/write from/to file.
'''

import sys, re
from BioSeq import BioSeq


class FastaSeq(BioSeq):
    '''
    holds a FASTA sequence
    read/write seq. from/to file
    '''
    
    charHeader = ">"
    charComment = ";"
    lineSize = 80
    delChars = None


    @staticmethod
    def setAlphabet(alphabet):
        codes = [ord(c) for c in alphabet]
        FastaSeq.delChars = "".join(chr(c) for c in frozenset(range(0, 256)).difference(codes))


    @staticmethod
    def isComment(s):
        return s[0] == FastaSeq.charComment


    @staticmethod
    def isHeader(s):
        return s[0] == FastaSeq.charHeader


    def __init__(self, identifier="", description="", \
        sequence="", quality=[]):
        '''
        Constructor
        '''
        # init
        super(FastaSeq, self).__init__(identifier, description, \
            sequence, quality)


    def __repr__(self):
        vd = vars(self)
        return ", ".join(str(vd[x]) for x in vd)


    @staticmethod
    def readFile(fd, quality=False):
        '''
        Read in a FASTA file.
        This function reads either a .fasta or a .qual file, and yields FastaSeq
        objects with either sequence or quality data filled in. This was done
        because these file formats are almost the same, so they share most of
        the parsing code.
        '''
        # parser state
        state = 0 # start
        lnCounter = 0 # line count - to report error location
        identifier = "" # of sequence
        description = "" # of sequence
        seqLst = [] # seq strings
        quaLst = [] # qual strings
        #
        for ln in fd:
            lnCounter += 1
            ln = ln.strip() # strip whitespace from line ??
            # ln = ln.rstrip("\n")
            if ( len(ln.strip()) <= 0 or  # skip empty lines
                (state < 2 and FastaSeq.isComment(ln)) ): # skip header comments
                continue
            # read the header
            if FastaSeq.isHeader(ln):
                # yield the previous sequence (if any)
                if state >= 1:
                    yield FastaSeq(identifier, description, \
                        "".join(seqLst), quaLst)
                #
                state = 1 # header
                # initialize
                identifier = ""
                description = ""
                seqLst = []
                quaLst = []
                # parse header
                tok1 = ln[1:].split(None, 1) # whitespace split
                identifier = tok1[0]
                if len(tok1) > 1:
                    description = tok1[1]
                continue
            # read the sequence
            if state >= 1:
                # done
                state = 2 # sequence
                if quality:
                    quaLst.extend(int(x) for x in ln.split())
                else: # sequence
                    # uppercase
                    ln = ln.upper()
                    # remove foreign chars
                    ln = ln.translate(None, FastaSeq.delChars)
                    seqLst.append(ln)
                continue

        # final sequence
        if state >= 1:
            yield FastaSeq(identifier, description, \
                "".join(seqLst), quaLst)
        pass


    def writeFile(self, fd, quality=False):
        """
        Write sequence to (opened) file fd.
        """
        fd.write(">{0} {1}\n".format(self.identifier, self.description))
        seq = self.sequence # which list to write
        step = FastaSeq.lineSize
        if quality:
            seq = self.quality
            step /= 3

        for i in range(0, len(seq), step):
            if quality:
                s = " ".join(str(x) for x in seq[i:(i+step)])
                fd.write("{0}\n".format(s))
            else: # sequence
                fd.write("{0}\n".format(seq[i:(i+step)]))
        # empty
        if len(seq) <= 0:
            fd.write("\n")
        
        pass


def read_fasta(fd):
    """
    Read sequences from (opened) file fd.
    """
    for s in FastaSeq.readFile(fd):
        yield s


def read_fasta_with_quality(fd_fa, fd_qu):
    """
    Read sequences from (opened) files (fd).
    """
    c = 0 # seq. counter
    for p in zip(FastaSeq.readFile(fd_fa), \
        FastaSeq.readFile(fd_qu, quality=True)):
        c += 1
        xs = p[0] # sequence
        xq = p[1] # quality
        if len(xs.sequence) != len(xq.quality):
            sys.stderr.write("ERROR: seq #{0}: sequence and quality lengths are not equal!\n".format(c))
            continue
        #
        xs.quality = xq.quality # patch in quality
        yield xs


################################################################################
# unit tests
################################################################################


def test_coding_cost_1(fn_in, fn_out, quality=False):
    with open(fn_in, "rt") as f_in:
        with open(fn_out, "wt") as f_out:
            for s in FastaSeq.readFile(f_in, quality):
                s.writeFile(f_out, quality)
                print s


def test_3():
    with open("test_fasta_3.txt", "wt") as f_seq:
        with open("test_qual_3.txt", "wt") as f_qual:
            for s in read_fasta_with_quality("tiny.fasta", "tiny.qual"):
                s.writeFile(f_seq)
                s.writeFile(f_qual, quality=True)
                print s


def test_4():
    with open("test_fasta_4.txt", "wt") as f:
        for s in read_fasta("crappy.fasta"):
            s.writeFile(f)
            print s


# tests
if __name__ == "__main__":
    #test_coding_cost_1("tiny.fasta", "test_1_fasta.txt")
    test_coding_cost_1("tiny.qual", "test_1_qual.txt", quality=True)
    pass
