'''

@author: Adrian Bivol
'''
from itertools import izip


class abstract_aligner(object):
    '''
    implements most alignment logic
    '''

    def __init__(self, subst, gaps):
        '''
        Constructor
        records the substitution matrix and gap costs for the alignments to follow. I found it useful to store both as dict objects, with subst mapping strings of 2 letters to the substitution matrix value for that pair, and gaps mapping "open", "extend", and "double" to the appropriate values. 
        '''
        # read substitution matrix from file
        with open(subst, "r") as f_in:
            # skip header
            for ln in f_in:
                ln = ln.strip()
                if len(ln) <= 0 or ln[0] == "#":
                    continue
                break
            # read header
            symbols = ln.split()
            subMatrix = {}
            for ln in f_in:
                ln = ln.strip()
                ln = ln.upper()
                toks = ln.split()
                assert len(toks) == len(symbols) + 1
                assert toks[0] in symbols
                for i, t in enumerate(toks[1:]):
                    subMatrix[(toks[0], symbols[i])] = int(t)
#                    print "subMatrix[{0},{1}]: {2}".format(toks[0], symbols[i], int(t))
        #
        self.alphabet = list(frozenset(symbols))
        self.subMatrix = subMatrix
        self.gaps = gaps
        # smallest int value
        self.minint = -1000


    def printMatrix(self, mx):
        """Debug helper
        """
        print ",,{0}".format(",".join(self.yseq))
        for r in range(0, self.m+1):
            ln = ",".join(str(mx[(r, c)]) \
                for c in range(0, self.n+1))
            print "{0},{1}".format(self.xseq[r-1] if r > 0 else "", ln)


    def findMax(self, iterable):
        """Find max value and its position in iterable
        Duplicates: don't care.
        """
        sz = len(iterable)
        md = dict(zip(iterable, range(0, sz)))
        mv = max(iterable)
        ix = md[mv]
        return (ix, mv)


    def mkTup_M(self, ix):
        """Make tuple for Match max formula
        Items in tuple are always in order: M, IA, IB, [minScore]
        """
        return (self.M[ix], self.IA[ix], self.IB[ix], self.minScore)


    def mkTup_IA(self, ix):
        """Make tuple for IA max formula
        Items in tuple are always in order: M, IA, IB, [minScore]
        """
        return (self.M[ix] - self.gaps["open"], \
            self.IA[ix] - self.gaps["extend"],
            self.IB[ix] - self.gaps["double"] \
            )


    def mkTup_IB(self, ix):
        """Make tuple for IB max formula
        Items in tuple are always in order: M, IA, IB, [minScore]
        """
        return (self.M[ix] - self.gaps["open"], \
            self.IA[ix] - self.gaps["double"],
            self.IB[ix] - self.gaps["extend"] \
            )


    def traceback(self, r, c, mt, local=False):
        """Trace alignment back from matrices.
        r,c : seq. positions
        mt : matrix type; position in 3-tuple (see mkTup_*)
        """
        A = self.xseq
        B = self.yseq
        # aligns (backwards)
        X = list()
        Y = list()
        # remember begin & end trace positions
        stop_r = r
        stop_c = c
        while r > 0 and c > 0:
            if mt == 0:
                # align A & B
                # string indices start from 0 !
                X.append(A[r-1])
                Y.append(B[c-1])
                # next
                r -= 1
                c -= 1
                # predecessor of M
                fm = self.findMax(self.mkTup_M((r, c)))
            elif mt == 1:
                # gap
                X.append(A[r-1].lower())
                Y.append("-")
                # next
                r -= 1
                # predecessor of IA
                fm = self.findMax(self.mkTup_IA((r, c)))
            elif mt == 2:
                # gap
                X.append("-")
                Y.append(B[c-1].lower())
                # next
                c -= 1
                # predecessor of IB
                fm = self.findMax(self.mkTup_IB((r, c)))
            else:
                # minScore special
                assert local
                break
            # next matrix type
            mt = fm[0]
        #
        start_c = c
        start_r = r
#        if local:
        self.xalign = "".join(reversed(X))
        self.yalign = "".join(reversed(Y))
        # fill in seq 2
        prefix = "-" * (start_r) + self.yseq[:start_c].lower()
        suffix = self.yseq[stop_c:].lower() + "-" * (self.m - stop_r)
        self.yalign = prefix + self.yalign + suffix
        pass


    def align(self, xseq, yseq, local=False):
        '''Creates and fills in alignment matrices and stores xseq and yseq. 
        '''
        self.xseq = A = xseq
        self.yseq = B = yseq
        #
        self.m = m = len(A)
        self.n = n = len(B)
        # allocate matrices: each dim +1 larger than sequence
        self.M = dict()
        self.IA = dict()
        self.IB = dict()
        # fill in the base cases!
        for r in range(0, m+1):
            self.M[(r,0)] = self.minint
            self.IA[(r,0)] = self.minint
            self.IB[(r,0)] = self.minint
        for c in range(0, n+1):
            self.M[(0,c)] = self.minint
            self.IA[(0,c)] = self.minint
            self.IB[(0,c)] = self.minint
        if not local:
            self.M[(0,0)] = 0
            for r in range(1, m+1):
                self.IA[(r,0)] = 0 - self.gaps["open"] - \
                    (r-1)*self.gaps["extend"]
            for c in range(1, n+1):
                self.IB[(0,c)] = 0 - self.gaps["open"] - \
                    (c-1)*self.gaps["extend"]
        #
        self.minScore = 0 if local else self.minint
        # fill in matrices bottom-up
        for r in range(1, m+1):
            for c in range(1, n+1):
                ixOut = (r, c) # write index
                # M
                ixIn = (r-1, c-1) # read index
                # string indices are 0-based!
                self.M[ixOut] = self.subMatrix[(A[r-1], B[c-1])] + \
                    max(self.mkTup_M(ixIn))
                # IA
                ixIn = (r-1, c)
                self.IA[ixOut] = max(self.mkTup_IA(ixIn))
                # IB
                ixIn = (r, c-1)
                self.IB[ixOut] = max(self.mkTup_IB(ixIn))
                #
#                print "ix: {0}, M: {1}, IA: {2}, IB: {3}".format( \
#                    ixOut, M[ixOut], IA[ixOut], IB[ixOut])
        #
#        print "M:"
#        self.printMatrix(M)
#        print "IA:"
#        self.printMatrix(IA)
#        print "IB:"
#        self.printMatrix(IB)
        #
        self.maxScore = None
        if local:
            self.maxScore = max(0, max(self.M.values()))
        else:
            x = (r, c)
            self.maxScore = max(self.M[x], self.IA[x], self.IB[x])
        #
        return self.maxScore


    def convertA2M(self, s1, s2):
        """"convert A2M align pair to 2-sequence alignment pair
        """
        t1 = list()
        # seq 2 never changes (in length)
        i = 0 # index seq 1
        j = 0 # index seq 2
        while j < len(s2):
            if s2[j].islower():
                # gap in seq 1
                t1.append("-")
                j += 1
                continue
            # no gap in seq 1
            if i < len(s1):
                t1.append(s1[i])
            else:
                t1.append("-")
            i += 1
            j += 1
        #
        t1 = "".join(t1)
        t2 = s2.upper()
        #print t1
        #print t2
        return (t1, t2)


    def score_a2m(self, s1, s2, local=False):
        '''
        Given two strings that represent an alignment in A2M format, score the 
        alignment with the parameters used when the aligner was created. 
        '''
        # convert A2M to aligned pair
        (t1, t2) = self.convertA2M(s1, s2)
        assert len(t1) == len(t2)
        # for local alignment, skip prefix & suffix gaps
        if local:
            start = None
            stop = None
            # find first match
            i = 0
            for (a, b) in izip(t1, t2):
                if a != "-" and b != "-":
                    start = i
                    break
                #
                i += 1
            # find last match
            i = len(t1)
            for (a, b) in izip(reversed(t1), reversed(t2)):
                if a != "-" and b != "-":
                    stop = i
                    break
                #
                i -= 1
            # cut the seqs
            t1 = t1[start:stop]
#            print t1
            t2 = t2[start:stop]
#            print t2
        # now walk the alignment and score it
        gapSeq = None
        score = 0
        for (a, b) in izip(t1, t2):
            assert a != "-" or b != "-"
            if a == "-" or b == "-":
                # gap!
                gs = 1 if a == "-" else 2
                if gs == gapSeq:
                    # extend gap
                    score -= self.gaps["extend"]
                elif gapSeq is None:
                    # open gap
                    score -= self.gaps["open"]
                else:
                    # double gap
                    score -= self.gaps["double"]
                # gapCnt
                gapSeq = gs
                continue
            # match
            score += self.subMatrix[(a, b)]
            gapSeq = None
        return score
        pass


###############################################################################


class local_aligner(abstract_aligner):
    '''
    implements local alignment
    '''

    def __init__(self, subst, gaps):
        '''
        Constructor
        '''
        super(local_aligner, self).__init__(subst, gaps)


    def align(self, xseq, yseq):
        '''
        Creates and fills in alignment matrices and stores xseq and yseq 
        '''
        return super(local_aligner, self).align(xseq, yseq, local=True)


    def traceback_yseq(self):
        """Find starting point for local alignment and start traceback
        """
        if self.maxScore <= 0:
            # nothing aligned
            (r, c) = (0, 0)
        else:
            it = (k for k in self.M if self.M[k] == self.maxScore)
            (r, c) = it.next()
        self.traceback(r, c, 0, True)
#        print self.yalign
        return self.yalign


###############################################################################


class global_aligner(abstract_aligner):
    '''
    implements global alignment
    '''

    def __init__(self, subst, gaps):
        '''
        Constructor
        '''
        super(global_aligner, self).__init__(subst, gaps)


    def align(self, xseq, yseq):
        '''
        Creates and fills in alignment matrices and stores xseq and yseq. 
        '''
        return super(global_aligner, self).align(xseq, yseq, local=False)


    def traceback_yseq(self):
        """Find starting point for global alignment and start traceback
        """
        (r, c) = (self.m, self.n)
        x = (r, c)
        (mt, mv) = self.findMax((self.M[x], self.IA[x], self.IB[x]))
        self.traceback(r, c, mt, False)
#        print self.yalign
        return self.yalign


###############################################################################


testA = "ISAYGARFIELDISAFATCAT"
testB = ["GARFIELDwasthelASTFATCAT".upper(), \
    "GARFIELDISAFATCAT".upper(), \
    "GARFIELDISTheFATCAT".upper(), \
    "GARFIELDISAFAsTCAT".upper(),
    "GARFIELDISTHefaSTCAT".upper(), \
    "GARFIELDISFAT".upper(), \
    "SAYFAt".upper(), \
    "ZXWP", \
    "P" \
    ]
testG = ["----GARFIELDwasthelASTFATCAT", \
    "----GARFIELDISAFATCAT", \
    "----GARFIELDISTheFATCAT", \
    "----GARFIELDISAFAsTCAT", \
    "----GARFIELDISTHefaSTCAT", \
    "----GARFIELDIS----FAT", \
    "-SAY--------------FAT", \
    "ZX------------------wP", \
    "--------------------P", \
    ]
testL = ["----GARFIELDwasthelASTFATCAT", \
    "----GARFIELDISAFATCAT", \
    "----GARFIELDISTheFATCAT", \
    "----GARFIELDISAFAsTCAT", \
    "----GARFIELDISTHefaSTCAT", \
    "----GARFIELDIS-FAT---", \
    "-SAYFAt---------------", \
    "---------Zxwp-----------", \
    "p---------------------", \
    ]


def test_1():
    aligner = local_aligner("BLOSUM62", {"open":12, "extend":1, "double":2})
    x = testA
    print "==GLOBAL=="
    for y in testG:
        t1, t2 = aligner.convertA2M(x, y)
        print "{0}\n{1}".format(t1, t2)
    print "==LOCAL=="
    for y in testL:
        t1, t2 = aligner.convertA2M(x, y)
        print "{0}\n{1}".format(t1, t2)


def test_2():
    aligner = local_aligner("BLOSUM62", {"open":12, "extend":1, "double":2})
    x = testA
    print "==GLOBAL=="
    print x
    for y in testG:
        print aligner.score_a2m(x, y, False)
    print "==LOCAL=="
    print x
    for y in testL:
        print aligner.score_a2m(x, y, True)


def test_3():
    aligner = global_aligner("BLOSUM62", {"open":12, "extend":1, "double":2})
    x = testA
    print "==GLOBAL=="
    print x
    for y in testB:
        print "{0} : {1}".format(y, aligner.align(x, y))
        print aligner.traceback_yseq()


def test_4():
    aligner = local_aligner("BLOSUM62", {"open":12, "extend":1, "double":2})
    x = testA
    print "==LOCAL=="
    print x
    for y in testB:
        print "{0} : {1}".format(y, aligner.align(x, y))
        print aligner.traceback_yseq()


###############################################################################


if __name__ == '__main__':
#    test_1()
#    test_2()
#    test_3()
#    test_4()

    pass
