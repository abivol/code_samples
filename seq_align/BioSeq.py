'''
Base sequence class.

References:
http://stackoverflow.com/questions/3694371/how-do-i-initialize-the-base-super-class-in-python
'''


class BioSeq(object):
    '''
    classdocs
    '''

    def __init__(self, identifier="", description="", \
        sequence="", quality=[]):
        '''
        Constructor
        '''
        # init
        self.identifier = identifier
        self.description = description
        self.sequence = sequence
        self.quality = quality
