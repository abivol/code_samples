import java.util.Date;
import java.util.logging.Logger;
import pkg1.TestClass1;
import pkg1.TestClass2;


class TestObj1 {
	String str1, str2;
	int int1, int2;

	TestObj1(String s1) {
		this.str1 = s1;
	}

	TestObj1() {
		this("string_1");
		this.int1 = 1;
	}
	
	TestObj1(int i2) {
		this();
		this.int2 = i2;
	}

	TestObj1(String s2, int i2) {
		this(i2);
		this.str2 = s2;
	}
}


public class TestMain {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public float fun_1() {
		float sum = 0;
		for (int i = 0; i < 2000; i++) {
			sum += i / 6;
//			LOG.info("iter: " + i);
			for (int j = 0; j < 1000; j++) {
				sum += j / 3;
			}
			//
			TestObj1 t1 = new TestObj1();
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sum;
	}

	public float fun_2() {
		float sum = 0;
		for (int i = 0; i < 1000; i++) {
			sum += i / 6;
//			LOG.info("iter: " + i);
			for (int j = 0; j < 1000; j++) {
				sum += j / 3;
			}
			//
			TestObj1 t2 = new TestObj1("string_2", 2);
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sum;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LOG.info("ProfTest main ENTER");
		TestMain tm = new TestMain();
		tm.fun_1();
		tm.fun_2();

		TestClass1 t1 = new TestClass1("string_1");
		TestClass1 t2 = new TestClass1();
		TestClass1 t3 = new TestClass1(2);
		TestClass1 t4 = new TestClass1("string_2", 2);

		TestClass2 t5 = new TestClass2("string_1");
		TestClass2 t6 = new TestClass2();
		TestClass2 t7 = new TestClass2(2);
		TestClass2 t8 = new TestClass2("string_2", 2);
		
		String s1 = new String();
		String s2 = new String("abcdefg");
		Date d1 = new Date();
		Date d2 = new Date(52453434);
		

		LOG.info("ProfTest main EXIT");
}

}
