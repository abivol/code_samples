package pkg1;

public class TestClass1 {
	String str1, str2;
	int int1, int2;

	public TestClass1(String s1) {
		this.str1 = s1;
	}

	public TestClass1() {
		this("string_1");
		this.int1 = 1;
	}
	
	public TestClass1(int i2) {
		this();
		this.int2 = i2;
	}

	public TestClass1(String s2, int i2) {
		this(i2);
		this.str2 = s2;
	}
}
