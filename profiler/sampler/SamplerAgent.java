import java.lang.instrument.Instrumentation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class MutableInt extends Number implements Comparable<Number> {
	public int v;

	public MutableInt(int v) { this.v = v; }
	@Override public int compareTo(Number o) { return v - o.intValue(); }
	@Override public int intValue() { return v; }
	@Override public long longValue() { return v; }
	@Override public float floatValue() { return v; }
	@Override public double doubleValue() { return v; }

	@Override
	public String toString() {
		return String.valueOf(v);
	}
}


/** Count how many of each key we have; not thread safe */
class FrequencySet<T> extends Hashtable<T, MutableInt> {
	public int count(T key) {
		MutableInt value = get(key);
		if (value == null) return 0;
		return value.v;
	}
	
	public void add(T key) {
		MutableInt value = get(key);
		if (value == null) {
			value = new MutableInt(1);
			put(key, value);
		}
		else {
			value.v++;
		}
	}
}


class SamplerThread implements Runnable {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	static FrequencySet<String> sampleCounter = new FrequencySet<String>();
	static final Pattern funPattern = Pattern.compile("^java\\.|^javax\\.|^sun\\.|^com\\.|^\\$");
	static final int quantum = 1;
            
    public void run() {
		boolean done = false;
		
		// find the main thread
    	Thread mainThread = null;
    	while (mainThread == null) {
    		Set<Thread> allThreads = Thread.getAllStackTraces().keySet();
    		for (Thread t : allThreads) {
    			if (t.getName().equals("main")) {
    				mainThread = t;
    				break;
    			}
    		}
    		LOG.info("main thread: " + mainThread);
    		Thread.yield();
    	}
    	
    	while(! done) {
    		try {
	    		StackTraceElement[] stack = mainThread.getStackTrace();
	    		if (stack.length < 1) {
	    			continue;
	    		}
	    		// record functions
	    		for (StackTraceElement frame: stack) {
	    			Matcher matcher = funPattern.matcher(frame.getClassName());
	    			// skip?
	    			if (matcher.find()) {
	    				continue;
	    			}
	    			// update
	    			String name = frame.getClassName() + "." + frame.getMethodName();
	    			LOG.info(String.format("frame: " + name));
    				sampleCounter.add(name);
	    			// done???
	    			break;
	    		}
    		} finally {
	    		//
	    		try {
	    			LOG.info("sleep");
					Thread.sleep(quantum);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
	    }
    }
}


class EntryComparator implements Comparator {
	public int compare(Object o1, Object o2){
		Map.Entry e1 = (Map.Entry) o1;
		Map.Entry e2 = (Map.Entry) o2;
		MutableInt v1 = (MutableInt) e1.getValue();
		MutableInt v2 = (MutableInt) e2.getValue();
		return(v2.compareTo(v1)); // descending order
	}
}


class ResultThread implements Runnable {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
            
    public void run() {
        LOG.info(String.format("table size: %d", SamplerThread.sampleCounter.keySet().size()));
        // sort
        List<Map.Entry> tableArray = new ArrayList<Map.Entry>(SamplerThread.sampleCounter.entrySet());
        Collections.sort(tableArray, new EntryComparator());
        // print results
        for (Map.Entry e : tableArray) {
        	System.out.println(e.getKey() + "\t" + e.getValue());
        }
	}
}


public class SamplerAgent {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    /**
     * @param args
     * @param inst
     * @throws Exception
     */
    public static void premain(String args, Instrumentation instr) throws Exception {
    	LOG.setLevel(Level.OFF);
        LOG.info(String.format("premain args: {}", args));
        //
        Thread result = new Thread(new ResultThread());
        Runtime.getRuntime().addShutdownHook(result);
        //
        Thread profiler = new Thread(new SamplerThread());
        profiler.setDaemon(true);
//        profiler.setPriority(Thread.MAX_PRIORITY);
        profiler.start();
    }
}
