package tracer;
import java.util.logging.Logger;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;


class CtorTransformAdapter extends MethodVisitor implements Opcodes {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    String className;
	String methodName;
    String fullName;
    int state = 0;

	public CtorTransformAdapter(MethodVisitor mv, String className,
			String name, String desc) {
		super(ASM4, mv);
		this.className = className;
		this.methodName = name;
		this.fullName = className + ":" + name + ":" + desc;
	}

    
	@Override
    public void visitTypeInsn(final int opcode, final String type) {
        switch (opcode) {
        	case NEW:
//        	case ANEWARRAY:
        		LOG.info("OPCODE NEW: " + fullName);
	    		// generate trampoline
	    		trampoline(type);
        		break;
        }

		//
        mv.visitTypeInsn(opcode, type);
	}

	@Override
	public void visitMaxs(int maxStack, int maxLocals) {
		super.visitMaxs(maxStack + 4, maxLocals);
	}
	
	//
	
    private void trampoline(String name) {
		// generate trampoline call
    	mv.visitLdcInsn(name);
    	mv.visitMethodInsn(INVOKESTATIC, "tracer/CtorSniffer", "newInstance", "(Ljava/lang/String;)V");
	}
    

//    private void testTrampoline() {
//    	CtorSniffer.newInstance("name");
//    }
}



public class CtorSniffer extends ClassVisitor {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	static FrequencySet<String> instanceCounter = new FrequencySet<String>();
    String className;
 
    public CtorSniffer(ClassVisitor cv) { super(Opcodes.ASM4, cv); }
 
    public void visit(final int version,
                      final int access,
                      final String name,
                      final String signature,
                      final String superName,
                      final String[] interfaces) {
        className = name.replaceAll("/", ".");
        super.visit(version, access, name, signature, superName, interfaces);
    }
 
    @Override
    public MethodVisitor visitMethod(final int access,
                                     final String name,
                                     final String desc,
                                     final String signature,
                                     final String[] exceptions) {
    	MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
        LOG.info("NEW class: " + className + ", name: " + name + ", desc: " + desc);

        // transform
        if(mv != null) {
        	mv = new CtorTransformAdapter(mv, className, name, desc);
        }

        return mv;
    }
    

    @Override
    public void visitEnd() {
		cv.visitEnd();
    }

    
    ///////////////////////////////////////////////////////////////////////


    public static void newInstance(String name) {
//		LOG.info("NEW instance: " + name);
		// update counter - thread safe!
		synchronized(instanceCounter) {
			instanceCounter.inc(name);
		}
    }
}
