package tracer;

import java.lang.instrument.Instrumentation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


class MutableInt extends Number implements Comparable<Number> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int v;

	public MutableInt(int v) { this.v = v; }
	@Override public int compareTo(Number o) { return v - o.intValue(); }
	@Override public int intValue() { return v; }
	@Override public long longValue() { return v; }
	@Override public float floatValue() { return v; }
	@Override public double doubleValue() { return v; }

	@Override
	public String toString() {
		return String.valueOf(v);
	}
}


/** Count how many of each key we have; not thread safe */
class FrequencySet<T> extends Hashtable<T, MutableInt> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int count(T key) {
		MutableInt value = get(key);
		if (value == null) return 0;
		return value.v;
	}

	public void inc(T key) {
		add(key, 1);
	}

	public void add(T key, int inc) {
		MutableInt value = get(key);
		if (value == null) {
			value = new MutableInt(inc);
			put(key, value);
		}
		else {
			value.v += inc;
		}
	}
}


class EntryComparator implements Comparator<Map.Entry> {
	public int compare(Map.Entry e1, Map.Entry e2){
		MutableInt v1 = (MutableInt) e1.getValue();
		MutableInt v2 = (MutableInt) e2.getValue();
		return(v2.compareTo(v1)); // descending order
	}
}


class ResultThread implements Runnable {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	static void PrintSorted(FrequencySet<String> freqSet) {
        // sort
        List<Map.Entry> tableArray = new ArrayList<Map.Entry>(freqSet.entrySet());
        Collections.sort(tableArray, new EntryComparator());
        // print results
        for (Map.Entry e : tableArray) {
        	System.out.println(e.getKey() + "\t" + e.getValue());
        }
	}

	
	@Override
    public void run() {
		// disable agent
		TracerAgent.enabled = false;
		//
		System.out.println("\n>>> Call counts:");
		PrintSorted(ProfileTracer.callCount);
		//
		System.out.println("\n>>> Call times:");
		PrintSorted(ProfileTracer.callTimes);
		//
		System.out.println("\n>>> Instance counts:");
		PrintSorted(CtorSniffer.instanceCounter);
	}
}


public class TracerAgent {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	static Instrumentation instrumentation;
	static public boolean enabled = true;;


    /**
     * @param args
     * @param inst
     * @throws Exception
     */
    public static void premain(String args, Instrumentation instr) throws Exception {
    	LOG.setLevel(Level.OFF);
        LOG.info(String.format("premain args: {}", args));
        //
        Thread result = new Thread(new ResultThread());
        Runtime.getRuntime().addShutdownHook(result);
        //
        instrumentation = instr;
        instrumentation.addTransformer(new TracerTransformer());
    }
}
