package tracer;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;


public class TracerTransformer implements ClassFileTransformer {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	static final Pattern funPattern = Pattern.compile("^tracer\\.|^java\\.|^javax\\.|^sun\\.|^com\\.|^\\$");

	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> clazz,
			ProtectionDomain protDomain, byte[] classBuffer)
			throws IllegalClassFormatException {
		LOG.info("LOAD class: " + className);
		className = className.replaceAll("/", ".");
		
		// skip all?
		if (! TracerAgent.enabled) {
			LOG.info("IGNORE class: " + className);
			return classBuffer;
		}

		// transform the class (in mem)
		LOG.info("INSTRUMENT_NEW class: " + className);

		// c-tor (NEW) instrumentation
		try {
            ClassReader cr = new ClassReader(classBuffer);
            ClassWriter cw = new ClassWriter(cr,
            	ClassWriter.COMPUTE_MAXS); //  | ClassWriter.COMPUTE_FRAMES);
            ClassVisitor cv = new CtorSniffer(cw);
            cr.accept(cv, 0);
            // done
            classBuffer = cw.toByteArray();
            // DEBUG: dump it
//            FileOutputStream fs = new FileOutputStream(
//            	new File("x_" + className + ".class"));
//            fs.write(cw.toByteArray());
        } catch (Exception e) {
    		LOG.info("FAILED CtorSniffer, class: " + className + "\n" + e.toString());
        }

		// skip function?
		Matcher matcher = funPattern.matcher(className);
		if (matcher.find()) {
			LOG.info("IGNORE class: " + className);
			return classBuffer;
		}

		// transform the class (in mem)
		LOG.info("INSTRUMENT_METHOD class: " + className);

		// function (method) instrumentation
		try {
            ClassReader cr = new ClassReader(classBuffer);
            ClassWriter cw = new ClassWriter(cr,
            	ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
            ClassVisitor cv = new ProfileTracer(cw);
            cr.accept(cv, ClassReader.EXPAND_FRAMES);
            // done
            classBuffer = cw.toByteArray();
            // DEBUG: dump it
//            FileOutputStream fs = new FileOutputStream(
//            	new File("z_" + className + ".class"));
//            fs.write(classBuffer);
        } catch (Exception e) {
    		LOG.info("FAILED ProfileTracer, class: " + className + "\n" + e.toString());
        }
        
		return classBuffer;
	}
}
