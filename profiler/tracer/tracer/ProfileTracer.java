package tracer;
import tracer.FrequencySet;

import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.AdviceAdapter;


class ProfileTransformAdapter extends AdviceAdapter {
	static Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	String className;
	String methodName;
	String fullName;

	public ProfileTransformAdapter(String className,
		int access, String name, String desc,
		MethodVisitor mv) {
		super(ASM4, mv, access, name, desc);
		this.className = className;
		this.methodName = name;
		this.fullName = className + ":" + name + ":" + desc;
	}

	@Override
	protected void onMethodEnter() {
		LOG.info("METHOD ENTER: " + fullName);

		// generate call
		trampoline("methodEnter", fullName);
	}

	@Override
	protected void onMethodExit(int opcode) {
		LOG.info("METHOD EXIT: " + fullName);

		// generate call
		trampoline("methodExit", fullName);
	}

	@Override
	public void visitMaxs(int maxStack, int maxLocals) {
		super.visitMaxs(maxStack + 4, maxLocals);
	}
	
	//
	
    private void trampoline(String fun, String name) {
		// generate standard call to fun
    	mv.visitLdcInsn(name);
    	mv.visitMethodInsn(INVOKESTATIC, "tracer/ProfileTracer", fun, "(Ljava/lang/String;)V");    }
    
//    private void testTrampoline() {
//    	ProfileTracer.methodEnter("name");
//    }
}


public class ProfileTracer extends ClassVisitor {
	static private Logger LOG = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	static FrequencySet<String> callCount = new FrequencySet<String>();
	static private Map<String, Long> callTimer = new Hashtable<String, Long>();
	static FrequencySet<String> callTimes = new FrequencySet<String>();
    String className;
 
    public ProfileTracer(ClassVisitor cv) { super(Opcodes.ASM4, cv); }
 
    public void visit(final int version,
                      final int access,
                      final String name,
                      final String signature,
                      final String superName,
                      final String[] interfaces) {
        className = name.replaceAll("/", ".");
        super.visit(version, access, name, signature, superName, interfaces);
    }
 
    @Override
    public MethodVisitor visitMethod(final int access,
                                     final String name,
                                     final String desc,
                                     final String signature,
                                     final String[] exceptions) {
    	MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
        LOG.info("METHOD class: " + className + ", name: " + name + ", desc: " + desc);

        // transform
        if (mv != null) {
        	mv = new ProfileTransformAdapter(className, access, name, desc, mv);
        }

        return mv;
    }

    
    ///////////////////////////////////////////////////////////////////////

    
    public static void methodEnter(String name) {
		if (! TracerAgent.enabled) {
			return;
		}
//		LOG.info("exec ENTER: " + name);
		
		// start call timer - per thread!
		String key = Thread.currentThread().getId() + "|" + name;
		callTimer.put(key, System.nanoTime());
    }


    public static void methodExit(String name) {
		if (! TracerAgent.enabled) {
			return;
		}
//		LOG.info("exec EXIT: " + name);

		// stop call timer - per thread!
		String key = Thread.currentThread().getId() + "|" + name;
		long delta = System.nanoTime() - callTimer.get(key);

		// update call times - thread safe!
		synchronized(callTimes) {
			callTimes.add(name, (int) (delta/Math.pow(10,6)));
		}

		// update call count - thread safe!
		synchronized(callCount) {
			callCount.inc(name);
		}
    }
}
