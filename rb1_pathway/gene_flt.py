#!/usr/bin/python

import sys, string, os



def read_symbols(fn_sym):
    s_sym = set()
    with open(fn_sym, "r") as f_in:
        for l in f_in:
            t = l.strip()
            t = t.upper()
            #print "[{0}]".format(t)
            s_sym.add(t)
    #print "length(s_sym): {0}".format(len(s_sym))
    return s_sym


def gene_filter(fn_sin, fn_in, fn_out, fn_sout, skip=0):
    s_sym = read_symbols(fn_sin)
    s_sout = set()
    #
    sep = "\t"
    with open(fn_out, "w") as f_out:
        with open(fn_in, "r") as f_in:
            for l in f_in:
                l = l.strip()
                tok = l.split(sep)
                lout = []
                if skip > 0:
                    if len(tok) <= skip:
                        continue
                    lout = tok[:skip]
                    tok = tok[skip:]
                #
                # print lout
                flt = [t for t in tok if t.upper() in s_sym]
                if len(flt) <= 0:
                    continue
                lout.extend(flt)
                so = sep.join(lout)
                f_out.write(so); f_out.write("\n");
                # print "before: {0}, after: {1}".format(len(tok), len(flt))
                s_sout = s_sout.union(set(flt))
    with open(fn_sout, "w") as f_out:
        for t in sorted(s_sout):
            f_out.write(t); f_out.write("\n");
    pass

#

def main(args):
    gene_filter(args[1], args[2], args[3], args[4], skip=int(args[5]))


if __name__ == '__main__':
    main(sys.argv)
