#!/usr/bin/python

import sys, string, os, re


def symbol_filter(fn_in):
    # compound entitities (to remove!)
    cre = re.compile( r"_\((complex|family|abstract)\)$" )
    #
    sep = "\t"
    with open(fn_in, "r") as f_in:
        for l in f_in:
            l = l.strip()
            tok = l.split(sep)
            flt = [t for t in tok if not cre.search(t, re.IGNORECASE)]
            if len(flt) <= 0:
                continue
            so = sep.join(flt)
            print so
            print "before: {0}, after: {1}".format(len(tok), len(flt))
    pass

#

def main(args):
    symbol_filter(args[1])


if __name__ == '__main__':
    main(sys.argv)
