#!/usr/bin/python

from xml.etree import ElementTree as XmlTree
import sys, string, os
# import numpy


"""
https://dna.five3genomics.com/paradigm/help

UCSC Pathway Tab Format

Paradigm requires provided pathway files to be provided in the UCSC Pathway Tab Format. This format is designed to be an extremely simple representation of a pathway. Each line in the file either defines the type of an entity, or an interaction between two entities. Entity definition lines are distinguished from interaction lines by the number of fields on the line: entity definitions have two tab-separated fields, whereas interaction lines have three tab-separated fields.

A simple example:

    protein     TP53
    protein     MDM2
    abstract    apoptosis
    MDM2        TP53         -a|
    TP53        apoptosis    -a>

This example consists of three lines defining entities, followed by two lines defining interactions.

Entity Definition Lines

An entity definition line is a two-field tab-delimited line that associates a biological type (the first field) to an identifier (the second field). In the example above, TP53 and MDM2 are identified as proteins, and apoptosis as abstract (an abbreviation for "abstract process"). Any identifier can be used as the biological type, but currently these are in common use:

    protein       a protein-coding gene **
    chemical      a small-molecule, such as GTP
    complex       a non-covalently-bound collection of other molecules,
                  such as proteins, small molecules, etc.
    family        a gene family that performs a similar function (e.g.
                  the RAS family, consisting of HRAS, NRAS, and KRAS)
    abstract      an abstract process, such as apoptosis

If an entity ID is encountered in a UCSC Pathway Tab format, and it is not explicitly assigned an entity type, then the entity defaults to the 'protein' type.

Interaction Definition Lines

Interaction definition lines consist of three tab-separated fields:

  parent_id  child_id  interaction_type

Currently, all our interaction types are directional, and there is no support for undirected edges. Therefore it's important to maintain the order of the first and second field. The edge types currently in use are:

    -a>          the parent promotes the activity of the child
    -a|          the parent inhibits the activity of the child
    -t>          the parent transcriptionally activates the child
    -t|          the parent transcriptionally inhibits the child
    -ap>         the parent activates the child, and either the parent
                 or child is an abstract process is an abstract process
    -ap|         the parent inhibits the child, and either the parent
                 or child is an abstract process is an abstract process
    component>   the child is a complex, and the parent is a component 
                 of that complex
    member>      the child is a gene family, and the parent is a
                 component of that family

"""


class Error(Exception):
    """Base class for exceptions in this module."""
    pass
    
    
class OntologyError(Error):

    def __init__(self, name, msg="", url="", txt=""):
        self.id = name
        self.msg = msg
        self.url = url
        self.txt = txt

    def __str__(self):
        return ">>> Exception, name: '{0}', message: '{1}', URL: '{2}', text:\n{3}\n".format(self.id, self.msg, self.url, self.txt)



class Entity():
    #
    Type_protein = "protein"
    Type_chemical = "chemical"
    Type_complex = "complex"
    Type_family = "family"
    Type_abstract = "abstract"

    T_Map = {
        "GeneProduct" : Type_protein,
        "Protein" : Type_protein,
        "Complex" : Type_complex,
        "Group" : Type_family,
        "Pathway" : Type_abstract,
    }

    # entity is either a Node or a Group
    # groups are made using GroupRef, while interactions use the GraphRef
    # Group is either GroupRef (for a node) or GroupId (for a group)
    def __init__(self, Type, Label=None, Graph=None, Group=None):
        self.Type = Type
        self.Label = Label
        self.Graph = Graph
        self.Group = Group


class Relation():
    #
    Type_promotes = "promotes"
    Type_inhibits = "inhibits"
    Type_trans_activates = "transcriptionally activates"
    Type_trans_inhibits = "transcriptionally inhibits"
    Type_activates_abstract = "activates (abstract)"
    Type_inhibits_abstract = "inhibits (abstract)"
    Type_component = "component"
    Type_member = "member"
    
    # PARADIGM format token
    P_Map = {
        Type_promotes : "-a>",
        Type_inhibits : "-a|",
        Type_trans_activates : "-t>",
        Type_trans_inhibits : "-t|",
        Type_activates_abstract : "-ap>",
        Type_inhibits_abstract : "-ap|",
        Type_component : "component>",
        Type_member : "member>",
    }

    def __init__(self, Type, Parent, Child):
        self.Type = Type
        self.Parent = Parent
        self.Child = Child


class GPMLNet():
    XNS = "{http://pathvisio.org/GPML/2013a}"
    # entity name translation
    # [...]  entity names should only contain the following characters:
    # A-Z, a-z, 0-9, _, /, -, (, )   (comma is not one of the recommended characters)
    # Spaces also need to be removed.
    ENAllowChr = ( string.ascii_lowercase + string.ascii_uppercase +
        "".join(str(x) for x in range(0,10)) +
        "_/-()" )
    ENForbidChr = ''.join([c for c in map(chr,range(256)) if c not in ENAllowChr])
    ENTransTable = string.maketrans(ENForbidChr, "_" * len(ENForbidChr))

    def __init__(self, filePath):
        self.filePath = filePath
        # entities := {id : Entity}
        self.DGraphs = {}
        self.DGroups = {}
        # relations := {Type : dict_Parent}
        # dictParent := {Parent, set(Child1, Child2, ...)}
        self.DRelations = {
            # entity links as dict: {Ent1 : set(E21, E22, ...)}
            Relation.Type_promotes : {},
            Relation.Type_inhibits : {},
            Relation.Type_trans_activates : {},
            Relation.Type_trans_inhibits : {},
            Relation.Type_activates_abstract : {},
            Relation.Type_inhibits_abstract : {},
            Relation.Type_component : {},
            Relation.Type_member : {},
        }
        #
        self.load()


    def __str__(self):
        return ""


    def getInteractionType(self, n1, n2, s):
        if s == "mim-transcription-translation":
            return Relation.Type_trans_activates

        if (n1.Type == Entity.Type_abstract or n2.Type == Entity.Type_abstract):
            if s == "mim-inhibition":
                return Relation.Type_inhibits_abstract
            if s == "mim-stimulation":
                return Relation.Type_activates_abstract

        if s == "mim-inhibition":
            return Relation.Type_inhibits
        if s == "mim-stimulation":
            return Relation.Type_promotes

        return None


    def getGroupRelType(self, group):
        rtype = None
        if group.Type == Entity.Type_complex:
            rtype = Relation.Type_component
        elif group.Type == Entity.Type_family:
            rtype = Relation.Type_member
        return rtype


    def fixEntName(self, ent):
        # translate
        name = string.translate(ent.Label, GPMLNet.ENTransTable)
        # add suffix for special types
        if ent.Type in [Entity.Type_abstract, Entity.Type_complex, Entity.Type_family]:
            name = "{0}_({1})".format(name, ent.Type)
        # update
        ent.Label = name


    def load(self):
        tree = XmlTree.ElementTree(file=self.filePath)
        root = tree.getroot()
        # print root.tag
        #print ", ".join(i.tag for i in root)

        # iterate all "DataNode" entities
        xname = "{0}DataNode".format(GPMLNet.XNS)
        # print xname
        for elem in root.findall(xname):
            # print elem.tag, elem.attrib
            attr = elem.attrib
            etype = Entity.T_Map[attr["Type"]]
            ent = Entity(
                etype,
                attr["TextLabel"],
                attr["GraphId"],
                elem.get("GroupRef"))
            self.DGraphs[ent.Graph] = ent
            # print "add ent Label: {0}, GraphId: {1}, Type: {2}".format(ent.Label, ent.Graph, ent.Type)

        # iterate all "Group" entities
        xname = "{0}Group".format(GPMLNet.XNS)
        # print xname
        for elem in root.findall(xname):
            # print elem.tag, elem.attrib
            attr = elem.attrib
            etype = Entity.T_Map[attr["Style"]]
            ent = Entity(
                etype,
                None,
                elem.get("GraphId"),
                attr["GroupId"])
            # print ent.Group
            if ent.Graph is not None:
                self.DGraphs[ent.Graph] = ent
            self.DGroups[ent.Group] = ent


        # iterate all "Interaction" relations
        # interactions use "GraphRef"
        xname = "{0}Interaction".format(GPMLNet.XNS)
        # print xname
        for elem in root.findall(xname):
            # print elem.tag, elem.attrib
            xname = "{0}Graphics".format(GPMLNet.XNS)
            Graphics = elem.find(xname)
            xname = "{0}Point".format(GPMLNet.XNS)
            points = Graphics.findall(xname)
            assert len(points) == 2
            Point1 = points[0]
            Point2 = points[1]
            Parent = self.DGraphs[ Point1.attrib["GraphRef"] ]
            Child = self.DGraphs[ Point2.attrib["GraphRef"] ]
            # regulation?
            rtype = None
            if "ArrowHead" in Point2.attrib:
                # regulation relation
                # print "regulation"
                rtype = self.getInteractionType(Parent, Child, Point2.attrib["ArrowHead"])
            else:
                # composition relation
                # print "composition"
                assert Graphics.attrib["LineStyle"] == "Broken"
                rtype = Relation.Type_member
            # add to list
            drels = self.DRelations[rtype]
            eset = set()
            if Parent in drels:
                eset = drels[Parent]
            else:
                drels[Parent] = eset
            eset.add(Child)

        # add composition relations for nodes (GroupRef)
        for k in self.DGraphs:
            ent = self.DGraphs[k]
            if ent.Type != Entity.Type_protein or ent.Group is None:
                continue
            Group = self.DGroups[ent.Group]
            rtype = self.getGroupRelType(Group)
            drels = self.DRelations[rtype]
            eset = set()
            if Group in drels:
                eset = drels[Group]
            else:
                drels[Group] = eset
            print "add to group: {0} ent: {1}".format(Group.Group, ent.Label)
            eset.add(ent)

        # output entities
        dout = set()
        for k in self.DGraphs:
            ent = self.DGraphs[k]
            # fix Group entity label (name)
            if ent.Group is not None:
                Group = self.DGroups[ent.Group]
                if Group.Label is None:
                    # print "Group: {0}".format(Group.Group)
                    rtype = self.getGroupRelType(Group)
                    drels = self.DRelations[rtype]
                    if Group not in drels:
                        continue
                    Group.Label = "/".join(sorted(e.Label for e in drels[Group]
                        if e.Label is not None))
                    # print "Group: {0}, Label: {1}".format(ent.Group, Group.Label)
            # suppress duplicates
            if (ent.Type, ent.Label) in dout:
                continue
            # fix
            self.fixEntName(ent)
            # output
            print "{0}\t{1}".format(ent.Type, ent.Label)
            dout.add((ent.Type, ent.Label))

        # output relations
        for k in self.DRelations:
            drels = self.DRelations[k]
            for p in drels:
                for c in drels[p]:
                    n1 = p
                    n2 = c
                    # swap p and c in some bizzare inversion cases
                    if k in [Relation.Type_component, Relation.Type_member]:
                        n1 = c
                        n2 = p
                    print "\t".join([n1.Label, n2.Label, Relation.P_Map[k]])

#
#
#
if __name__ == '__main__':
    a = GPMLNet("RB1_v4.gpml")
