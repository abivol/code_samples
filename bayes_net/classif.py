
import sys, math, random, stat, getopt
from tokenfreq import *
from util import *



class Classifier:

    # if no traindir given, assume built-in cross-validation from single folder
    def __init__(self, testdir, traindir = None):
        self.Root = testdir
        self.Classes = [c for c in os.listdir(testdir) if stat.S_ISDIR(os.stat(os.path.join(testdir, c))[stat.ST_MODE])]
        # single dir => cross-validation
        if traindir == None:
            return
        # separate train / test dirs were given 
        self.TestSet = {}
        self.TokDist = {}
        # read training samples
        for c, trainset in self.ReadSamples(traindir):
            print '%s size: %d' % (c, len(trainset))
            # train each class recognizer
            train = [os.path.join(traindir, c, f) for f in trainset]
            self.Bayes(c, train)
        # read test samples
        for c, s in self.ReadSamples(self.Root):
            self.TestSet[c] = s


    def Bayes(self, c, t):
        tf = TokenFreq(c)
        # estimate priors for some known classes
        prior = 1.0 / len(self.Classes)
        scale = 1.0
        # spam?
        if len(self.Classes) == 2:
            spam_prior = 0.8
            if 'spam' in c:
                prior = spam_prior
                scale = 1/1.10
#                print 'spam!'
            elif 'ham' in c:
                prior = 1 - spam_prior
#                print 'ham!'
        tf.LogPrior = math.log(prior)
        tf.Scale = scale
        #
        tf.Train(t)
        self.TokDist[c] = tf


    def Predict(self, fn):
        mlp = -sys.maxint
        classif = None
        sbc = None # second best class
        sblp = mlp
#        print 'sample: %s' % (fn)
        for c in self.Classes:
            tf = self.TokDist[c]
            lp = tf.Compute(fn) * tf.Scale
#            print 'class: %s, lp: %f' % (c, lp)
            # update second best
            if lp > sblp:
                sblp = lp
                sbc = c
            # best
            if lp > mlp:
                # save second best
                sblp = mlp
                sbc = classif
                # update best
                mlp = lp
                classif = c
        # distance between top 2 classifications
        delta = math.fabs(mlp - sblp)
#        print 'class: %s, sbc: %s, delta: %f' % (classif, sbc, delta)
        return classif
    

    def Print(self, msg):
        if not self.Noisy:
            return
        print msg


    def Evaluate(self, noisy = True):
        self.Noisy = noisy
        # samples are organized by class
        known = []
        predicted = []
        for c in self.Classes:
            testset = self.TestSet[c]
            # compute predictions
            for f in testset:
                p = os.path.join(self.Root, c, f)
                classif = self.Predict(p)
                # update results
                known.append(c)
                predicted.append(classif)
                self.Print( 'item: %s, known: %s, predicted: %s' % (f, c, classif) )
        assert len(known) == len(predicted)
        count = len(predicted)
        # compute metrics
        # per class: precision, recall
        self.Precision = {}
        self.Recall = {}
        self.Fmeasure = {}
        for c in self.Classes:
            predc = predicted.count(c) # predicted of class c
            knc = known.count(c) # known of class c
            tp = 0 # predicted + ('true positive')
            for i in range(count):
                if predicted[i] != c:
                    continue
                if known[i] == c:
                    tp += 1
            assert(predc >= tp and knc >= tp)
            if predc > 0:
                self.Precision[c] = float(tp) / predc
            else:
                self.Precision[c] = 0.0
            self.Print( 'Precision[%s]: %f' % (c, self.Precision[c]) )
            if knc > 0:
                self.Recall[c] = float(tp) / knc
            else:
                self.Recall[c] = 0.0
            self.Print( 'Recall[%s]: %f' % (c, self.Recall[c]) )
            if self.Precision[c] + self.Recall[c] == 0:
                self.Fmeasure[c] = 0
            else:
                self.Fmeasure[c] = 2 * self.Precision[c] * self.Recall[c] / (self.Precision[c] + self.Recall[c])
            self.Print( 'Fmeasure[%s]: %f' % (c, self.Fmeasure[c]) )
            print '%s f-measure: %f' % (c, self.Fmeasure[c])
        

    def ReadSamples(self, root):
        for c in self.Classes:
            dir = os.path.join(root, c)
            samples = []
            for f in os.listdir(dir):
                p = os.path.join(dir, f)
                assert stat.S_ISREG(os.stat(p)[stat.ST_MODE])
                samples.append(f)
            random.shuffle(samples)
            yield c, samples


    def Crossvalidation(self, ratio = 1.0/5):
        # holds data set info, per class
        self.Samples = {}
        self.TestSet = {}
#        self.TrainSet = {}
        self.TokDist = {}
        # prepare data sets
        for c, s in self.ReadSamples(self.Root):
            self.Samples[c] = s
        # fold
        count = int(1.0/ratio)
        results = {} # contains a list of data tuples for each class
        for i in range(count):
            for c in self.Classes:
                # partition
                samples = self.Samples[c]
                tsz = int(len(samples) * ratio)
                begin = i * tsz
                end = (i + 1) * tsz 
                testset = samples[begin : end]
                trainset = []
                trainset.extend(samples[:begin])
                trainset.extend(samples[end:])
                # partition done                
                self.TestSet[c] = testset
                # train each class recognizer
                train = [os.path.join(self.Root, c, f) for f in trainset]
                self.Bayes(c, train)
            assert fequal(sum(math.exp(t.LogPrior) for t in self.TokDist.values()), 1.0)
            # evaluate performance for fold
            print 'Fold: %d' % (i)
            self.Evaluate()
            # update results
            for c in self.Classes:
                if c not in results:
                    results[c] = []
                results[c].append((self.Precision[c], self.Recall[c], self.Fmeasure[c]))
        # averages
        print results
        for c in self.Classes:
            aprecision, arecall, afmeasure = [ sum(results[c][i][j] for i in range(count)) / count for j in range(3) ]
            print 'Averages[%s]: precision = %f, recall = %f, fmeasure = %f' % (c, aprecision, arecall, afmeasure)


def UT_Classif_1():
    c = Classifier('hard2')
    for i in range(1):
        c.Crossvalidation()
    
    
def UT_Classif_2():
    c = Classifier('test3\\test', 'test3\\train')
    c.Evaluate(noisy = False)


def UT():
    UT_Classif_1()
#    UT_Classif_2()


def Usage():
    print 'Usage:\nclassif.py -c <dir>\nclassif.py --traindir=<dir> --testdir=<dir>'
    sys.exit(1)


def main():
    try :
        opts, args = getopt.getopt(sys.argv[1:], 'c:r:e:', ['cross=', 'traindir=', 'testdir='])
    except getopt.GetoptError :
        Usage()
    #
    ted = None
    trd = None
    for o, v in opts:
        if o in ['-c', '--cross']:
            c = Classifier(v)
            c.Crossvalidation()
            return
        elif o in ('-r', '--traindir'):
            trd = v
        elif o in ('-e', '--testdir'):
            ted = v
    if trd == None or ted == None:
        Usage()
    #
    c = Classifier(ted, trd)
    c.Evaluate(noisy = False)


if __name__ == '__main__' :
#    UT()
    main()
