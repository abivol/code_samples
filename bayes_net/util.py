
import sys, math


stopwords = ['I', 'a', 'an', 'as', 'at', 'by', 'he', 'his', 'me', 'or', 'thou',
            'us', 'who', 'against', 'amid', 'amidst', 'among', 'amongst',
            'and', 'anybody', 'anyone', 'because', 'beside', 'circa',
            'despite', 'during', 'everybody', 'everyone', 'for', 'from', 'her',
            'hers', 'herself', 'him', 'himself', 'hisself', 'idem', 'if',
            'into', 'it', 'its', 'itself', 'myself', 'nor', 'of', 'oneself',
            'onto', 'our', 'ourself', 'ourselves', 'per', 'she', 'since',
            'than', 'that', 'the', 'thee', 'theirs', 'them', 'themselves',
            'they', 'thine', 'this', 'thyself', 'to', 'tother', 'toward',
            'towards', 'unless', 'until', 'upon', 'versus', 'via', 'we',
            'what', 'whatall', 'whereas', 'which', 'whichever', 'whichsoever',
            'whoever', 'whom', 'whomever', 'whomso', 'whomsoever', 'whose',
            'whosoever', 'with', 'without', 'ye', 'you', 'you-all', 'yours',
            'yourself', 'yourselves', 'aboard', 'about', 'above', 'across',
            'after', 'all', 'along', 'alongside', 'although', 'another',
            'anti', 'any', 'anything', 'around', 'astride', 'aught', 'bar',
            'barring', 'before', 'behind', 'below', 'beneath', 'besides',
            'between', 'beyond', 'both', 'but', 'concerning', 'considering',
            'down', 'each', 'either', 'enough', 'except', 'excepting',
            'excluding', 'few', 'fewer', 'following', 'ilk', 'in',
            'including', 'inside', 'like', 'many', 'mine', 'minus', 'more',
            'most', 'naught', 'near', 'neither', 'nobody', 'none', 'nothing',
            'notwithstanding', 'off', 'on', 'opposite', 'other', 'otherwise',
            'outside', 'over', 'own', 'past', 'pending', 'plus', 'regarding',
            'round', 'save', 'self', 'several', 'so', 'some', 'somebody',
            'someone', 'something', 'somewhat', 'such', 'suchlike', 'sundry',
            'there', 'though', 'through', 'throughout', 'till', 'twain',
            'under', 'underneath', 'unlike', 'up', 'various', 'vis-a-vis',
            'whatever', 'whatsoever', 'when', 'wherewith', 'wherewithal',
            'while', 'within', 'worth', 'yet', 'yon', 'yonder']



# Knuth (http://live.boost.org/doc/libs/1_31_0/libs/test/doc/components/test_tools/floating_point_comparison.html)
def fequal(f1, f2, e = 0.000001):
    a1, a2 = math.fabs(f1), math.fabs(f2)
    d = math.fabs(f1 - f2)
    r = (d <= e * a1) and (d <= e * a2)
    return r

