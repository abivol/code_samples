
import sys, math, os, re, random


# returns message body without headers (SMTP)
def RemoveMessageHeaders(msg):
    lines = msg.splitlines(True)
    for i in range(len(lines) - 1):
        # first blank line
        if len(lines[i]) <= 1:
            accept = True
            break
    return ''.join(lines[i+1:])


def LogProb(p):
    assert p > 0 and p <= 1
    return math.log(p)


class TokenFreq:
#    Delimiters = re.compile('@|\.|\'|\"|,|;|=|\+|\(|\)|<|>|\[|\]|\{|}|\*|/|&|\\\\|%|#|\^|!|_|:|\?|\$|\|')

    def __init__(self, label):
        self.Class = label
        self.TokenFreq = {}
        self.DocFreq = {}
        self.LogProb = {}
        self.LogPrior = math.log(0.5)
        self.TokenCount = 0
        self.DocCount = 0
        self.Type = None
        self.Scale = 1.0
        
    def __repr__(self):
        s = 'DocCount = %d, DocFreq: %s, LogProb: %s' % (self.DocCount, self.DocFreq.items()[:100], self.LogProb.items()[:100])
        return s

    # transform entire document: eliminate message headers, html tags, etc
    def Transform(self, s):
#        print 'Before:\n', s
#        s = RemoveMessageHeaders(s)
#        print 'After:\n', s
#        assert len(s) > 0
        return s

    # filter token
    def Filter(self, s):
        if len(s) <= 0:
            return False
        # remove numbers
#        if ord(s[0]) in range(ord('0'),ord('9')):
#            return False
        return True

    # return tokens
    def Tokenize(self, s):
        s = self.Transform(s)
        # tokenize based on common delimiters
#        s = re.sub(TokenFreq.Delimiters, ' ', s)
        tokens = s.split()
        tokens = [t for t in tokens if self.Filter(t)]
        return tokens


    # update token frequencies after adding a new document    
    def Update(self, fn):
        f = open(fn, 'r')
        str = f.read()
        f.close()
        # tokenize, filter, etc
        toks = self.Tokenize(str)
        # update counts
        self.TokenCount += len(toks)
        self.DocCount += 1
        for t in set(toks):
            cnt = toks.count(t)
            if t in self.TokenFreq:
                self.TokenFreq[t] += cnt
                self.DocFreq[t] += 1
            else:
                self.TokenFreq[t] = cnt
                self.DocFreq[t] = 1


    def Finalize(self):
        # update probabilities
        for t in self.DocFreq:
            self.LogProb[t] = math.log(float(self.DocFreq[t]) / self.DocCount)


    # train by updating from file sequence
    def Train(self, seq):
        for f in seq:
            self.Update(f)
        self.Finalize()


    # compute probability
    def Compute(self, fn):
        f = open(fn, 'r')
        str = f.read()
        f.close()
        # tokenize, filter, etc
        toks = self.Tokenize(str)
        lpc = self.LogPrior
        for t in set(toks):
            lp = 0.0
            if t in self.LogProb:
                lp = self.LogProb[t]
            else:
                # fix
                lp = math.log(1.0 / self.DocCount)
            # update log probability
#            print t, lp
            lpc += lp
        return lpc


def UT_TokenFreq_1():
    tf = TokenFreq()
    tf.Update('test\spam\\0500.2e8762b67913d1b07bc8da293448d27f')
    print tf
    pass

def UT_TokenFreq_2():
    # spam
    print 'Train: spam'
    d = 'test\\spam'
    data = [os.path.join(d, f) for f in os.listdir(d)]
    random.shuffle(data)
    train = data[:-1]
    test = data[-1]
    tf = TokenFreq()
    tf.Train(train)
    print tf
#    s1 = tf.Compute('test\\hard_ham\\0221.cecc5d590b4439a306e218b773adac08')
    s1 = tf.Compute('test\\easy_ham\\0945.1a86d4f67991ad5793a17ba1863edd17')
    print 'easy ham: ', s1
    s2 = tf.Compute(test)
    print 'spam: ', s2
    # ham
    print 'Train: ham'
    d = 'test\\easy_ham'
    data = [os.path.join(d, f) for f in os.listdir(d)]
    random.shuffle(data)
    train = data[:-1]
    test = data[-1]
    tf = TokenFreq()
    tf.Train(train)
    print tf
    s1 = tf.Compute(test)
    print 'hard ham: ', s1
    s2 = tf.Compute('test\\spam\\0086.4b3a02be9a2561ada188d95b4601c01e')
    print 'spam: ', s2
    pass

def UT_RemoveMessageHeaders():
    fn = 'test\\easy_ham\\1304.10546e3c5e1352134ff26c0e49f72864'
    f = open(fn, 'r')
    m = f.read()
    f.close()
    print RemoveMessageHeaders(m)
    
def UT_Tokenize():
    s = '@.\'",;=+()<>[]{}*/&\\%#^!_:?$'
    tf = TokenFreq()
    print 'UT_Tokenize: "%s"   -->   "%s"' % (s, ''.join(tf.Tokenize(s)))    


def UT():
#    UT_TokenFreq_1()
    UT_TokenFreq_2()
#    UT_RemoveMessageHeaders()
#    UT_Tokenize()
    
def main():
    pass


if __name__ == '__main__' :
    UT()
#    main()
