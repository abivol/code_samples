
function [x,fval,g ] = mymin(x0)
    x = x0;
    done = 0;
    while ~done
        H = hessian(x);
        g_center = gradient(x)';
        b_approx = H * x - g_center; 
            %b of quadratic approximation. Chosen so that
            %H*x - b_approx = g_center
        for j = 1:3
            g = H * x - b_approx;
            if j == 1
                d = -g;
                    if norm(g) > 1e-9
                        alpha = linesearch(x,d);
                    else
                        alpha = 0;
                    end;
            else
                # subtract from steepest descent direction (gradient)
                # its H-orthogonal projection onto the previous direction d
                gamma = (g'*H*d) / (d'*H*d);
                d = -(g - gamma*d);
                    if norm(d) > 1e-9
                        alpha = linesearch(x,d);
                    else
                        alpha = 0;
                    end;
            end;
            x = x + alpha*d;
            fval = objective(x)
        end;
        g = gradient(x);
        if norm(g)<1e-9
            done = 1;
        end;
    end;
    fval = objective(x);
end


function alpha = linesearch(x,d)
    alpha = 0;
    for j=1:10
        alpha = alpha + -gradient(x+alpha*d)*d / (d'*hessian(x+alpha*d)*d);
    end;
end    


function fval = objective(q)
    fval = ones(1,3) * exp((1:3)' .* q) + q(1)^2 + q(2)^2 + q(3)^2 + q(1)*q(2) + 0.2*q(2)*q(3) - 10*q(1) - 20*q(2) - 30*q(3)
end


function grad = gradient(q)
    g_1 = exp(q(1)) + 2*q(1) + q(2) - 10;
    g_2 = 2*exp(2*q(2)) + 2*q(2) + q(1) + 0.2*q(3) - 20;
    g_3 = 3*exp(3*q(3)) + 2*q(3) + 0.2*q(2) - 30;
    grad = [g_1 g_2 g_3];
end


function H = hessian(q)
    H = zeros(3);
    h_11 = exp(q(1)) + 2;
    h_22 = 4*exp(2*q(2)) + 2;
    h_33 = 9*exp(3*q(3)) + 2;
    h_12 = 1;
    h_13 = 0;
    h_23 = 0.2;
    #
    H(1,1) = h_11;
    H(2,2) = h_22;
    H(3,3) = h_33;
    #
    H(1,2) = H(2,1) = h_12;
    H(1,3) = H(3,1) = h_13;
    H(2,3) = H(3,2) = h_23;
end
