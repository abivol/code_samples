
%
% pp. 369-370
%
function [x,fval,g] = myminimize(a, b, x0, W)
    %
    A = [0 1 0; 0 0 1;];
    %
    x = x0;
    done = 0;
    step = 1
    while ~done
        % optimization loop
        while 1
            % [1] test/add new constraint(s)
            if step == 1
                fprintf('STEP 1\n')
                if ~ W(1)
                    % 1st contraint hit?
                    if x(2) >= a
                        fprintf('1st contraint hit\n')
                        W(1) = 1;
                    end
                end
                if ~ W(2)
                    % 2nd contraint hit?
                    if x(3) >= b
                        fprintf('2nd contraint hit\n')
                        W(2) = 1;
                    end
                end
                W
            end % step == 1
            %
            g = gradient(x)';
            % [2] compute direction d
            fprintf('STEP 2\n')
            B = A(W==[1 1], :)
            if isempty(B)
                d = -g
            else
                F = eye(3) - B'*inv(B*B')*B;
                d = - F * g
            end
            % done with d?
            if norm(d) <= 1e-6
                break
            end
            % [3] move along d
            fprintf('STEP 3\n')
            step = 1
            % update x
            x = linesearch(x, d, A, [a b]')
        end % ~ while
        %
        % [4] test the Lagrange multipliers / KKT conditions
        fprintf('STEP 4\n')
        if isempty(B)
            break
        end
        %
        lambda = - (inv(B*B')*B) * g
        if lambda >= 0
            done = 1
        else
            % remove inactive constraints
            fprintf('remove inactive constraints\n')
            W
            W(W==[1 1]) = (lambda >= 0)
        end
        % do not update W now! (step 1)
        step = 2

    end % ~while

    fval = objective(x);
    g = gradient(x)
end


function nx = linesearch(x, d, A, z)
    %
    nx = x
    alpha = 0
    a = 0
    fmin = objective(x);
    % small factor / steps
    f = 1 %e-2;
    for j=1:10
        a = a + f*( -gradient(x+a*d)*d / (d'*hessian(x+a*d)*d) )
        % feasible?
        y = x + a*d
        F = A*y - z
        if sum(F > 0) > 0
            fprintf('y not feasible!\n')
            i = (F > 0)
            nx([false; i]) = z(i)
            break
        end
        % optimal?
        if objective(y) < fmin
            alpha = a
            nx = x + alpha*d
            fmin = objective(y)
        end
    end;
end


function fval = objective(q)
    fval = ones(1,3) * exp((1:3)' .* q) + q(1)^2 + q(2)^2 + q(3)^2 + q(1)*q(2) + 0.2*q(2)*q(3) - 10*q(1) - 20*q(2) - 30*q(3)
end


function grad = gradient(q)
    g_1 = exp(q(1)) + 2*q(1) + q(2) - 10;
    g_2 = 2*exp(2*q(2)) + 2*q(2) + q(1) + 0.2*q(3) - 20;
    g_3 = 3*exp(3*q(3)) + 2*q(3) + 0.2*q(2) - 30;
    grad = [g_1 g_2 g_3];
end


function H = hessian(q)
    H = zeros(3);
    h_11 = exp(q(1)) + 2;
    h_22 = 4*exp(2*q(2)) + 2;
    h_33 = 9*exp(3*q(3)) + 2;
    h_12 = 1;
    h_13 = 0;
    h_23 = 0.2;
    %
    H(1,1) = h_11;
    H(2,2) = h_22;
    H(3,3) = h_33;
    %
    H(1,2) = h_12;
    H(2,1) = h_12;
    H(1,3) = h_13;
    H(3,1) = h_13;
    H(2,3) = h_23;
    H(3,2) = h_23;
end
