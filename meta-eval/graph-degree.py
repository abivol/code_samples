#!/usr/bin/python

import sys, networkx as nx

#
# print out the degree of each vertex in the graph
#

def graph_degree(fn):
    fh = open(fn, "rb")
    g = nx.read_edgelist(fh)
    fh.close()
    #
    for n in g.nodes():
        # print n
        d = g.degree(n)
        print "{0}\t{1}".format(n, d)


#
#
#
def main(args):
    graph_degree(args[1])


if __name__ == '__main__':
    main(sys.argv)
